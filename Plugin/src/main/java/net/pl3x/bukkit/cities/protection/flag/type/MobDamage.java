package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.entity.EntityDamagePlayerEvent;
import net.pl3x.bukkit.cities.event.entity.EntitySplashPlayerEvent;
import net.pl3x.bukkit.cities.event.entity.MobTargetPlayerEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to stop hostile mobs from hurting players
 */
public class MobDamage extends FlagListener {
    private final String name = "mob-damage";

    public MobDamage(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onDamagePlayer(EntityDamagePlayerEvent event) {
        if (!(event.getDamager() instanceof LivingEntity)) {
            return;
        }

        if (Flags.isAllowed(name, null, event.getDamaged().getLocation())) {
            return;
        }

        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onMobSplashPlayer(EntitySplashPlayerEvent event) {
        if (!(event.getThrower() instanceof Creature)) {
            return;
        }

        if (Flags.isAllowed(name, null, event.getSplashed().getLocation())) {
            return;
        }

        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onMobTargetPlayer(MobTargetPlayerEvent event) {
        if (Flags.isAllowed(name, null, event.getPlayer().getLocation())) {
            return;
        }

        event.setCancelled(true);
    }
}
