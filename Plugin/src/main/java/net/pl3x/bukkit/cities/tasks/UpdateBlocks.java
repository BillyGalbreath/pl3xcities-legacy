package net.pl3x.bukkit.cities.tasks;

import org.bukkit.block.BlockState;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class UpdateBlocks extends BukkitRunnable {
    private final List<BlockState> toRemove;

    public UpdateBlocks(List<BlockState> toRemove) {
        this.toRemove = toRemove;
    }

    @Override
    public void run() {
        for (BlockState state : toRemove) {
            state.update(true, false);
        }
    }
}

