package net.pl3x.bukkit.cities;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.hook.ProtocolLib;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;

@SuppressWarnings("WeakerAccess")
public class Title {
    private final String title;
    private final String subtitle;
    private final String actionbar;
    private final int fadeIn;
    private final int stay;
    private final int fadeOut;

    public Title(String title, String subtitle, String actionbar) {
        this.title = title;
        this.subtitle = subtitle;
        this.actionbar = actionbar;
        this.fadeIn = Config.TITLE_TIME_FADE_IN.getInt();
        this.stay = Config.TITLE_TIME_STAY.getInt();
        this.fadeOut = Config.TITLE_TIME_FADE_OUT.getInt();
    }

    public void send(Player player) {
        if (title != null && !title.isEmpty()) {
            resetTitle(player);

            sendPacket(player, buildTimingsPacket());
            sendPacket(player, buildTitlePacket());

            if (subtitle != null && !subtitle.isEmpty()) {
                sendPacket(player, buildSubtitlePacket());
            }
        }

        if (actionbar != null && !actionbar.isEmpty()) {
            sendPacket(player, buildActionbarPacket());
        }
    }

    public void resetTitle(Player player) {
        try {
            sendPacket(player, buildResetPacket());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendPacket(Player player, PacketContainer packet) {
        try {
            ProtocolLib.getManager().sendServerPacket(player, packet);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private PacketContainer buildTimingsPacket() {
        PacketContainer packet = getTitlePacket();

        packet.getTitleActions().write(0, EnumWrappers.TitleAction.TIMES);
        packet.getIntegers()
                .write(0, fadeIn)
                .write(1, stay)
                .write(2, fadeOut);

        return packet;
    }

    private PacketContainer buildTitlePacket() {
        PacketContainer packet = getTitlePacket();

        packet.getTitleActions().write(0, EnumWrappers.TitleAction.TITLE);
        packet.getChatComponents().write(0, WrappedChatComponent.fromText(getText(title)));

        return packet;
    }

    private PacketContainer buildSubtitlePacket() {
        PacketContainer packet = getTitlePacket();

        packet.getTitleActions().write(0, EnumWrappers.TitleAction.SUBTITLE);
        packet.getChatComponents().write(0, WrappedChatComponent.fromText(getText(subtitle)));

        return packet;
    }

    private PacketContainer buildResetPacket() {
        PacketContainer packet = getTitlePacket();

        packet.getTitleActions().write(0, EnumWrappers.TitleAction.RESET);

        return packet;
    }

    private PacketContainer buildActionbarPacket() {
        PacketContainer packet = getActionbarPacket();

        packet.getBytes().write(0, (byte) 2);
        packet.getChatComponents().write(0, WrappedChatComponent.fromText(getText(actionbar)));

        return packet;
    }

    private PacketContainer getTitlePacket() {
        return ProtocolLib.getManager().createPacket(PacketType.Play.Server.TITLE);
    }

    private PacketContainer getActionbarPacket() {
        return ProtocolLib.getManager().createPacket(PacketType.Play.Server.CHAT);
    }

    private String getText(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }
}
