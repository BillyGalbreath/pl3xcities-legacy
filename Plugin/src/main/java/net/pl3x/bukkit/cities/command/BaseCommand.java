package net.pl3x.bukkit.cities.command;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class BaseCommand extends CommandHandler {
    private final TreeMap<String, CommandHandler> subCommands = new TreeMap<>();

    protected BaseCommand(Pl3xCities plugin, String name, Lang description, String permission, Lang help) {
        super(plugin, name, description, permission, help);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, LinkedList<String> args) {
        if (args.size() > 1) {
            CommandHandler subCmd = subCommands.get(args.pop().toLowerCase());
            if (subCmd != null) {
                if (subCmd.getPermission() == null || PermManager.hasPerm(sender, subCmd.getPermission())) {
                    return subCmd.onTabComplete(sender, command, args);
                }
            }
            return null;
        } else {
            return subCommands.entrySet().stream()
                    .filter(cmdPair -> cmdPair.getKey().startsWith(args.peek().toLowerCase()))
                    .filter(cmdPair -> cmdPair.getValue().getPermission() == null || PermManager.hasPerm(sender, cmdPair.getValue().getPermission()))
                    .map(Map.Entry::getKey).collect(Collectors.toList());
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, LinkedList<String> args) throws CommandException {
        if (sender instanceof Player) {
            if (Config.isWorldDisabled(((Player) sender).getWorld())) {
                new Chat(Lang.WORLD_DISABLED).send(sender);
                return true;
            }
        }
        if (args.size() > 0) {
            CommandHandler subCmd = subCommands.get(args.pop().toLowerCase());
            if (subCmd != null) {
                return subCmd.onCommand(sender, command, args);
            }
            new Chat(Lang.UNKNOWN_SUBCOMMAND).send(sender);
            if (sender instanceof Player) {
                SoundManager.playSoundToPlayer((Player) sender, Config.SOUND_COMMAND_ERROR.getString());
            }
        }
        showSubCommands(sender);
        return true;
    }

    private void showSubCommands(CommandSender sender) {
        new Chat(Lang.AVAILABLE_SUBCOMMANDS).send(sender);
        boolean hasSubCmds = false;
        for (CommandHandler handler : subCommands.values()) {
            if (handler.getPermission() == null || PermManager.hasPerm(sender, handler.getPermission())) {
                new Chat(Lang.COMMAND_DESCRIPTION
                        .replace("{command}", handler.getName())
                        .replace("{description}", handler.getDescription()))
                        .send(sender);
                hasSubCmds = true;
            }
        }
        if (!hasSubCmds) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
            if (sender instanceof Player) {
                SoundManager.playSoundToPlayer((Player) sender, Config.SOUND_COMMAND_ERROR.getString());
            }
        }
    }

    protected void registerSubcommand(CommandHandler commandHandler) {
        subCommands.put(commandHandler.getName().toLowerCase(), commandHandler);
    }
}
