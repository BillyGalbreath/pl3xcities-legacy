package net.pl3x.bukkit.cities.command.city;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.CityConfig;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdCitySetSpawn extends PlayerCommand {
    public CmdCitySetSpawn(Pl3xCities plugin) {
        super(plugin, "setspawn", Lang.CMD_DESC_CITY_SETSPAWN, "cities.command.city.setspawn", Lang.CMD_HELP_CITY_SETSPAWN);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // Show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        // Get the city
        City city = CityManager.getManager().getCity(player.getLocation());
        if (city == null) {
            throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
        }

        // check ownership/permissions
        if (!city.isOwner(player) && !PermManager.hasPerm(player, "cities.override.setspawn")) {
            throw new CommandException(Lang.NOT_CITY_OWNER);
        }

        // set the spawn
        city.setSpawn(player.getLocation());
        CityConfig.SPAWN.setLocation(city.getName(), city.getSpawn());

        // notice to player
        new Chat(Lang.SETSPAWN
                .replace("{city}", city.getName()))
                .send(player);

        // play sound
        SoundManager.playSoundToPlayer(player, Config.SOUND_CITY_SETSPAWN.getString());
    }
}
