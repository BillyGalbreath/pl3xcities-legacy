package net.pl3x.bukkit.cities.event.explode;

import net.pl3x.bukkit.cities.event.ExplodeEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;

import java.util.List;

public class FireballExplodeEvent extends ExplodeEvent {
    public FireballExplodeEvent(Entity entity, List<Block> blocks) {
        super(entity, blocks);
    }
}
