package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class PlayerPlantFlowerPotEvent extends PlayerEvent {
    private final Block pot;

    public PlayerPlantFlowerPotEvent(Player player, Block flowerPot) {
        super(player);
        this.pot = flowerPot;
    }

    public Block getFlowerPot() {
        return pot;
    }
}
