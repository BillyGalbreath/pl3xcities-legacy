package net.pl3x.bukkit.cities.listener;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.event.player.PlayerChunkChangeEvent;
import net.pl3x.bukkit.cities.hook.SUI;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class ChunkParticlesListener implements Listener {
    private final Pl3xCities plugin;

    public ChunkParticlesListener(Pl3xCities plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void playerChangedChunk(PlayerChunkChangeEvent event) {
        Chunk to = event.getChunkTo();
        if (Config.isWorldDisabled(to.getWorld())) {
            return;
        }
        new UpdateChunkVisuals(event.getPlayer(), to).runTaskLaterAsynchronously(plugin, 1);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void playerChangedChunk(PlayerTeleportEvent event) {
        Location loc = event.getTo();
        if (Config.isWorldDisabled(loc.getWorld())) {
            return;
        }
        Chunk to = loc.getChunk();
        Chunk from = event.getFrom().getChunk();
        if (from.getX() == to.getX() && from.getZ() == to.getZ()) {
            return;
        }
        new UpdateChunkVisuals(event.getPlayer(), to).runTaskLaterAsynchronously(plugin, 1);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void playerRespawn(PlayerRespawnEvent event) {
        Location respawn = event.getRespawnLocation();
        if (Config.isWorldDisabled(respawn.getWorld())) {
            return;
        }
        new UpdateChunkVisuals(event.getPlayer(), respawn.getChunk()).runTaskLaterAsynchronously(plugin, 1);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void playerItemHeld(PlayerItemHeldEvent event) {
        Player player = event.getPlayer();
        if (Config.isWorldDisabled(player.getWorld())) {
            return;
        }
        new UpdateChunkVisuals(player, player.getLocation().getChunk()).runTaskLaterAsynchronously(plugin, 1);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void playerSwapHand(PlayerSwapHandItemsEvent event) {
        Player player = event.getPlayer();
        if (Config.isWorldDisabled(player.getWorld())) {
            return;
        }
        new UpdateChunkVisuals(player, player.getLocation().getChunk()).runTaskLaterAsynchronously(plugin, 1);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void playerInventoryClick(InventoryClickEvent event) {
        HumanEntity entity = event.getWhoClicked();
        if (!(entity instanceof Player)) {
            return;
        }
        Player player = (Player) entity;
        if (Config.isWorldDisabled(player.getWorld())) {
            return;
        }
        new UpdateChunkVisuals(player, player.getLocation().getChunk()).runTaskLaterAsynchronously(plugin, 1);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void playerDropItem(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        if (Config.isWorldDisabled(player.getWorld())) {
            return;
        }
        new UpdateChunkVisuals(player, player.getLocation().getChunk()).runTaskLaterAsynchronously(plugin, 1);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void playerPickupItem(PlayerPickupItemEvent event) {
        Player player = event.getPlayer();
        if (Config.isWorldDisabled(player.getWorld())) {
            return;
        }
        new UpdateChunkVisuals(player, player.getLocation().getChunk()).runTaskLaterAsynchronously(plugin, 1);
    }

    private class UpdateChunkVisuals extends BukkitRunnable {
        private final Player player;
        private final Chunk to;

        public UpdateChunkVisuals(Player player, Chunk to) {
            this.player = player;
            this.to = to;
        }

        @Override
        public void run() {
            SUI.showChunk(player, to);
        }
    }
}
