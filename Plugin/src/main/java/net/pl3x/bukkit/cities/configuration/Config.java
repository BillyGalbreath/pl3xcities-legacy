package net.pl3x.bukkit.cities.configuration;

import net.pl3x.bukkit.cities.Pl3xCities;
import org.bukkit.Material;
import org.bukkit.World;

import java.util.List;

public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    SHOW_STACKTRACES(false),
    LANGUAGE_FILE("lang-en.yml"),

    ENABLED_WORLDS(null),
    PROTECT_WILD(null),
    PROTECT_CROPS(true),
    PROTECT_DROPS_TIME(15),
    PREVENT_WITHER_DRAGON_EXPLOIT(true),

    CITY_STARTING_COST(10000.0),
    CITY_EXPANDING_COST(1000.0),
    CITY_UNCLAIM_COST(0.0),
    CITY_DELETION_COST(0.0),

    CITY_NAME_BLACKLIST(null),

    ALLOW_TOUCHING_CITIES(false),

    REGION_WAND("STICK"),

    TITLE_TIME_FADE_IN(10),
    TITLE_TIME_STAY(30),
    TITLE_TIME_FADE_OUT(20),

    PLAY_SOUNDS(true),

    SOUND_COMMAND_ERROR("ENTITY_GHAST_SCREAM"),
    SOUND_CITY_INFO("ENTITY_EXPERIENCE_ORB_PICKUP"),
    SOUND_CITY_CLAIM("ENTITY_PLAYER_LEVELUP"),
    SOUND_CITY_FLAGS_LIST("UI_BUTTON_CLICK"),
    SOUND_CITY_FLAGS_SET("UI_BUTTON_CLICK"),
    SOUND_CITY_MEMBER_LIST("UI_BUTTON_CLICK"),
    SOUND_CITY_MEMBER_ADDED("UI_BUTTON_CLICK"),
    SOUND_CITY_MEMBER_REMOVED("UI_BUTTON_CLICK"),
    SOUND_CITY_OWNER_LIST("UI_BUTTON_CLICK"),
    SOUND_CITY_OWNER_ADDED("UI_BUTTON_CLICK"),
    SOUND_CITY_OWNER_REMOVED("UI_BUTTON_CLICK"),
    SOUND_CITY_PLOTLIMIT_SET("UI_BUTTON_CLICK"),
    SOUND_CITY_PLOTRESTRICTED_LIST("UI_BUTTON_CLICK"),
    SOUND_CITY_PLOTRESTRICTED_ADDED("UI_BUTTON_CLICK"),
    SOUND_CITY_PLOTRESTRICTED_REMOVED("UI_BUTTON_CLICK"),
    SOUND_CITY_SETSPAWN("ENTITY_COW_AMBIENT"),
    SOUND_CITY_UNCLAIM("ENTITY_PLAYER_LEVELUP"),
    SOUND_PLOT_CREATE("ENTITY_PLAYER_LEVELUP"),
    SOUND_PLOT_DELETE("ENTITY_GENERIC_EXPLODE"),
    SOUND_PLOT_INFO("ENTITY_EXPERIENCE_ORB_PICKUP"),
    SOUND_PLOT_FLAGS_LIST("UI_BUTTON_CLICK"),
    SOUND_PLOT_FLAGS_SET("UI_BUTTON_CLICK"),
    SOUND_PLOT_MEMBER_LIST("UI_BUTTON_CLICK"),
    SOUND_PLOT_MEMBER_ADDED("UI_BUTTON_CLICK"),
    SOUND_PLOT_MEMBER_REMOVED("UI_BUTTON_CLICK"),
    SOUND_PLOT_OWNER_LIST("UI_BUTTON_CLICK"),
    SOUND_PLOT_OWNER_ADDED("UI_BUTTON_CLICK"),
    SOUND_PLOT_OWNER_REMOVED("UI_BUTTON_CLICK"),
    SOUND_SPAWN_TELEPORT("ENTITY_ENDERMEN_TELEPORT"),

    VISUAL_CITY_MINE_FILL_COLOR("FUCHSIA"),
    VISUAL_CITY_MINE_EDGE_COLOR("BLUE"),

    VISUAL_CITY_OTHER_FILL_COLOR("RED"),
    VISUAL_CITY_OTHER_EDGE_COLOR("BLUE"),

    VISUAL_WILDERNESS_FILL_COLOR("YELLOW"),
    VISUAL_WILDERNESS_EDGE_COLOR("BLUE"),

    VISUAL_PLOT_FILL_COLOR("#6699CC"),
    VISUAL_PLOT_EDGE_COLOR("#003366"),

    VISUAL_SELECTION_FILL_COLOR("#00FF00"),
    VISUAL_SELECTION_EDGE_COLOR("#FF0000"),

    VISUAL_INHAND_ITEM("PAPER");

    private final Pl3xCities plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = Pl3xCities.getPlugin(Pl3xCities.class);
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public int getInt() {
        return plugin.getConfig().getInt(getKey(), (Integer) def);
    }

    public double getDouble() {
        return plugin.getConfig().getDouble(getKey(), (Double) def);
    }

    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    public List<String> getStringList() {
        return plugin.getConfig().getStringList(getKey());
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }

    public Material getMaterial() {
        return Material.matchMaterial(plugin.getConfig().getString(getKey(), (String) def));
    }

    /**
     * Check if plugin is disabled in world
     *
     * @param world World to check
     * @return True if world is disabled
     */
    public static boolean isWorldDisabled(World world) {
        return !Config.ENABLED_WORLDS.getStringList().contains(world.getName());
    }

    /**
     * Check if wilderness (no city) is unprotected in world
     *
     * @param world World to check
     * @return True if wilderness is NOT protected
     */
    public static boolean isWildNotProtected(World world) {
        return !Config.PROTECT_WILD.getStringList().contains(world.getName());
    }
}
