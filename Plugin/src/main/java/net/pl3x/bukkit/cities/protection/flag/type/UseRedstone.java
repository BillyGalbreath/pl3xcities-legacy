package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.entity.EntityUseRedstoneEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUseRedstoneEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to control redstone usage
 */
public class UseRedstone extends FlagListener {
    private final String name = "use-redstone";

    public UseRedstone(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.DENY);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerUseRedstone(PlayerUseRedstoneEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getBlock().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.USE_REDSTONE_DENY).send(player);
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityUseRedstone(EntityUseRedstoneEvent event) {
        if (Flags.isAllowed(name, null, event.getBlock().getLocation(), false, true)) {
            return;
        }
        event.setCancelled(true);
    }
}
