package net.pl3x.bukkit.cities.command.city;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CmdCitySpawn extends PlayerCommand {
    public CmdCitySpawn(Pl3xCities plugin) {
        super(plugin, "spawn", Lang.CMD_DESC_CITY_SPAWN, "cities.command.city.spawn", Lang.CMD_HELP_CITY_SPAWN);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        List<String> results = new ArrayList<>();
        if (args.size() == 1) {
            results.addAll(CityManager.getManager().getCities().stream()
                    .filter(city -> city.getName().toLowerCase().startsWith(args.peek().toLowerCase()))
                    .map(City::getName).collect(Collectors.toList()));
        }
        return results;
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // Show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        // Get the city
        City city;
        String cityName = args.peek();
        if (cityName != null) {
            city = CityManager.getManager().getCity(cityName);
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_NAME);
            }
        } else {
            city = Pl3xPlayer.getPlayer(player).getCity();
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
            }
        }

        Location from = player.getLocation();
        Location to = city.getSpawn();

        // teleport to spawn
        player.teleport(to);

        // notice to player
        new Chat(Lang.SPAWN
                .replace("{city}", city.getName()))
                .send(player);

        // play sound
        SoundManager.playSoundToWorld(to, Config.SOUND_SPAWN_TELEPORT.getString());
        SoundManager.playSoundToWorld(from, Config.SOUND_SPAWN_TELEPORT.getString());
    }
}
