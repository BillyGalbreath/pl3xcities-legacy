package net.pl3x.bukkit.cities.manager;

import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.PlayerConfig;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class SoundManager {
    public static void playSoundToPlayer(Player player, String sound) {
        if (!Config.PLAY_SOUNDS.getBoolean()) {
            return;
        }
        if (!PlayerConfig.SOUNDS_ENABLED.getBoolean(player.getUniqueId())) {
            return;
        }
        Sound snd;
        try {
            snd = Sound.valueOf(sound);
        } catch (IllegalArgumentException e) {
            return;
        }
        player.playSound(player.getLocation(), snd, 1.0F, 1.0F);
    }

    public static void playSoundToWorld(Location location, String sound) {
        if (!Config.PLAY_SOUNDS.getBoolean()) {
            return;
        }
        Sound snd;
        try {
            snd = Sound.valueOf(sound);
        } catch (IllegalArgumentException e) {
            return;
        }
        location.getWorld().playSound(location, snd, 1.0F, 1.0F);
    }
}
