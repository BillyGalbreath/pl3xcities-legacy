package net.pl3x.bukkit.cities.command.cities;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.BaseCommand;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdCitiesReload extends BaseCommand {
    public CmdCitiesReload(Pl3xCities plugin) {
        super(plugin, "reload", Lang.CMD_DESC_CITIES_RELOAD, "cities.command.reload", Lang.CMD_HELP_CITIES_RELOAD);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, LinkedList<String> args) throws CommandException {
        if ("?".equals(args.peek())) {
            showHelp(sender);
            return true;
        }
        plugin.reload();
        new Chat("&d" + plugin.getName() + " v" + plugin.getDescription().getVersion() + " reloaded.").send(sender);
        return true;
    }
}
