package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.protection.FireSpreadEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBurnEvent;

/*
 * Protection flag to stop fire from spreading
 */
public class FireSpread extends FlagListener {
    private final String name = "fire-spread";

    public FireSpread(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.DENY);
    }

    /*
     * Stops fire from spreading to other blocks
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onFireSpread(FireSpreadEvent event) {
        Block from = event.getFrom();
        Block to = event.getTo();

        if (Flags.isAllowed(name, null, from.getLocation(), false, true)) {
            if (Flags.isAllowed(name, null, to.getLocation(), false, true)) {
                return;
            }
        }
        event.setCancelled(true);
    }

    /*
     * Stops blocks from being destroyed by fire
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockBurn(BlockBurnEvent event) {
        Block block = event.getBlock();
        if (Flags.isAllowed(name, null, block.getLocation(), false, true)) {
            return;
        }
        event.setCancelled(true);
    }
}
