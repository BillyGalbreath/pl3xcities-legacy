package net.pl3x.bukkit.cities.command.plot;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.hook.Vault;
import net.pl3x.bukkit.cities.manager.ChatManager;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.flag.Flag;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

public class CmdPlotInfo extends PlayerCommand {
    public CmdPlotInfo(Pl3xCities plugin) {
        super(plugin, "info", Lang.CMD_DESC_PLOT_INFO, "cities.command.plot.info", Lang.CMD_HELP_PLOT_INFO);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        // get city player standing in
        Location here = player.getLocation();
        City city = CityManager.getManager().getCity(here);
        if (city == null) {
            throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
        }

        // get plot player is standing in
        Plot plot = city.getPlot(here);
        if (plot == null) {
            throw new CommandException(Lang.FAILED_FIND_PLOT_LOCATION);
        }

        // get flags list
        TreeMap<String, String> map = new TreeMap<>();
        for (Flag flag : plot.getFlags()) {
            map.put(flag.getName(), flag.getState().name());
        }
        StringBuilder flags = new StringBuilder();
        for (Entry<String, String> entry : map.entrySet()) {
            if (flags.length() > 0) {
                flags.append("&e, &7");
            }
            flags.append(entry.getKey());
            flags.append("&d: &7");
            flags.append(entry.getValue());
        }
        if (flags.length() == 0) {
            flags.append("&onone");
        }

        // show the plot info
        new Chat("&dPlot: &7" + plot.getId()).send(player);
        ChatManager.sendMessage(player, plot.getUsersTextComponent("&dOwner(s): ", plot.getOwners()));
        ChatManager.sendMessage(player, plot.getUsersTextComponent("&dMember(s): ", plot.getMembers()));
        new Chat("   &dOccupants: &7" + plot.getPopulation()).send(player);
        new Chat("   &dSpawn: &7" + plot.getSpawnCoords()).send(player);
        new Chat("   &dForSale: &7" + (plot.isForSale() ? Lang.WORD_YES + " " + Vault.format(plot.getPrice()) : Lang.WORD_NO)).send(player);
        new Chat("   &dBounds: &7" + plot.getRegion().getBounds()).send(player);
        new Chat("   &dFlags: &7 " + flags.toString()).send(player);

        SoundManager.playSoundToPlayer(player, Config.SOUND_PLOT_INFO.getString());
    }
}
