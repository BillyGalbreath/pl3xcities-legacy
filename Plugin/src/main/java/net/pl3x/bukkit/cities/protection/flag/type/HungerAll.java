package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.player.PlayerHungerEvent;
import net.pl3x.bukkit.cities.event.player.PlayerWalkEvent;
import net.pl3x.bukkit.cities.protection.Protection;
import net.pl3x.bukkit.cities.protection.flag.Flag;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import net.pl3x.bukkit.cities.tasks.StopHungerBarShaking;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerTeleportEvent;

/*
 * Protection flag to stop hunger for anyone in the protection
 */
public class HungerAll extends FlagListener {
    private final String name = "hunger-all";

    public HungerAll(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops hunger for all players
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerHunger(PlayerHungerEvent event) {
        if (shouldCancel(event.getPlayer().getLocation())) {
            event.setCancelled(true);
        }
    }

    /*
     * Stops hunger bar from shaking
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerWalk(PlayerWalkEvent event) {
        if (shouldCancel(event.getTo())) {
            new StopHungerBarShaking(event.getPlayer()).runTask(Pl3xCities.getPlugin(Pl3xCities.class));
        }
    }

    /*
     * Stops hunger bar from shaking
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (shouldCancel(event.getTo())) {
            new StopHungerBarShaking(event.getPlayer()).runTask(Pl3xCities.getPlugin(Pl3xCities.class));
        }
    }

    private boolean shouldCancel(Location location) {
        Protection protection = Protection.getProtection(location);
        if (protection == null) {
            return false; // no protection here
        }

        Flag flag = protection.getFlag(name);
        if (flag == null) {
            return false; // no flag
        }

        if (flag.getState().equals(FlagState.ALLOW)) {
            return false; // allow hunger
        }

        // cancel hunger
        return true;
    }
}
