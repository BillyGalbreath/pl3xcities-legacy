package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("unused")
public class PlayerRenameAnimalEvent extends PlayerEvent {
    private final Entity animal;
    private final ItemStack nametag;

    public PlayerRenameAnimalEvent(Player player, Entity animal, ItemStack nametag) {
        super(player);
        this.animal = animal;
        this.nametag = nametag;
    }

    public Entity getEntity() {
        return animal;
    }

    public Entity getAnimal() {
        return getEntity();
    }

    public ItemStack getNameTag() {
        return nametag;
    }
}
