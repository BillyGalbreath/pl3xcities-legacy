package net.pl3x.bukkit.cities.command.city;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.city.CityBulkClaimEvent;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.hook.Vault;
import net.pl3x.bukkit.cities.manager.ChunkManager;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.CityChunk;
import net.pl3x.bukkit.cities.protection.PlotRegion;
import net.pl3x.bukkit.cities.protection.Selection;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class CmdCityBulkClaim extends PlayerCommand {
    public CmdCityBulkClaim(Pl3xCities plugin) {
        super(plugin, "bulk-claim", Lang.CMD_DESC_CITY_BULKCLAIM, "cities.command.city.bulkclaim", Lang.CMD_HELP_CITY_BULKCLAIM);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // Show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        // check for specified city name in command
        if (args.isEmpty()) {
            throw new CommandException(Lang.CITY_NAME_NOT_SPECIFIED);
        }
        String name = args.pop().trim();

        CityManager cityManager = CityManager.getManager();
        ChunkManager chunkManager = ChunkManager.getManager();

        City city = cityManager.getCity(name);
        if (city == null) {
            throw new CommandException(Lang.FAILED_FIND_CITY_NAME);
        }

        // check if player owns this city
        if (!city.isOwner(player) && !PermManager.hasPerm(player, "cities.override.claimcity")) {
            throw new CommandException(Lang.NOT_CITY_OWNER);
        }

        // check player's selection
        Selection selection = Pl3xPlayer.getPlayer(player).getSelection();
        if (selection == null) {
            throw new CommandException(Lang.SELECTION_NOT_FOUND);
        }

        if (selection.getPrimary() == null) {
            throw new CommandException(Lang.SELECTION_NOT_COMPLETE);
        }
        if (selection.getSecondary() == null) {
            throw new CommandException(Lang.SELECTION_NOT_COMPLETE);
        }

        // get chunks from selection
        Set<CityChunk> chunksToClaim = new PlotRegion(selection).getChunks();

        boolean isTouchingNamedCity = false;
        for (Iterator<CityChunk> iter = chunksToClaim.iterator(); iter.hasNext(); ) {
            CityChunk chunk = iter.next();
            Location chunkLoc = chunk.getLocation();

            // check if chunk is already claimed
            City checkCity = cityManager.getCity(chunkLoc);
            if (checkCity != null) {
                if (checkCity.getName().equals(city.getName())) {
                    iter.remove(); // do not re-claim chunks that are already claimed for this city
                    continue;
                }
                throw new CommandException(Lang.CITY_BULKCLAIM_TOUCHING_OTHER_CITY);
            }

            // check if chunk touches named city
            if (!chunkManager.notTouchingCity(chunkLoc, city.getName())) {
                isTouchingNamedCity = true;
            }

            // check if chunk touches another city
            if (!Config.ALLOW_TOUCHING_CITIES.getBoolean()) {
                if (chunkManager.isChunkTouchingOtherCity(chunk.getLocation(), name)) {
                    throw new CommandException(Lang.CITY_BULKCLAIM_TOUCHING_OTHER_CITY);
                }
            }
        }

        // check if any unclaimed chunks are left in the chunks-to-claim set
        if (chunksToClaim.isEmpty()) {
            throw new CommandException(Lang.CITY_BULKCLAIM_NO_UNCLAIMED_CHUNKS);
        }

        // check if chunk touches named city
        if (!isTouchingNamedCity) {
            throw new CommandException(Lang.CITY_BULKCLAIM_NOT_CONNECTED);
        }

        // check player funds
        double cost = 0;
        if (!player.hasPermission("cities.override.economy.city.claim")) {
            cost = Config.CITY_EXPANDING_COST.getDouble() * chunksToClaim.size();
            double balance = Vault.getBalance(player.getUniqueId());
            if (cost > balance) {
                throw new CommandException(Lang.CANNOT_AFFORD_CITY_BULKCLAIM.replace("{amount}", Vault.format(cost)));
            }
        }

        // call the event and check for cancellation
        CityBulkClaimEvent event = new CityBulkClaimEvent(city, player, chunksToClaim);
        Bukkit.getServer().getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            return;
        }

        // charge the player
        if (!player.hasPermission("cities.override.economy.city.claim")) {
            Vault.withdrawPlayer(player.getUniqueId(), cost);
        }

        // create/expand the actual city
        for (CityChunk chunk : chunksToClaim) {
            chunkManager.claimCityChunk(city, chunk.getLocation());
        }

        new Chat(Lang.CITY_EXPAND_SUCCESS).send(player);
        if (!player.hasPermission("cities.override.economy.city.claim")) {
            new Chat(Lang.VAULT_ACCOUNT_CHARGED
                    .replace("{amount}", Vault.format(cost)))
                    .send(player);
        }

        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        // update current city
        pl3xPlayer.setCity(cityManager.getCity(player.getLocation()));

        // send player sound
        SoundManager.playSoundToPlayer(player, Config.SOUND_CITY_CLAIM.getString());
    }
}
