package net.pl3x.bukkit.cities.event;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class RegionWandLeftClickEvent extends PlayerEvent {
    private final Block block;

    public RegionWandLeftClickEvent(Player player, Block block) {
        super(player);
        this.block = block;
    }

    public Block getClickedBlock() {
        return block;
    }
}
