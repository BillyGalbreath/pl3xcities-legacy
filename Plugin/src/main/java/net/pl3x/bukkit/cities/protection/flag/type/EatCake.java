package net.pl3x.bukkit.cities.protection.flag.type;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.player.PlayerEatCakeEvent;
import net.pl3x.bukkit.cities.hook.ProtocolLib;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import java.lang.reflect.InvocationTargetException;

/*
 * Protection flag to stop players from eating cakes
 */
public class EatCake extends FlagListener {
    private final String name = "eat-cake";

    public EatCake(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.DENY);
    }

    /*
     * Stops players from eating cakes
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerEatCake(PlayerEatCakeEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.eatcake")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getBlock().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.EAT_CAKE_DENY).send(player);
        event.setCancelled(true);

        // Send update health packet to override client hunger bar (hunger still grows, visual glitch)
        PacketContainer packet = new PacketContainer(PacketType.Play.Server.UPDATE_HEALTH);
        packet.getFloat().write(0, (float) player.getHealth());
        packet.getIntegers().write(0, player.getFoodLevel());
        packet.getFloat().write(1, player.getSaturation());

        try {
            ProtocolLib.getManager().sendServerPacket(player, packet);
        } catch (InvocationTargetException ignore) {
        }
    }
}
