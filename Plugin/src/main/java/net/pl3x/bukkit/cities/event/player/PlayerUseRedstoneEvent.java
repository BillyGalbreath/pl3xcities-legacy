package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class PlayerUseRedstoneEvent extends PlayerEvent {
    private final Block redstone;

    public PlayerUseRedstoneEvent(Player player, Block redstone) {
        super(player);
        this.redstone = redstone;
    }

    public Block getBlock() {
        return redstone;
    }
}
