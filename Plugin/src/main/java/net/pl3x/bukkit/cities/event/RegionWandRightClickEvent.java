package net.pl3x.bukkit.cities.event;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class RegionWandRightClickEvent extends PlayerEvent {
    private final Block block;

    public RegionWandRightClickEvent(Player player, Block block) {
        super(player);
        this.block = block;
    }

    public Block getClickedBlock() {
        return block;
    }
}
