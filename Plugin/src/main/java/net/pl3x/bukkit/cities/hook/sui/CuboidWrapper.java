package net.pl3x.bukkit.cities.hook.sui;

import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.PlotRegion;
import net.pl3x.bukkit.cities.protection.Selection;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Cuboid;
import org.bukkit.Location;

public class CuboidWrapper extends Cuboid {
    public CuboidWrapper(Selection selection) {
        setPoints(selection.getPrimary(), selection.getSecondary());
    }

    public CuboidWrapper(Plot plot) {
        PlotRegion region = plot.getRegion();

        if (region == null) {
            return; // blank cuboid
        }

        setPoints(region.getMinPoint(), region.getMaxPoint());
    }

    public void setPoints(Location primary, Location secondary) {
        if (primary == null && secondary == null) {
            return; // blank cuboid
        }

        if (primary == null) {
            primary = secondary;
        }

        if (secondary == null) {
            secondary = primary;
        }

        setPrimary(primary.toVector());
        setSecondary(secondary.toVector());
    }
}
