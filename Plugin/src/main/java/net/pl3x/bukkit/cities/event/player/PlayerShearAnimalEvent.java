package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerShearAnimalEvent extends PlayerEvent {
    private final Entity sheared;

    public PlayerShearAnimalEvent(Player player, Entity sheared) {
        super(player);
        this.sheared = sheared;
    }

    public Entity getEntity() {
        return sheared;
    }

    public Entity getSheared() {
        return getEntity();
    }
}
