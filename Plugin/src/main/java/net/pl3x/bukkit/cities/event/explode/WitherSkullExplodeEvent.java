package net.pl3x.bukkit.cities.event.explode;

import net.pl3x.bukkit.cities.event.ExplodeEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;

import java.util.List;

public class WitherSkullExplodeEvent extends ExplodeEvent {
    public WitherSkullExplodeEvent(Entity entity, List<Block> blocks) {
        super(entity, blocks);
    }
}
