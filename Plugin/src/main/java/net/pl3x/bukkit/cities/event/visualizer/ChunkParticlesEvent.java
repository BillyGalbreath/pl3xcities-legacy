package net.pl3x.bukkit.cities.event.visualizer;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class ChunkParticlesEvent extends PlayerEvent {
    private final Chunk chunk;

    public ChunkParticlesEvent(Player player, Chunk chunk) {
        super(player);
        this.chunk = chunk;
    }

    public Chunk getChunk() {
        return chunk;
    }
}
