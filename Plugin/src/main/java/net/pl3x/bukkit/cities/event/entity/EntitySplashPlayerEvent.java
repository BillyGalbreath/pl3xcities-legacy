package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class EntitySplashPlayerEvent extends EntityEvent {
    private final Player splashed;

    public EntitySplashPlayerEvent(Player splashed, Entity thrower) {
        super(thrower);
        this.splashed = splashed;
    }

    public Player getSplashed() {
        return splashed;
    }

    public Entity getThrower() {
        return getEntity();
    }
}
