package net.pl3x.bukkit.cities.protection;

import org.bukkit.Chunk;
import org.bukkit.Location;

public class Selection implements Cloneable {
    private Location primary;
    private Location secondary;

    public Selection() {
        primary = null;
        secondary = null;
    }

    public Selection(PlotRegion region) {
        primary = region.getMinPoint();
        secondary = region.getMaxPoint();
    }

    public Selection(Chunk chunk) {
        primary = chunk.getBlock(0, 0, 0).getLocation();
        secondary = chunk.getBlock(15, 256, 15).getLocation();
    }

    public Location getPrimary() {
        return primary;
    }

    public Selection setPrimary(Location location) {
        primary = location;
        return this;
    }

    public Location getSecondary() {
        return secondary;
    }

    public Selection setSecondary(Location location) {
        secondary = location;
        return this;
    }

    public void clear() {
        primary = null;
        secondary = null;
    }

    @Override
    public Selection clone() {
        try {
            return (Selection) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }

    @Override
    public String toString() {
        if (primary == null && secondary == null) {
            return "Selection[null]";
        }

        StringBuilder sb = new StringBuilder(128);
        sb.append("Selection[world:");
        sb.append(primary.getWorld().getName());
        sb.append(" ");

        sb.append("primary:[");
        if (primary == null) {
            sb.append("null");
        } else {
            sb.append(primary.getBlockX());
            sb.append(",");
            sb.append(primary.getBlockY());
            sb.append(",");
            sb.append(primary.getBlockZ());
        }
        sb.append("], ");

        sb.append("secondary:[");
        if (secondary == null) {
            sb.append("null");
        } else {
            sb.append(secondary.getBlockX());
            sb.append(",");
            sb.append(secondary.getBlockY());
            sb.append(",");
            sb.append(secondary.getBlockZ());
        }
        sb.append("]");

        sb.append("]");
        return sb.toString();
    }
}
