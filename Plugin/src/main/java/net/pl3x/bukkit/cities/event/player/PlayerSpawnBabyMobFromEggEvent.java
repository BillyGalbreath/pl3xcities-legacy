package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerSpawnBabyMobFromEggEvent extends PlayerEvent {
    private final Entity mob;

    public PlayerSpawnBabyMobFromEggEvent(Player player, Entity mob) {
        super(player);
        this.mob = mob;
    }

    public Entity getEntity() {
        return mob;
    }

    public Entity getMob() {
        return getEntity();
    }
}
