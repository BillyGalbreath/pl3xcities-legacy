package net.pl3x.bukkit.cities.exception;

public class VaultException extends RuntimeException {
    public VaultException(String str) {
        super(str);
    }
}
