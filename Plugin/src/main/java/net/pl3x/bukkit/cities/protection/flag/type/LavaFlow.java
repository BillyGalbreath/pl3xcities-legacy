package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockFromToEvent;

/*
 * Protection flag to stop lava from flowing
 */
public class LavaFlow extends FlagListener {
    private final String name = "lava-flow";

    public LavaFlow(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onLavaFlow(BlockFromToEvent event) {
        Block block = event.getBlock();
        if (!block.isLiquid()) {
            return;
        }
        Material type = block.getType();
        if (!type.equals(Material.STATIONARY_LAVA) && !type.equals(Material.LAVA)) {
            return;
        }
        if (Flags.isAllowed(name, null, block.getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
