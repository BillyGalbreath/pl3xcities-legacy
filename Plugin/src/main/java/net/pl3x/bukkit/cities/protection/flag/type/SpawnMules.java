package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.entity.MobSpawnEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to stop mules from spawning
 */
public class SpawnMules extends FlagListener {
    private final String name = "spawn-mules";

    public SpawnMules(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onMobSpawn(MobSpawnEvent event) {
        if (event.getEntity().getType() != EntityType.HORSE) {
            return;
        }
        if (((Horse) event.getEntity()).getVariant() != Horse.Variant.MULE) {
            return;
        }
        if (Flags.isAllowed(name, null, event.getLocation(), false, false)) {
            event.setCancelled(false); // override spawn-animals flag
        }
        event.setCancelled(true);
    }
}
