package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("unused")
public class PlayerFeedAnimalEvent extends PlayerEvent {
    private final Entity animal;
    private final ItemStack food;

    public PlayerFeedAnimalEvent(Player player, Entity animal, ItemStack food) {
        super(player);
        this.animal = animal;
        this.food = food;
    }

    public Entity getEntity() {
        return animal;
    }

    public Entity getAnimal() {
        return getEntity();
    }

    public ItemStack getFood() {
        return food;
    }
}
