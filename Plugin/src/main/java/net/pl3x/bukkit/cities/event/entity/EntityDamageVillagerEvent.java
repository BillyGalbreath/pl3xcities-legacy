package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Villager;

@SuppressWarnings("unused")
public class EntityDamageVillagerEvent extends EntityEvent {
    private final Entity villager;

    public EntityDamageVillagerEvent(Entity villager, Entity damager) {
        super(damager);
        this.villager = villager;
    }

    public Entity getDamaged() {
        return villager;
    }

    public Entity getDamager() {
        return getEntity();
    }

    public Villager getVillager() {
        return (Villager) villager;
    }
}
