package net.pl3x.bukkit.cities.command;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.cities.CmdCitiesReload;
import net.pl3x.bukkit.cities.command.cities.CmdCitiesSounds;
import net.pl3x.bukkit.cities.configuration.Lang;

public class CmdCities extends BaseCommand {
    public CmdCities(Pl3xCities plugin) {
        super(plugin, "cities", Lang.CMD_DESC_CITIES, "cities.command.cities", null);
        registerSubcommand(new CmdCitiesReload(plugin));
        registerSubcommand(new CmdCitiesSounds(plugin));
    }
}
