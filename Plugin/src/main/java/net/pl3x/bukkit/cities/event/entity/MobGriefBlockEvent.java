package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;

public class MobGriefBlockEvent extends EntityEvent {
    private final Block block;

    public MobGriefBlockEvent(Entity entity, Block block) {
        super(entity);
        this.block = block;
    }

    /**
     * Get the block affected by this event
     *
     * @return Affected block
     */
    public Block getBlock() {
        return block;
    }
}
