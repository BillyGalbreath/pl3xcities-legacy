package net.pl3x.bukkit.cities.event.visualizer;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import net.pl3x.bukkit.cities.protection.Plot;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlotParticlesEvent extends PlayerEvent {
    private final Plot plot;

    public PlotParticlesEvent(Player player, Plot plot) {
        super(player);
        this.plot = plot;
    }

    public Plot getPlot() {
        return plot;
    }
}
