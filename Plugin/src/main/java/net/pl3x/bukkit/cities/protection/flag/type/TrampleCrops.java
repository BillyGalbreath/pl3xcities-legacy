package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.event.entity.MobGriefBlockEvent;
import net.pl3x.bukkit.cities.event.player.PlayerTrampleCropEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to control trampling of crops and soil
 */
public class TrampleCrops extends FlagListener {
    private final String name = "trample-crops";

    public TrampleCrops(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.DENY);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerTrampleCrop(PlayerTrampleCropEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.trample")) {
            return;
        }
        if (Config.PROTECT_CROPS.getBoolean()) {
            event.setCancelled(true); // crops are protected regardless of location
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getBlock().getLocation())) {
            return;
        }
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onMobTrampleCrop(MobGriefBlockEvent event) {
        Material material = event.getBlock().getType();

        if (!material.equals(Material.CROPS) && !material.equals(Material.SOIL)) {
            return; // not trampling crops. ignore.
        }

        if (Config.PROTECT_CROPS.getBoolean()) {
            event.setCancelled(true); // crops are protected regardless of location
            return;
        }

        if (Flags.isAllowed(name, null, event.getBlock().getLocation())) {
            return;
        }
        event.setCancelled(true);
    }
}
