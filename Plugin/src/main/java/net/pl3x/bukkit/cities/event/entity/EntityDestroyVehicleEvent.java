package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Vehicle;

public class EntityDestroyVehicleEvent extends EntityEvent {
    private final Vehicle vehicle;

    public EntityDestroyVehicleEvent(Entity entity, Vehicle vehicle) {
        super(entity);
        this.vehicle = vehicle;
    }

    public Entity getVehicle() {
        return vehicle;
    }
}
