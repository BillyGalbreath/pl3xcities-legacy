package net.pl3x.bukkit.cities.protection;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Plot extends Protection {
    private final String id; // cityname_000001
    private final PlotRegion region;

    public Plot(String id, PlotRegion region) {
        this.id = id;
        this.region = region;
    }

    public String getId() {
        return id;
    }

    public PlotRegion getRegion() {
        return region;
    }

    public int getPopulation() {
        Set<UUID> population = new HashSet<>(getOwners().keySet());
        try {
            population.addAll(getMembers().keySet());
        } catch (Exception ignore) {
        }
        return population.size();
    }
}
