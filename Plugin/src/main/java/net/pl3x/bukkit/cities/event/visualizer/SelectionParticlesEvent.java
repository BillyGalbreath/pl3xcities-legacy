package net.pl3x.bukkit.cities.event.visualizer;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import net.pl3x.bukkit.cities.protection.Selection;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class SelectionParticlesEvent extends PlayerEvent {
    private final Selection selection;

    public SelectionParticlesEvent(Player player, Selection selection) {
        super(player);
        this.selection = selection;
    }

    public Selection getSelection() {
        return selection;
    }
}
