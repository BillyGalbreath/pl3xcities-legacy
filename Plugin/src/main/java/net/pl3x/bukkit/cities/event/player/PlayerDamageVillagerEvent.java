package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

@SuppressWarnings("unused")
public class PlayerDamageVillagerEvent extends PlayerEvent {
    private final Entity villager;

    public PlayerDamageVillagerEvent(Entity villager, Player damager) {
        super(damager);
        this.villager = villager;
    }

    public Entity getEntity() {
        return villager;
    }

    public Entity getDamaged() {
        return getEntity();
    }

    public Player getDamager() {
        return getPlayer();
    }

    public Villager getVillager() {
        return (Villager) villager;
    }
}
