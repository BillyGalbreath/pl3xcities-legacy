package net.pl3x.bukkit.cities.protection.flag;

import net.pl3x.bukkit.cities.Pl3xCities;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

@SuppressWarnings("WeakerAccess")
public abstract class FlagListener implements Listener {
    protected final Pl3xCities plugin;

    public FlagListener(Pl3xCities plugin) {
        this.plugin = plugin;

        Bukkit.getPluginManager().registerEvents(this, plugin);
    }
}
