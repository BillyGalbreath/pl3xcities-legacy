package net.pl3x.bukkit.cities.command;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

public abstract class PlayerCommand extends CommandHandler {
    protected PlayerCommand(Pl3xCities plugin, String name, Lang description, String permission, Lang help) {
        super(plugin, name, description, permission, help);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, LinkedList<String> args) {
        if (sender instanceof Player) {
            return onTabComplete((Player) sender, args);
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, LinkedList<String> args) throws CommandException {
        if (!(sender instanceof Player)) {
            new Chat(Lang.PLAYER_COMMAND).send(sender);
            return true;
        }
        Player player = (Player) sender;
        if ("?".equals(args.peek())) {
            showHelp(player);
            return true;
        }
        onCommand(player, args);
        return true;
    }

    public abstract List<String> onTabComplete(Player player, LinkedList<String> args);

    public abstract void onCommand(Player player, LinkedList<String> args) throws CommandException;
}
