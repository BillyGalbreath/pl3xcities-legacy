package net.pl3x.bukkit.cities.event.city;

import net.pl3x.bukkit.cities.event.CityEvent;
import net.pl3x.bukkit.cities.protection.City;
import org.bukkit.Location;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class CityClaimEvent extends CityEvent {
    private final Location location;
    private final boolean expanding;

    public CityClaimEvent(City city, Player player, Location location, boolean expanding) {
        super(city, player);
        this.location = location;
        this.expanding = expanding;
    }

    public Location getLocation() {
        return location;
    }

    public boolean isExpanding() {
        return expanding;
    }
}
