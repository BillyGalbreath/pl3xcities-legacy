package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.player.PlayerHungerEvent;
import net.pl3x.bukkit.cities.event.player.PlayerWalkEvent;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.flag.Flag;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import net.pl3x.bukkit.cities.tasks.StopHungerBarShaking;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerTeleportEvent;

/*
 * Protection flag to stop hunger for protection residents
 */
public class HungerResidents extends FlagListener {
    private final String name = "hunger-residents";

    public HungerResidents(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops hunger for protection residents
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerHunger(PlayerHungerEvent event) {
        Player player = event.getPlayer();
        if (shouldCancel(player, player.getLocation())) {
            event.setCancelled(true);
        }
    }

    /*
     * Stops hunger bar from shaking for members
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerWalk(PlayerWalkEvent event) {
        Player player = event.getPlayer();
        if (shouldCancel(player, event.getTo())) {
            new StopHungerBarShaking(player).runTask(Pl3xCities.getPlugin(Pl3xCities.class));
        }
    }

    /*
     * Stops hunger bar from shaking for members
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        Player player = event.getPlayer();
        if (shouldCancel(player, event.getTo())) {
            new StopHungerBarShaking(player).runTask(Pl3xCities.getPlugin(Pl3xCities.class));
        }
    }

    private boolean shouldCancel(Player player, Location location) {
        City city = CityManager.getManager().getCity(location);
        if (city == null) {
            return false; // not in a city
        }

        if (!city.getResidents().contains(player.getUniqueId())) {
            return false; // not a city resident
        }

        Flag flag = city.getFlag(name);
        if (flag != null && flag.getState().equals(FlagState.DENY)) {
            return true; // city is denying hunger for all city residents
        }

        Plot plot = city.getPlot(location);
        if (plot != null) {
            flag = plot.getFlag(name);
            if (flag != null && flag.getState().equals(FlagState.DENY)) {
                return true; // plot is denying hunger for all city residents
            }
        }

        // flag not set
        return false;
    }
}
