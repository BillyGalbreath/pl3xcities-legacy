package net.pl3x.bukkit.cities.protection.flag;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.flag.type.Build;
import net.pl3x.bukkit.cities.protection.flag.type.EatCake;
import net.pl3x.bukkit.cities.protection.flag.type.EnderPearls;
import net.pl3x.bukkit.cities.protection.flag.type.Entry;
import net.pl3x.bukkit.cities.protection.flag.type.Exit;
import net.pl3x.bukkit.cities.protection.flag.type.Explosions;
import net.pl3x.bukkit.cities.protection.flag.type.FallDamage;
import net.pl3x.bukkit.cities.protection.flag.type.FireSpread;
import net.pl3x.bukkit.cities.protection.flag.type.Fishing;
import net.pl3x.bukkit.cities.protection.flag.type.HungerAll;
import net.pl3x.bukkit.cities.protection.flag.type.HungerMembers;
import net.pl3x.bukkit.cities.protection.flag.type.HungerOwners;
import net.pl3x.bukkit.cities.protection.flag.type.HungerResidents;
import net.pl3x.bukkit.cities.protection.flag.type.HurtAnimals;
import net.pl3x.bukkit.cities.protection.flag.type.HurtMobs;
import net.pl3x.bukkit.cities.protection.flag.type.HurtVillagers;
import net.pl3x.bukkit.cities.protection.flag.type.IceForm;
import net.pl3x.bukkit.cities.protection.flag.type.IceMelt;
import net.pl3x.bukkit.cities.protection.flag.type.LavaFire;
import net.pl3x.bukkit.cities.protection.flag.type.LavaFlow;
import net.pl3x.bukkit.cities.protection.flag.type.LeafDecay;
import net.pl3x.bukkit.cities.protection.flag.type.Lighter;
import net.pl3x.bukkit.cities.protection.flag.type.Lightning;
import net.pl3x.bukkit.cities.protection.flag.type.MobDamage;
import net.pl3x.bukkit.cities.protection.flag.type.MobGriefing;
import net.pl3x.bukkit.cities.protection.flag.type.Pistons;
import net.pl3x.bukkit.cities.protection.flag.type.Pvp;
import net.pl3x.bukkit.cities.protection.flag.type.Sleep;
import net.pl3x.bukkit.cities.protection.flag.type.SnowForm;
import net.pl3x.bukkit.cities.protection.flag.type.SnowMelt;
import net.pl3x.bukkit.cities.protection.flag.type.SoilDry;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnAnimals;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnBats;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnBlazes;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnCaveSpiders;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnChickens;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnCows;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnCreepers;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnDonkeys;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnEndermen;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnEndermites;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnGhasts;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnGuardians;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnHorses;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnIronGolems;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnMagmaCubes;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnMobs;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnMooshrooms;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnMules;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnOcelots;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnPigZombies;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnPigs;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnRabbits;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnSheep;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnSilverfish;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnSkeletons;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnSlimes;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnSnowmen;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnSpiders;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnSquids;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnVillagers;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnWitches;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnWitherSkeletons;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnWolves;
import net.pl3x.bukkit.cities.protection.flag.type.SpawnZombies;
import net.pl3x.bukkit.cities.protection.flag.type.SplashPotion;
import net.pl3x.bukkit.cities.protection.flag.type.TrampleCrops;
import net.pl3x.bukkit.cities.protection.flag.type.UseAnimals;
import net.pl3x.bukkit.cities.protection.flag.type.UseChests;
import net.pl3x.bukkit.cities.protection.flag.type.UseDoors;
import net.pl3x.bukkit.cities.protection.flag.type.UseMobs;
import net.pl3x.bukkit.cities.protection.flag.type.UseRedstone;
import net.pl3x.bukkit.cities.protection.flag.type.UseVillagers;
import net.pl3x.bukkit.cities.protection.flag.type.VehicleBreak;
import net.pl3x.bukkit.cities.protection.flag.type.VehiclePlace;
import net.pl3x.bukkit.cities.protection.flag.type.WaterFlow;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.UUID;

@SuppressWarnings("WeakerAccess")
public class Flags {
    private static final HashMap<String, FlagState> flags = new HashMap<>();

    /**
     * Registers known flag with default state
     *
     * @param name  Flag's name
     * @param state Flag's default state
     */
    public static void registerFlag(String name, FlagState state) {
        flags.put(name.toLowerCase(), state);
    }

    /**
     * Gets the known flags
     *
     * @return HashMap of flag names with default states
     */
    public static HashMap<String, FlagState> getFlags() {
        return flags;
    }

    public static FlagState getDefaultState(String name) {
        return flags.get(name);
    }

    /**
     * Creates a new flag to be saved to a protection
     *
     * @param name  Flag's name
     * @param state Flag's current state
     * @return The new Flag
     */
    public static Flag newFlag(String name, FlagState state) {
        if (!getFlags().containsKey(name.toLowerCase())) {
            return null;
        }
        return new Flag(name, state);
    }

    /**
     * Check the state of flag for player at location. Does not check owner/member lists if location is in a protection. Does not check wilderness.
     *
     * @param name     Name of flag
     * @param uuid     UUID of player to check
     * @param location Location to check
     * @return Allow or Deny state for player at location
     */
    public static boolean isAllowed(String name, UUID uuid, Location location) {
        return isAllowed(name, uuid, location, false, false);
    }

    /**
     * Check the state of flag for player at location.
     *
     * @param name           Name of flag
     * @param uuid           UUID of player to check
     * @param location       Location to check
     * @param checkUserLists True if you want to allow if player is on owner/member list of protection
     * @param checkWild      True if you want to check the wilderness (wilderness protection can be enabled in config.yml)
     * @return Allow or Deny state for player at location
     */
    public static boolean isAllowed(String name, UUID uuid, Location location, boolean checkUserLists, boolean checkWild) {
        // Check if plugin is enabled on this world
        if (Config.isWorldDisabled(location.getWorld())) {
            return true; // world not enabled for cities
        }

        // Check for wilderness
        City city = CityManager.getManager().getCity(location);
        if (city == null) {
            return !checkWild || Config.isWildNotProtected(location.getWorld());
        }

        // Get the default flag state
        FlagState defaultState = Flags.getDefaultState(name);
        if (defaultState == null) {
            // sanity check. this should never happen.
            defaultState = FlagState.DENY;
        }

        // Check plot first
        Plot plot = city.getPlot(location);
        if (plot != null) {
            // Check plot user lists
            if (checkUserLists && (plot.isOwner(uuid) || plot.isMember(uuid))) {
                return true;
            }

            // Check plot flags (plot overrides city)
            Flag flag = plot.getFlag(name);
            if (flag != null) {
                return flag.getState().equals(FlagState.ALLOW);
            }

            // Check for city flag (plot inherits from city)
            flag = city.getFlag(name);
            if (flag != null) {
                return flag.getState().equals(FlagState.ALLOW);
            }

            // no plot or city flags found, return default state
            return defaultState.equals(FlagState.ALLOW);
        }

        // Check city user lists
        if (checkUserLists && (city.isOwner(uuid) || city.isMember(uuid))) {
            return true;
        }

        // Finally check city flags
        Flag flag = city.getFlag(name);
        if (flag != null) {
            return flag.getState().equals(FlagState.ALLOW);
        }

        // No city flags found, return default state
        return defaultState.equals(FlagState.ALLOW);
    }

    public static void load(Pl3xCities plugin) {
        new Build(plugin);
        new EatCake(plugin);
        new EnderPearls(plugin);
        new Entry(plugin);
        new Exit(plugin);
        new Explosions(plugin);
        new Fishing(plugin);
        new FallDamage(plugin);
        new FireSpread(plugin);
        new HungerAll(plugin);
        new HungerMembers(plugin);
        new HungerOwners(plugin);
        new HungerResidents(plugin);
        new HurtAnimals(plugin);
        new HurtMobs(plugin);
        new HurtVillagers(plugin);
        new IceForm(plugin);
        new IceMelt(plugin);
        new LavaFire(plugin);
        new LavaFlow(plugin);
        new LeafDecay(plugin);
        new Lighter(plugin);
        new Lightning(plugin);
        new MobDamage(plugin);
        new MobGriefing(plugin);
        new Pistons(plugin);
        new Pvp(plugin);
        new Sleep(plugin);
        new SnowForm(plugin);
        new SnowMelt(plugin);
        new SoilDry(plugin);
        new SpawnAnimals(plugin);
        new SpawnBats(plugin);
        new SpawnBlazes(plugin);
        new SpawnCaveSpiders(plugin);
        new SpawnChickens(plugin);
        new SpawnCows(plugin);
        new SpawnCreepers(plugin);
        new SpawnDonkeys(plugin);
        new SpawnEndermen(plugin);
        new SpawnEndermites(plugin);
        new SpawnGhasts(plugin);
        new SpawnGuardians(plugin);
        new SpawnHorses(plugin);
        new SpawnIronGolems(plugin);
        new SpawnMagmaCubes(plugin);
        new SpawnMobs(plugin);
        new SpawnMooshrooms(plugin);
        new SpawnMules(plugin);
        new SpawnOcelots(plugin);
        new SpawnPigs(plugin);
        new SpawnPigZombies(plugin);
        new SpawnRabbits(plugin);
        new SpawnSheep(plugin);
        new SpawnSilverfish(plugin);
        new SpawnSkeletons(plugin);
        new SpawnSlimes(plugin);
        new SpawnSnowmen(plugin);
        new SpawnSpiders(plugin);
        new SpawnSquids(plugin);
        new SpawnVillagers(plugin);
        new SpawnWitches(plugin);
        new SpawnWitherSkeletons(plugin);
        new SpawnWolves(plugin);
        new SpawnZombies(plugin);
        new SplashPotion(plugin);
        new TrampleCrops(plugin);
        new UseAnimals(plugin);
        new UseChests(plugin);
        new UseDoors(plugin);
        new UseMobs(plugin);
        new UseRedstone(plugin);
        new UseVillagers(plugin);
        new VehicleBreak(plugin);
        new VehiclePlace(plugin);
        new WaterFlow(plugin);
    }
}
