package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.entity.Entity;


@SuppressWarnings("unused")
public class EntityDamageMobEvent extends EntityEvent {
    private final Entity damaged;

    public EntityDamageMobEvent(Entity damaged, Entity damager) {
        super(damager);
        this.damaged = damaged;
    }

    public Entity getDamaged() {
        return damaged;
    }

    public Entity getDamager() {
        return getEntity();
    }
}
