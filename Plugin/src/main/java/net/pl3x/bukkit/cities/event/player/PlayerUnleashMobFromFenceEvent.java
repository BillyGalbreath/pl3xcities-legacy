package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerUnleashMobFromFenceEvent extends PlayerEvent {
    private final Entity mob;
    private final Location fence;

    public PlayerUnleashMobFromFenceEvent(Player player, Entity mob, Location fence) {
        super(player);
        this.mob = mob;
        this.fence = fence;
    }

    public Entity getMob() {
        return mob;
    }

    public Entity getEntity() {
        return getMob();
    }

    public Entity getRemoved() {
        return getMob();
    }

    public Player getRemover() {
        return getPlayer();
    }

    public Location getFenceLocation() {
        return fence;
    }
}
