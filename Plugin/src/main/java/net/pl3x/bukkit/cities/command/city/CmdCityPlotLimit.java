package net.pl3x.bukkit.cities.command.city;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.CityConfig;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdCityPlotLimit extends PlayerCommand {
    public CmdCityPlotLimit(Pl3xCities plugin) {
        super(plugin, "plot-limit", Lang.CMD_DESC_CITY_PLOTLIMIT, "cities.command.city.plotlimit", Lang.CMD_HELP_CITY_PLOTLIMIT);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        // get the city
        City city = CityManager.getManager().getCity(player.getLocation());
        if (city == null) {
            throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
        }

        // check ownership/permissions
        if (!city.isOwner(player) && !PermManager.hasPerm(player, "cities.override.setplotlimit")) {
            throw new CommandException(Lang.NOT_CITY_OWNER);
        }

        // get the limit
        int limit;
        try {
            limit = Integer.valueOf(args.peek());
        } catch (NumberFormatException e) {
            throw new CommandException(Lang.NOT_A_NUMBER);
        }

        // set the limit
        city.setPlotLimit(limit);
        CityConfig.PLOTLIMIT.set(city.getName(), city.getPlotLimit());

        // notify player of success
        new Chat(Lang.CITY_PLOTLIMIT_SET).send(player);

        // send player sound
        SoundManager.playSoundToPlayer(player, Config.SOUND_CITY_PLOTLIMIT_SET.getString());
    }
}
