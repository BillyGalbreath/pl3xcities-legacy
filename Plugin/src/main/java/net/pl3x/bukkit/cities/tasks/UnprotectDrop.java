package net.pl3x.bukkit.cities.tasks;

import net.pl3x.bukkit.cities.Pl3xCities;
import org.bukkit.entity.Item;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

public class UnprotectDrop extends BukkitRunnable {
    private final Item item;

    public UnprotectDrop(Item item) {
        this.item = item;
    }

    @Override
    public void run() {
        item.setMetadata("owner", new FixedMetadataValue(Pl3xCities.getPlugin(Pl3xCities.class), "none"));
    }
}
