package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.Location;
import org.bukkit.entity.Entity;

@SuppressWarnings("unused")
public class EntityUnleashAnimalFromFenceEvent extends EntityEvent {
    private final Entity animal;
    private final Location fence;

    public EntityUnleashAnimalFromFenceEvent(Entity entity, Entity animal, Location fence) {
        super(entity);
        this.animal = animal;
        this.fence = fence;
    }

    public Entity getAnimal() {
        return animal;
    }

    public Entity getRemoved() {
        return getAnimal();
    }

    public Entity getRemover() {
        return getEntity();
    }

    public Location getFenceLocation() {
        return fence;
    }
}
