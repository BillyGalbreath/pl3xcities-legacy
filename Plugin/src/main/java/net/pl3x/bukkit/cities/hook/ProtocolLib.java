package net.pl3x.bukkit.cities.hook;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;

public class ProtocolLib {
    private static ProtocolManager protocolManager;

    public static ProtocolManager getManager() {
        if (protocolManager == null) {
            protocolManager = ProtocolLibrary.getProtocolManager();
        }
        return protocolManager;
    }
}
