package net.pl3x.bukkit.cities.tasks;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RefreshChunks extends BukkitRunnable {
    private final Set<Chunk> chunks = new HashSet<>();

    public RefreshChunks(List<Block> blocks) {
        chunks.addAll(blocks.stream()
                .map(Block::getChunk).collect(Collectors.toList()));
    }

    public RefreshChunks(Location location) {
        chunks.add(location.getChunk());
    }

    @Override
    public void run() {
        for (Chunk chunk : chunks) {
            // noinspection deprecation - Bukkit note: refreshChunk() is not guaranteed to work across all client implementations
            chunk.getWorld().refreshChunk(chunk.getX(), chunk.getZ());
        }
    }
}
