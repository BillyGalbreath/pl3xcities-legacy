package net.pl3x.bukkit.cities.manager;

import net.pl3x.bukkit.cities.hook.Vault;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PermManager {
    private static Boolean hasVault = null;

    private static boolean hasVault() {
        if (hasVault == null) {
            hasVault = Vault.setupPermissions();
        }
        return hasVault;
    }

    public static boolean hasPerm(CommandSender sender, String node) {
        if (hasVault()) {
            return Vault.getPermission().has(sender, node);
        }
        return sender.hasPermission(node);
    }

    public static boolean hasPerm(OfflinePlayer target, String node) {
        if (hasVault()) {
            return Vault.getPermission().playerHas(null, target, node);
        }
        Player player = target.getPlayer();
        return player != null && player.hasPermission(node);
    }

    public static boolean hasPerm(Player player, String node) {
        if (hasVault()) {
            return Vault.getPermission().has(player, node);
        }
        return player.hasPermission(node);
    }
}
