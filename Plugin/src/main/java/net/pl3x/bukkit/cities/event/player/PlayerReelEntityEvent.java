package net.pl3x.bukkit.cities.event.player;

import org.bukkit.entity.Entity;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Player;

public class PlayerReelEntityEvent extends PlayerFishingEvent {
    private final Entity reeled;

    public PlayerReelEntityEvent(Player player, Entity reeled, FishHook hook) {
        super(player, hook);
        this.reeled = reeled;
    }

    public Entity getEntity() {
        return reeled;
    }

    public Entity getReeled() {
        return getEntity();
    }
}
