package net.pl3x.bukkit.cities.event;

import net.pl3x.bukkit.cities.protection.City;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public abstract class CityEvent extends PlayerEvent {
    private final City city;

    public CityEvent(City city, Player player) {
        super(player);
        this.city = city;
    }

    public City getCity() {
        return city;
    }
}
