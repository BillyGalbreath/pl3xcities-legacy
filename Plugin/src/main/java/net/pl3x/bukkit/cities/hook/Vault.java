package net.pl3x.bukkit.cities.hook;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.permission.Permission;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.VaultException;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.UUID;

@SuppressWarnings("WeakerAccess")
public class Vault {
    private static Economy economy = null;
    private static Permission permission = null;

    public static Economy getEconomy() {
        return economy;
    }

    public static boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }
        return (economy != null);
    }

    public static double getBalance(Player player) {
        return getBalance(player.getUniqueId());
    }

    public static double getBalance(UUID uuid) {
        return economy.getBalance(Bukkit.getOfflinePlayer(uuid));
    }

    public static String format(double amount) {
        return economy.format(amount);
    }

    public static void withdrawPlayer(UUID uuid, double amount) throws VaultException {
        EconomyResponse response = getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(uuid), amount);
        if (!response.transactionSuccess()) {
            throw new VaultException(Lang.VAULT_WITHDRAW_ERROR.replace("{error}", response.errorMessage));
        }
    }

    public static void depositPlayer(UUID uuid, double amount) throws VaultException {
        EconomyResponse response = getEconomy().depositPlayer(Bukkit.getOfflinePlayer(uuid), amount);
        if (!response.transactionSuccess()) {
            throw new VaultException(Lang.VAULT_DEPOSIT_ERROR.replace("{error}", response.errorMessage));
        }
    }

    public static boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServicesManager().getRegistration(Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }

    public static Permission getPermission() {
        if (permission == null) {
            setupPermissions();
        }
        return permission;
    }
}
