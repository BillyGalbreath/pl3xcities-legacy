package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Player;

public class PlayerDamagePlayerEvent extends PlayerEvent {
    private final Player damaged;

    public PlayerDamagePlayerEvent(Player damaged, Player damager) {
        super(damager);
        this.damaged = damaged;
    }

    public Player getDamaged() {
        return damaged;
    }

    public Player getDamager() {
        return getPlayer();
    }
}
