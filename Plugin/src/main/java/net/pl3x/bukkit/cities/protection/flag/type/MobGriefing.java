package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.entity.EntityDestroyVehicleEvent;
import net.pl3x.bukkit.cities.event.entity.MobGriefBlockEvent;
import net.pl3x.bukkit.cities.event.explode.CreeperExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.FireballExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.WitherExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.WitherSkullExplodeEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import java.util.List;

/*
 * Protection flag to stop mobs from griefing the world
 */
public class MobGriefing extends FlagListener {
    private final String name = "mob-griefing";

    public MobGriefing(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.DENY);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onWitherExplode(WitherExplodeEvent event) {
        if (cancelExplodeEvent(event.getBlocks())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onWitherSkullExplode(WitherSkullExplodeEvent event) {
        if (cancelExplodeEvent(event.getBlocks())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onCreeperExplode(CreeperExplodeEvent event) {
        if (cancelExplodeEvent(event.getBlocks())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onFireballExplode(FireballExplodeEvent event) {
        if (cancelExplodeEvent(event.getBlocks())) {
            event.setCancelled(true);
        }
    }

    private boolean cancelExplodeEvent(List<Block> blocks) {
        for (Block block : blocks) {
            if (!Flags.isAllowed(name, null, block.getLocation(), false, true)) {
                return true;
            }
        }
        return false;
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onGriefBlock(MobGriefBlockEvent event) {
        if (Flags.isAllowed(name, null, event.getBlock().getLocation(), false, true)) {
            return;
        }
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityDestroyVehicle(EntityDestroyVehicleEvent event) {
        if (Flags.isAllowed(name, null, event.getVehicle().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
