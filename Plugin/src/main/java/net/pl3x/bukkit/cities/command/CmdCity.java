package net.pl3x.bukkit.cities.command;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.city.CmdCityBulkClaim;
import net.pl3x.bukkit.cities.command.city.CmdCityClaim;
import net.pl3x.bukkit.cities.command.city.CmdCityFlags;
import net.pl3x.bukkit.cities.command.city.CmdCityInfo;
import net.pl3x.bukkit.cities.command.city.CmdCityMembers;
import net.pl3x.bukkit.cities.command.city.CmdCityOwners;
import net.pl3x.bukkit.cities.command.city.CmdCityPlotLimit;
import net.pl3x.bukkit.cities.command.city.CmdCityPlotRestricted;
import net.pl3x.bukkit.cities.command.city.CmdCitySetSpawn;
import net.pl3x.bukkit.cities.command.city.CmdCitySpawn;
import net.pl3x.bukkit.cities.command.city.CmdCityUnclaim;
import net.pl3x.bukkit.cities.configuration.Lang;

public class CmdCity extends BaseCommand {
    public CmdCity(Pl3xCities plugin) {
        super(plugin, "city", Lang.CMD_DESC_CITY, "cities.command.city", null);
        registerSubcommand(new CmdCityBulkClaim(plugin));
        registerSubcommand(new CmdCityClaim(plugin));
        registerSubcommand(new CmdCityFlags(plugin));
        registerSubcommand(new CmdCityInfo(plugin));
        registerSubcommand(new CmdCityMembers(plugin));
        registerSubcommand(new CmdCityOwners(plugin));
        registerSubcommand(new CmdCityPlotLimit(plugin));
        registerSubcommand(new CmdCityPlotRestricted(plugin));
        registerSubcommand(new CmdCitySetSpawn(plugin));
        registerSubcommand(new CmdCitySpawn(plugin));
        registerSubcommand(new CmdCityUnclaim(plugin));
    }
}
