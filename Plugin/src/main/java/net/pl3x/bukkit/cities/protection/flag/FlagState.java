package net.pl3x.bukkit.cities.protection.flag;

public enum FlagState {
    ALLOW,
    DENY
}
