package net.pl3x.bukkit.cities.command.selection;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.hook.SUI;
import net.pl3x.bukkit.cities.protection.PlotRegion;
import net.pl3x.bukkit.cities.protection.Selection;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdSelectionContract extends PlayerCommand {
    public CmdSelectionContract(Pl3xCities plugin) {
        super(plugin, "contract", Lang.CMD_DESC_SELECTION_CONTRACT, "cities.command.selection.contract", Lang.CMD_HELP_SELECTION_CONTRACT);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // Show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        Selection selection = pl3xPlayer.getSelection();

        if (args.size() < 2) {
            throw new CommandException(Lang.CMD_HELP_SELECTION_CONTRACT);
        }

        int amount;
        try {
            amount = Integer.valueOf(args.pop().trim());
        } catch (NumberFormatException e) {
            throw new CommandException(Lang.NOT_A_NUMBER);
        }

        if (amount <= 0) {
            throw new CommandException(Lang.ZERO_NUMBER);
        }

        if (selection.getPrimary() == null || selection.getSecondary() == null) {
            throw new CommandException(Lang.SELECTION_NOT_COMPLETE);
        }

        PlotRegion region = new PlotRegion(selection);
        Location primary = region.getMinPoint();
        Location secondary = region.getMaxPoint();

        String direction = args.pop().trim().toLowerCase();
        if ("up".startsWith(direction)) {
            int max = secondary.getBlockY();
            double newValue = primary.getY() + amount;
            if (newValue > max) {
                newValue = max;
            }
            primary.setY(newValue);
        } else if ("down".startsWith(direction)) {
            int min = primary.getBlockY();
            double newValue = secondary.getY() - amount;
            if (newValue < min) {
                newValue = min;
            }
            secondary.setY(newValue);
        } else if ("north".startsWith(direction)) {
            int min = primary.getBlockZ();
            double newValue = secondary.getZ() - amount;
            if (newValue < min) {
                newValue = min;
            }
            secondary.setZ(newValue);
        } else if ("south".startsWith(direction)) {
            int max = secondary.getBlockZ();
            double newValue = primary.getZ() + amount;
            if (newValue > max) {
                newValue = max;
            }
            primary.setZ(newValue);
        } else if ("east".startsWith(direction)) {
            int max = secondary.getBlockX();
            double newValue = primary.getX() + amount;
            if (newValue > max) {
                newValue = max;
            }
            primary.setX(newValue);
        } else if ("west".startsWith(direction)) {
            int min = primary.getBlockX();
            double newValue = secondary.getX() - amount;
            if (newValue < min) {
                newValue = min;
            }
            secondary.setX(newValue);
        } else {
            throw new CommandException(Lang.INVALID_DIRECTION.replace("{direction}", direction));
        }

        selection.setPrimary(primary).setSecondary(secondary);

        new Chat(Lang.SELECTION_CONTRACTED).send(player);

        if (Pl3xCities.hasSUI()) {
            SUI.showSelection(player);
        }
    }
}
