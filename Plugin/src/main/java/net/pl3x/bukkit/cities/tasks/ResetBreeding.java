package net.pl3x.bukkit.cities.tasks;

import org.bukkit.entity.Ageable;
import org.bukkit.scheduler.BukkitRunnable;

public class ResetBreeding extends BukkitRunnable {
    private final Ageable entity;

    public ResetBreeding(Ageable entity) {
        this.entity = entity;
    }

    @Override
    public void run() {
        entity.setBreed(true);
    }
}
