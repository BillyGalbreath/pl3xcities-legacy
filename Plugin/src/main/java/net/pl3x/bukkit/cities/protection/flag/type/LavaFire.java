package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;

/*
 * Protection flag to stop lava from starting fires
 */
public class LavaFire extends FlagListener {
    private final String name = "lava-fire";

    public LavaFire(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onLavaFire(BlockIgniteEvent event) {
        if (!event.getCause().equals(IgniteCause.LAVA)) {
            return;
        }
        if (Flags.isAllowed(name, null, event.getBlock().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
