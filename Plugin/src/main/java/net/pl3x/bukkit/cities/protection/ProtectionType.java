package net.pl3x.bukkit.cities.protection;

import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.usercache.api.CachedPlayer;
import org.bukkit.Chunk;

@SuppressWarnings("WeakerAccess")
public enum ProtectionType {
    WILDERNESS,
    CITY_MINE,
    CITY_OTHER;

    private final String fillColor;
    private final String edgeColor;

    ProtectionType() {
        fillColor = Config.valueOf("VISUAL_" + name() + "_FILL_COLOR").getString();
        edgeColor = Config.valueOf("VISUAL_" + name() + "_EDGE_COLOR").getString();
    }

    public String getFillColor() {
        return fillColor;
    }

    public String getEdgeColor() {
        return edgeColor;
    }

    public static ProtectionType findType(CachedPlayer player, Chunk chunk) {
        return findType(player, CityManager.getManager().getCity(chunk));
    }

    public static ProtectionType findType(CachedPlayer player, City city) {
        if (city != null) {
            if (city.isOwner(player)) {
                return CITY_MINE;
            }
            return CITY_OTHER;
        }
        return WILDERNESS;
    }
}
