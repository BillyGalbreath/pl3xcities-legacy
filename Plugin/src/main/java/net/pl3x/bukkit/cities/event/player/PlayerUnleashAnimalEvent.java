package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerUnleashAnimalEvent extends PlayerEvent {
    private final Entity animal;

    public PlayerUnleashAnimalEvent(Player player, Entity animal) {
        super(player);
        this.animal = animal;
    }

    public Entity getAnimal() {
        return animal;
    }

    public Entity getEntity() {
        return getAnimal();
    }

    public Player getRemover() {
        return getPlayer();
    }
}
