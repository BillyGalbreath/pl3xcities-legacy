package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.protection.SoilDryEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to stop soil from drying out
 */
public class SoilDry extends FlagListener {
    private final String name = "soil-dry";

    public SoilDry(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops soil from drying out
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onSoilDry(SoilDryEvent event) {
        if (Flags.isAllowed(name, null, event.getBlock().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
