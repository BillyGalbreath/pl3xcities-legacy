package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class PlayerTrampleCropEvent extends PlayerEvent {
    private final Block block;

    public PlayerTrampleCropEvent(Player player, Block block) {
        super(player);
        this.block = block;
    }

    public Block getBlock() {
        return block;
    }
}
