package net.pl3x.bukkit.cities.listener;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.event.RegionWandLeftClickEvent;
import net.pl3x.bukkit.cities.event.RegionWandRightClickEvent;
import net.pl3x.bukkit.cities.event.entity.AnimalSpawnEvent;
import net.pl3x.bukkit.cities.event.entity.EntityBreakItemFrameEvent;
import net.pl3x.bukkit.cities.event.entity.EntityBreakPaintingEvent;
import net.pl3x.bukkit.cities.event.entity.EntityDamageAnimalEvent;
import net.pl3x.bukkit.cities.event.entity.EntityDamageEntityEvent;
import net.pl3x.bukkit.cities.event.entity.EntityDamageMobEvent;
import net.pl3x.bukkit.cities.event.entity.EntityDamagePlayerEvent;
import net.pl3x.bukkit.cities.event.entity.EntityDamageVillagerEvent;
import net.pl3x.bukkit.cities.event.entity.EntityDestroyVehicleEvent;
import net.pl3x.bukkit.cities.event.entity.EntityEnterVehicleEvent;
import net.pl3x.bukkit.cities.event.entity.EntitySplashPlayerEvent;
import net.pl3x.bukkit.cities.event.entity.EntityUnleashAnimalFromFenceEvent;
import net.pl3x.bukkit.cities.event.entity.EntityUnleashMobFromFenceEvent;
import net.pl3x.bukkit.cities.event.entity.EntityUseRedstoneEvent;
import net.pl3x.bukkit.cities.event.entity.MobGriefBlockEvent;
import net.pl3x.bukkit.cities.event.entity.MobSpawnEvent;
import net.pl3x.bukkit.cities.event.entity.MobTargetPlayerEvent;
import net.pl3x.bukkit.cities.event.entity.VillagerSpawnEvent;
import net.pl3x.bukkit.cities.event.explode.CreeperExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.EnderCrystalExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.FireballExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.GenericExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.HangingExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.TNTExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.WitherExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.WitherSkullExplodeEvent;
import net.pl3x.bukkit.cities.event.player.PlayerBreakBlockEvent;
import net.pl3x.bukkit.cities.event.player.PlayerBreakItemFrameEvent;
import net.pl3x.bukkit.cities.event.player.PlayerBreakPaintingEvent;
import net.pl3x.bukkit.cities.event.player.PlayerCaughtFishEvent;
import net.pl3x.bukkit.cities.event.player.PlayerCaughtItemEvent;
import net.pl3x.bukkit.cities.event.player.PlayerChunkChangeEvent;
import net.pl3x.bukkit.cities.event.player.PlayerColorAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerDamageAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerDamageEntityEvent;
import net.pl3x.bukkit.cities.event.player.PlayerDamageMobEvent;
import net.pl3x.bukkit.cities.event.player.PlayerDamagePlayerEvent;
import net.pl3x.bukkit.cities.event.player.PlayerDamageVillagerEvent;
import net.pl3x.bukkit.cities.event.player.PlayerDestroyVehicleEvent;
import net.pl3x.bukkit.cities.event.player.PlayerEatCakeEvent;
import net.pl3x.bukkit.cities.event.player.PlayerEnderPearlEvent;
import net.pl3x.bukkit.cities.event.player.PlayerEnterVehicleEvent;
import net.pl3x.bukkit.cities.event.player.PlayerExtinguishFireEvent;
import net.pl3x.bukkit.cities.event.player.PlayerFallDamageEvent;
import net.pl3x.bukkit.cities.event.player.PlayerFeedAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerFishingCastEvent;
import net.pl3x.bukkit.cities.event.player.PlayerFishingEvent;
import net.pl3x.bukkit.cities.event.player.PlayerHungerEvent;
import net.pl3x.bukkit.cities.event.player.PlayerIgniteTNTEvent;
import net.pl3x.bukkit.cities.event.player.PlayerLeashAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerLeashAnimalToFenceEvent;
import net.pl3x.bukkit.cities.event.player.PlayerLeashMobEvent;
import net.pl3x.bukkit.cities.event.player.PlayerLeashMobToFenceEvent;
import net.pl3x.bukkit.cities.event.player.PlayerMilkCowEvent;
import net.pl3x.bukkit.cities.event.player.PlayerPlaceArmorStandEvent;
import net.pl3x.bukkit.cities.event.player.PlayerPlaceBlockEvent;
import net.pl3x.bukkit.cities.event.player.PlayerPlaceItemFrameEvent;
import net.pl3x.bukkit.cities.event.player.PlayerPlacePaintingEvent;
import net.pl3x.bukkit.cities.event.player.PlayerPlaceVehicleEvent;
import net.pl3x.bukkit.cities.event.player.PlayerPlantFlowerPotEvent;
import net.pl3x.bukkit.cities.event.player.PlayerReelAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerReelEntityEvent;
import net.pl3x.bukkit.cities.event.player.PlayerReelMobEvent;
import net.pl3x.bukkit.cities.event.player.PlayerRenameAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerRenameMobEvent;
import net.pl3x.bukkit.cities.event.player.PlayerRightClickItemFrameEvent;
import net.pl3x.bukkit.cities.event.player.PlayerShearAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerSleepEvent;
import net.pl3x.bukkit.cities.event.player.PlayerSoupMushroomCowEvent;
import net.pl3x.bukkit.cities.event.player.PlayerSpawnBabyAnimalFromEggEvent;
import net.pl3x.bukkit.cities.event.player.PlayerSpawnBabyMobFromEggEvent;
import net.pl3x.bukkit.cities.event.player.PlayerSpawnBabyVillagerFromEggEvent;
import net.pl3x.bukkit.cities.event.player.PlayerSplashPlayerEvent;
import net.pl3x.bukkit.cities.event.player.PlayerTameAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerTrampleCropEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUnleashAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUnleashAnimalFromFenceEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUnleashMobEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUnleashMobFromFenceEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUseRedstoneEvent;
import net.pl3x.bukkit.cities.event.player.PlayerWalkEvent;
import net.pl3x.bukkit.cities.event.protection.DragonEggTeleportEvent;
import net.pl3x.bukkit.cities.event.protection.FireSpreadEvent;
import net.pl3x.bukkit.cities.event.protection.HangingObstructionEvent;
import net.pl3x.bukkit.cities.event.protection.HangingPhysicsEvent;
import net.pl3x.bukkit.cities.event.protection.IceFormEvent;
import net.pl3x.bukkit.cities.event.protection.IceMeltEvent;
import net.pl3x.bukkit.cities.event.protection.SnowFormEvent;
import net.pl3x.bukkit.cities.event.protection.SnowMeltEvent;
import net.pl3x.bukkit.cities.event.protection.SoilDryEvent;
import net.pl3x.bukkit.cities.tasks.StopHungerBarShaking;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.Item;
import org.bukkit.entity.LeashHitch;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerLeashEntityEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerUnleashEntityEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;

/**
 * This listener breaks Bukkit events down into more specific custom events for finer controlled listeners.
 */
public class BukkitListener implements Listener {
    private final Pl3xCities plugin;

    public BukkitListener(Pl3xCities plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        Location to = event.getTo();
        Location from = event.getFrom();

        if (to.getBlockX() == from.getBlockX() && to.getBlockY() == from.getBlockY() && to.getBlockZ() == from.getBlockZ()) {
            return; // did not move a full block
        }

        Player player = event.getPlayer();

        // Create a PlayerWalkEvent to signal that a player has moved between blocks
        PlayerWalkEvent playerWalkEvent = new PlayerWalkEvent(player, to, from);
        Bukkit.getPluginManager().callEvent(playerWalkEvent);
        if (playerWalkEvent.isCancelled()) {
            // event.setCancelled(true);
            // DO NOT CANCEL EVENT! Cancelling causes glitches in movement
            Location back = from.clone();
            back.setX(back.getBlockX() + 0.5);
            back.setZ(back.getBlockZ() + 0.5);
            event.setTo(back); // sending player back to center of previous Location
            return;
        }

        Chunk chunkTo = to.getChunk();
        Chunk chunkFrom = from.getChunk();

        if (chunkFrom.getX() == chunkTo.getX() && chunkFrom.getZ() == chunkTo.getZ()) {
            return; // did not move a full chunk
        }

        // Create a PlayerChangeChunkEvent to signal that a player has moved between chunks
        PlayerChunkChangeEvent playerChunkChangeEvent = new PlayerChunkChangeEvent(player, to, from, chunkTo, chunkFrom);
        Bukkit.getPluginManager().callEvent(playerChunkChangeEvent);
        if (playerChunkChangeEvent.isCancelled()) {
            // event.setCancelled(true);
            // DO NOT CANCEL EVENT! Cancelling causes glitches in movement
            Location back = from.clone();
            back.setX(back.getBlockX() + 0.5);
            back.setZ(back.getBlockZ() + 0.5);
            event.setTo(back); // sending player back to center of previous Location
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerFoodLevelChange(FoodLevelChangeEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return; // should never happen
        }

        Player player = (Player) event.getEntity();
        int toLevel = event.getFoodLevel();
        int fromLevel = player.getFoodLevel();

        if (toLevel >= fromLevel) {
            return; // not losing hunger
        }

        PlayerHungerEvent playerHungerEvent = new PlayerHungerEvent(player);
        Bukkit.getPluginManager().callEvent(playerHungerEvent);
        if (playerHungerEvent.isCancelled()) {
            event.setCancelled(true);
            new StopHungerBarShaking(player).runTask(plugin);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerBedEnter(PlayerBedEnterEvent event) {
        PlayerSleepEvent playerSleepEvent = new PlayerSleepEvent(event.getPlayer(), event.getBed());
        Bukkit.getPluginManager().callEvent(playerSleepEvent);
        if (playerSleepEvent.isCancelled()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getHand() == null || event.getHand().equals(EquipmentSlot.OFF_HAND)) {
            return; // ignore offhand 2nd packet
        }

        Block block = event.getClickedBlock();
        if (Config.isWorldDisabled(block.getWorld())) {
            return; // cities not enabled in this world
        }

        Player player = event.getPlayer();
        boolean cancelled = false;
        switch (event.getAction()) {
            case LEFT_CLICK_AIR:
                break;
            case RIGHT_CLICK_AIR:
                break;
            case LEFT_CLICK_BLOCK:
                if (player.getInventory().getItemInMainHand().getType().equals(Config.REGION_WAND.getMaterial())) {
                    RegionWandLeftClickEvent regionWandLeftClickEvent = new RegionWandLeftClickEvent(player, block);
                    Bukkit.getPluginManager().callEvent(regionWandLeftClickEvent);
                    if (!regionWandLeftClickEvent.isCancelled()) {
                        event.setCancelled(true);
                        return; // special case
                    }
                }
                HashSet<Material> transparent = new HashSet<>();
                transparent.add(Material.AIR);
                Block targetBlock = player.getTargetBlock(transparent, 5);
                if (targetBlock.getType().equals(Material.FIRE)) {
                    PlayerExtinguishFireEvent playerExtinguishFireEvent = new PlayerExtinguishFireEvent(player, targetBlock);
                    Bukkit.getPluginManager().callEvent(playerExtinguishFireEvent);
                    cancelled = playerExtinguishFireEvent.isCancelled();
                }
                break;
            case RIGHT_CLICK_BLOCK:
                if (player.getInventory().getItemInMainHand().getType().equals(Config.REGION_WAND.getMaterial())) {
                    RegionWandRightClickEvent regionWandRightClickEvent = new RegionWandRightClickEvent(player, block);
                    Bukkit.getPluginManager().callEvent(regionWandRightClickEvent);
                    if (!regionWandRightClickEvent.isCancelled()) {
                        event.setCancelled(true);
                        return; // special case
                    }
                }
                switch (player.getInventory().getItemInMainHand().getType()) {
                    case ARMOR_STAND:
                        PlayerPlaceArmorStandEvent playerPlaceArmorStandEvent = new PlayerPlaceArmorStandEvent(player, block.getRelative(event.getBlockFace()).getLocation());
                        Bukkit.getPluginManager().callEvent(playerPlaceArmorStandEvent);
                        cancelled = playerPlaceArmorStandEvent.isCancelled();
                        break;
                    case FLINT_AND_STEEL:
                        if (event.getClickedBlock().getType().equals(Material.TNT)) {
                            PlayerIgniteTNTEvent playerIgniteTNTEvent = new PlayerIgniteTNTEvent(player, event.getClickedBlock());
                            Bukkit.getPluginManager().callEvent(playerIgniteTNTEvent);
                            cancelled = playerIgniteTNTEvent.isCancelled();
                        }
                        break;
                    case BOAT:
                    case MINECART:
                    case COMMAND_MINECART:
                    case EXPLOSIVE_MINECART:
                    case HOPPER_MINECART:
                    case POWERED_MINECART:
                    case STORAGE_MINECART:
                        switch (block.getType()) {
                            case RAILS:
                            case ACTIVATOR_RAIL:
                            case DETECTOR_RAIL:
                            case POWERED_RAIL:
                                PlayerPlaceVehicleEvent playerPlaceVehicleEvent = new PlayerPlaceVehicleEvent(player, block.getLocation());
                                Bukkit.getPluginManager().callEvent(playerPlaceVehicleEvent);
                                cancelled = playerPlaceVehicleEvent.isCancelled();
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
                switch (block.getType()) {
                    case BED_BLOCK:
                        PlayerSleepEvent playerSleepEvent = new PlayerSleepEvent(player, block);
                        Bukkit.getPluginManager().callEvent(playerSleepEvent);
                        cancelled = playerSleepEvent.isCancelled();
                        break;
                    case FLOWER_POT:
                        switch (player.getInventory().getItemInMainHand().getType()) {
                            case RED_ROSE:
                            case YELLOW_FLOWER:
                            case SAPLING:
                            case RED_MUSHROOM:
                            case BROWN_MUSHROOM:
                            case DEAD_BUSH:
                            case CACTUS:
                                PlayerPlantFlowerPotEvent playerPlantFlowerPotEvent = new PlayerPlantFlowerPotEvent(player, block);
                                Bukkit.getPluginManager().callEvent(playerPlantFlowerPotEvent);
                                cancelled = playerPlantFlowerPotEvent.isCancelled();
                            default:
                                break;
                        }
                        break;
                    case CAKE_BLOCK:
                        PlayerEatCakeEvent playerEatCakeEvent = new PlayerEatCakeEvent(player, block);
                        Bukkit.getPluginManager().callEvent(playerEatCakeEvent);
                        cancelled = playerEatCakeEvent.isCancelled();
                        break;
                    default:
                        if (isRedstone(block)) {
                            PlayerUseRedstoneEvent playerUseRedstoneEvent = new PlayerUseRedstoneEvent(player, block);
                            Bukkit.getServer().getPluginManager().callEvent(playerUseRedstoneEvent);
                            cancelled = playerUseRedstoneEvent.isCancelled();
                        }
                        break;
                }
                break;
            case PHYSICAL:
                if (isRedstone(block)) {
                    PlayerUseRedstoneEvent playerUseRedstoneEvent = new PlayerUseRedstoneEvent(player, block);
                    Bukkit.getServer().getPluginManager().callEvent(playerUseRedstoneEvent);
                    cancelled = playerUseRedstoneEvent.isCancelled();
                } else if (block.getType().equals(Material.SOIL) || block.getType().equals(Material.CROPS)) {
                    PlayerTrampleCropEvent playerTrampleCropEvent = new PlayerTrampleCropEvent(player, block);
                    Bukkit.getServer().getPluginManager().callEvent(playerTrampleCropEvent);
                    cancelled = playerTrampleCropEvent.isCancelled();
                }
                break;
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        Entity entity = event.getRightClicked();
        if (Config.isWorldDisabled(entity.getWorld())) {
            return; // cities not enabled in this world
        }

        Player player = event.getPlayer();
        ItemStack hand = player.getInventory().getItemInMainHand();

        boolean cancelled = false;
        switch (hand.getType()) {
            case INK_SACK: // changes color (sheep/wolf)
                switch (entity.getType()) {
                    case WOLF:
                        Wolf wolf = (Wolf) entity;
                        if (!wolf.isTamed()) {
                            return; // not tamed, cant color collar
                        }
                        // no break; continue into sheep case to run color animal event
                    case SHEEP:
                        PlayerColorAnimalEvent playerColorAnimalEvent = new PlayerColorAnimalEvent(player, entity, hand);
                        Bukkit.getServer().getPluginManager().callEvent(playerColorAnimalEvent);
                        cancelled = playerColorAnimalEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            case NAME_TAG: // renames animal/mob
                ItemMeta meta = hand.getItemMeta();
                if (meta != null && meta.hasDisplayName()) {
                    if (isAnimal(entity)) {
                        PlayerRenameAnimalEvent playerRenameAnimalEvent = new PlayerRenameAnimalEvent(player, entity, hand);
                        Bukkit.getServer().getPluginManager().callEvent(playerRenameAnimalEvent);
                        cancelled = playerRenameAnimalEvent.isCancelled();
                    } else if (isMob(entity)) {
                        PlayerRenameMobEvent playerRenameMobEvent = new PlayerRenameMobEvent(player, entity, hand);
                        Bukkit.getServer().getPluginManager().callEvent(playerRenameMobEvent);
                        cancelled = playerRenameMobEvent.isCancelled();
                    }
                }
                break;
            case MONSTER_EGG: // spawns babies
                String entityTagId = plugin.getEntityTagHandler().getEntityTagId(hand);
                if (entityTagId == null) {
                    break;
                }
                EntityType eggType = EntityType.valueOf(entityTagId);
                if (eggType.equals(entity.getType())) {
                    if (isAnimal(entity)) {
                        PlayerSpawnBabyAnimalFromEggEvent playerSpawnBabyAnimalFromEggEvent = new PlayerSpawnBabyAnimalFromEggEvent(player, entity);
                        Bukkit.getServer().getPluginManager().callEvent(playerSpawnBabyAnimalFromEggEvent);
                        cancelled = playerSpawnBabyAnimalFromEggEvent.isCancelled();
                    } else if (isMob(entity)) {
                        PlayerSpawnBabyMobFromEggEvent playerSpawnBabyMobFromEggEvent = new PlayerSpawnBabyMobFromEggEvent(player, entity);
                        Bukkit.getServer().getPluginManager().callEvent(playerSpawnBabyMobFromEggEvent);
                        cancelled = playerSpawnBabyMobFromEggEvent.isCancelled();
                    } else if (entity instanceof Villager) {
                        PlayerSpawnBabyVillagerFromEggEvent playerSpawnBabyVillagerFromEggEvent = new PlayerSpawnBabyVillagerFromEggEvent(player, entity);
                        Bukkit.getServer().getPluginManager().callEvent(playerSpawnBabyVillagerFromEggEvent);
                        cancelled = playerSpawnBabyVillagerFromEggEvent.isCancelled();
                    }
                }
                break;
            case APPLE:
            case GOLDEN_APPLE: // (includes enchanted ones)
            case SUGAR:
            case BREAD:
            case HAY_BLOCK:
                switch (entity.getType()) {
                    case HORSE:
                        PlayerFeedAnimalEvent playerFeedAnimalEvent = new PlayerFeedAnimalEvent(player, entity, hand);
                        Bukkit.getServer().getPluginManager().callEvent(playerFeedAnimalEvent);
                        cancelled = playerFeedAnimalEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            case CARROT:
                switch (entity.getType()) {
                    case PIG:
                    case RABBIT:
                        PlayerFeedAnimalEvent playerFeedAnimalEvent = new PlayerFeedAnimalEvent(player, entity, hand);
                        Bukkit.getServer().getPluginManager().callEvent(playerFeedAnimalEvent);
                        cancelled = playerFeedAnimalEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            case GOLDEN_CARROT:
                switch (entity.getType()) {
                    case HORSE:
                    case RABBIT:
                        PlayerFeedAnimalEvent playerFeedAnimalEvent = new PlayerFeedAnimalEvent(player, entity, hand);
                        Bukkit.getServer().getPluginManager().callEvent(playerFeedAnimalEvent);
                        cancelled = playerFeedAnimalEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            case WHEAT:
                switch (entity.getType()) {
                    case HORSE:
                    case SHEEP:
                    case COW:
                    case MUSHROOM_COW:
                        PlayerFeedAnimalEvent playerFeedAnimalEvent = new PlayerFeedAnimalEvent(player, entity, hand);
                        Bukkit.getServer().getPluginManager().callEvent(playerFeedAnimalEvent);
                        cancelled = playerFeedAnimalEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            case SEEDS:
                switch (entity.getType()) {
                    case CHICKEN:
                        PlayerFeedAnimalEvent playerFeedAnimalEvent = new PlayerFeedAnimalEvent(player, entity, hand);
                        Bukkit.getServer().getPluginManager().callEvent(playerFeedAnimalEvent);
                        cancelled = playerFeedAnimalEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            case BONE: // used for wolf taming
            case COOKED_BEEF:
            case COOKED_CHICKEN:
            case COOKED_MUTTON:
            case COOKED_RABBIT:
            case RAW_BEEF:
            case RAW_CHICKEN:
            case MUTTON:
            case RABBIT:
            case ROTTEN_FLESH:
                switch (entity.getType()) {
                    case WOLF:
                        PlayerFeedAnimalEvent playerFeedAnimalEvent = new PlayerFeedAnimalEvent(player, entity, hand);
                        Bukkit.getServer().getPluginManager().callEvent(playerFeedAnimalEvent);
                        cancelled = playerFeedAnimalEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            case COOKED_FISH:
            case RAW_FISH:
                switch (entity.getType()) {
                    case OCELOT:
                        PlayerFeedAnimalEvent playerFeedAnimalEvent = new PlayerFeedAnimalEvent(player, entity, hand);
                        Bukkit.getServer().getPluginManager().callEvent(playerFeedAnimalEvent);
                        cancelled = playerFeedAnimalEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            case YELLOW_FLOWER:
                switch (entity.getType()) {
                    case RABBIT:
                        PlayerFeedAnimalEvent playerFeedAnimalEvent = new PlayerFeedAnimalEvent(player, entity, hand);
                        Bukkit.getServer().getPluginManager().callEvent(playerFeedAnimalEvent);
                        cancelled = playerFeedAnimalEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            case BUCKET:
                switch (entity.getType()) {
                    case COW:
                        PlayerMilkCowEvent playerMilkCowEvent = new PlayerMilkCowEvent(player, entity);
                        Bukkit.getServer().getPluginManager().callEvent(playerMilkCowEvent);
                        cancelled = playerMilkCowEvent.isCancelled();
                        break;
                    case MUSHROOM_COW:
                        PlayerSoupMushroomCowEvent playerSoupMushroomCowEvent = new PlayerSoupMushroomCowEvent(player, entity);
                        Bukkit.getServer().getPluginManager().callEvent(playerSoupMushroomCowEvent);
                        cancelled = playerSoupMushroomCowEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            case BOWL:
                switch (entity.getType()) {
                    case COW:
                        PlayerMilkCowEvent playerMilkCowEvent = new PlayerMilkCowEvent(player, entity);
                        Bukkit.getServer().getPluginManager().callEvent(playerMilkCowEvent);
                        cancelled = playerMilkCowEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            case SHEARS:
                switch (entity.getType()) {
                    case SHEEP:
                    case COW:
                    case MUSHROOM_COW:
                        PlayerShearAnimalEvent playerShearAnimalEvent = new PlayerShearAnimalEvent(player, entity);
                        Bukkit.getServer().getPluginManager().callEvent(playerShearAnimalEvent);
                        cancelled = playerShearAnimalEvent.isCancelled();
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        switch (entity.getType()) {
            case ITEM_FRAME:
                PlayerRightClickItemFrameEvent playerRightClickItemFrameEvent = new PlayerRightClickItemFrameEvent(player, entity);
                Bukkit.getServer().getPluginManager().callEvent(playerRightClickItemFrameEvent);
                cancelled = playerRightClickItemFrameEvent.isCancelled();
                break;
            default:
                break;
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerFish(PlayerFishEvent event) {
        Player player = event.getPlayer();
        Entity caught = event.getCaught();
        FishHook hook = event.getHook();

        PlayerFishingEvent fishingEvent;
        System.out.println("Fish Event: " + event.getState().name());
        switch (event.getState()) {
            case CAUGHT_ENTITY: // reeled entity
            case FAILED_ATTEMPT: // did not reel anything in
                if (isAnimal(caught)) {
                    fishingEvent = new PlayerReelAnimalEvent(player, caught, hook);
                } else if (isMob(caught)) {
                    fishingEvent = new PlayerReelMobEvent(player, caught, hook);
                } else {
                    fishingEvent = new PlayerReelEntityEvent(player, caught, hook);
                }
                break;
            case CAUGHT_FISH: // reeled in fish
                if (!(caught instanceof Item)) {
                    return; // this should never happen
                }
                Item item = (Item) caught;
                Material type = item.getItemStack().getType();
                if (type == Material.RAW_FISH || type == Material.COOKED_FISH) {
                    fishingEvent = new PlayerCaughtFishEvent(player, item, hook);
                } else {
                    fishingEvent = new PlayerCaughtItemEvent(player, item, hook);
                }
                break;
            case FISHING: // casting line out. Caught is always null
                new BukkitRunnable() {
                    public void run() {
                        PlayerFishingCastEvent castEvent = new PlayerFishingCastEvent(player, hook);
                        Bukkit.getPluginManager().callEvent(castEvent);
                        if (castEvent.isCancelled()) {
                            castEvent.getHook().remove();
                        }
                    }
                }.runTaskLater(plugin, 20L);
                return;
            case IN_GROUND: // bobber stuck in ground
            case BITE: // fish bites hook and ready to be reeled in
            default:
                fishingEvent = new PlayerFishingEvent(player, hook);
        }

        Bukkit.getPluginManager().callEvent(fishingEvent);
        if (fishingEvent.isCancelled()) {
            event.setCancelled(true);
            fishingEvent.getHook().remove();
        }

    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerShearEntity(PlayerShearEntityEvent event) {
        if (Config.isWorldDisabled(event.getEntity().getWorld())) {
            return; // cities not enabled in this world
        }

        PlayerShearAnimalEvent playerShearAnimalEvent = new PlayerShearAnimalEvent(event.getPlayer(), event.getEntity());
        Bukkit.getServer().getPluginManager().callEvent(playerShearAnimalEvent);

        if (playerShearAnimalEvent.isCancelled()) {
            event.setCancelled(true);
        }
    }

    @EventHandler()
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (Config.isWorldDisabled(event.getTo().getWorld())) {
            return; // cities not enabled in this world
        }

        boolean cancelled = false;
        switch (event.getCause()) {
            case COMMAND:
                break;
            case ENDER_PEARL:
                PlayerEnderPearlEvent playerEnderPearlEvent = new PlayerEnderPearlEvent(event.getPlayer(), event.getFrom(), event.getTo());
                Bukkit.getServer().getPluginManager().callEvent(playerEnderPearlEvent);
                cancelled = playerEnderPearlEvent.isCancelled();
                break;
            case END_PORTAL:
                break;
            case NETHER_PORTAL:
                break;
            case PLUGIN:
                break;
            case SPECTATE:
                break;
            case UNKNOWN:
                break;
            default:
                break;
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }

        PlayerPlaceBlockEvent playerPlaceBlockEvent = new PlayerPlaceBlockEvent(event.getPlayer(), event.getBlock(), event.getBlockAgainst());
        Bukkit.getServer().getPluginManager().callEvent(playerPlaceBlockEvent);

        if (playerPlaceBlockEvent.isCancelled()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }

        PlayerBreakBlockEvent playerBreakBlockEvent = new PlayerBreakBlockEvent(event.getPlayer(), event.getBlock(), event.getExpToDrop());
        Bukkit.getServer().getPluginManager().callEvent(playerBreakBlockEvent);

        if (playerBreakBlockEvent.isCancelled()) {
            event.setCancelled(true);
        }
    }

    /*
     * Called when dragon eggs move from one block to another
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void protectFromLiquidSpills(BlockFromToEvent event) {
        if (event.getBlock().isLiquid()) {
            return; // ignore liquids (protection listener handles liquids)
        }
        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }
        DragonEggTeleportEvent dragonEggTeleportEvent = new DragonEggTeleportEvent(event.getBlock(), event.getToBlock(), event.getFace());
        Bukkit.getServer().getPluginManager().callEvent(dragonEggTeleportEvent);

        if (dragonEggTeleportEvent.isCancelled()) {
            event.setCancelled(true);
        }
    }

    /*
     * Called when a block explodes (bed in nether/end)
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockExplode(BlockExplodeEvent event) {
        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }

        GenericExplodeEvent genericExplodeEvent = new GenericExplodeEvent(event.blockList());
        Bukkit.getServer().getPluginManager().callEvent(genericExplodeEvent);

        if (genericExplodeEvent.isCancelled()) {
            event.setCancelled(true);
        }
    }

    /*
     * Called when a block fades (ice/snow melts)
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockFade(BlockFadeEvent event) {
        Block block = event.getBlock();
        boolean cancelled;
        switch (block.getType()) {
            case ICE:
                IceMeltEvent iceMeltEvent = new IceMeltEvent(block);
                Bukkit.getServer().getPluginManager().callEvent(iceMeltEvent);
                cancelled = iceMeltEvent.isCancelled();
                break;
            case SNOW:
                SnowMeltEvent snowMeltEvent = new SnowMeltEvent(block);
                Bukkit.getServer().getPluginManager().callEvent(snowMeltEvent);
                cancelled = snowMeltEvent.isCancelled();
                break;
            case SOIL:
                SoilDryEvent soilDryEvent = new SoilDryEvent(block);
                Bukkit.getServer().getPluginManager().callEvent(soilDryEvent);
                cancelled = soilDryEvent.isCancelled();
                break;
            default:
                return;
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    /*
     * Called when a block forms (ice/snow forms)
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockForm(BlockFormEvent event) {
        Block block = event.getBlock();
        BlockState state = event.getNewState();
        boolean cancelled;
        switch (state.getType()) {
            case ICE:
                IceFormEvent iceFormEvent = new IceFormEvent(block);
                Bukkit.getServer().getPluginManager().callEvent(iceFormEvent);
                cancelled = iceFormEvent.isCancelled();
                break;
            case SNOW:
                SnowFormEvent snowFormEvent = new SnowFormEvent(block);
                Bukkit.getServer().getPluginManager().callEvent(snowFormEvent);
                cancelled = snowFormEvent.isCancelled();
                break;
            default:
                return;
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockSpread(BlockSpreadEvent event) {
        Block source = event.getSource();
        Block block = event.getBlock();

        if (!source.getType().equals(Material.FIRE)) {
            return;
        }

        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }

        FireSpreadEvent fireSpreadEvent = new FireSpreadEvent(source, block);
        Bukkit.getServer().getPluginManager().callEvent(fireSpreadEvent);

        if (fireSpreadEvent.isCancelled()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        if (Config.isWorldDisabled(event.getLocation().getWorld())) {
            return; // cities not enabled in this world
        }

        Entity entity = event.getEntity();
        boolean cancelled;
        if (isMob(entity)) {
            MobSpawnEvent mobSpawnEvent = new MobSpawnEvent(entity, event.getLocation(), event.getSpawnReason());
            Bukkit.getServer().getPluginManager().callEvent(mobSpawnEvent);
            cancelled = mobSpawnEvent.isCancelled();
        } else if (isAnimal(entity)) {
            AnimalSpawnEvent animalSpawnEvent = new AnimalSpawnEvent(entity, event.getLocation(), event.getSpawnReason());
            Bukkit.getServer().getPluginManager().callEvent(animalSpawnEvent);
            cancelled = animalSpawnEvent.isCancelled();
        } else if (entity instanceof Villager) {
            VillagerSpawnEvent villagerSpawnEvent = new VillagerSpawnEvent(entity, event.getLocation(), event.getSpawnReason());
            Bukkit.getServer().getPluginManager().callEvent(villagerSpawnEvent);
            cancelled = villagerSpawnEvent.isCancelled();
        } else {
            return;
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPotionSplash(PotionSplashEvent event) {
        ThrownPotion potion = event.getEntity();
        ProjectileSource source = potion.getShooter();
        if (!(source instanceof Entity)) {
            return;
        }

        Entity thrower = (Entity) source;
        if (Config.isWorldDisabled(thrower.getLocation().getWorld())) {
            return; // cities not enabled in this world
        }

        boolean cancelled = false;
        if (isMob(thrower)) {
            for (Entity attacked : event.getAffectedEntities()) {
                if (!(attacked instanceof Player)) {
                    continue;
                }

                EntitySplashPlayerEvent entitySplashPlayerEvent = new EntitySplashPlayerEvent((Player) attacked, thrower);
                Bukkit.getServer().getPluginManager().callEvent(entitySplashPlayerEvent);
                cancelled = entitySplashPlayerEvent.isCancelled();
            }
        } else if (thrower instanceof Player) {
            for (Entity attacked : event.getAffectedEntities()) {
                if (!(attacked instanceof Player)) {
                    continue;
                }

                PlayerSplashPlayerEvent playerSplashPlayerEvent = new PlayerSplashPlayerEvent((Player) attacked, (Player) thrower);
                Bukkit.getServer().getPluginManager().callEvent(playerSplashPlayerEvent);
                cancelled = playerSplashPlayerEvent.isCancelled();
            }
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityExplode(EntityExplodeEvent event) {
        if (Config.isWorldDisabled(event.getLocation().getWorld())) {
            return; // cities not enabled in this world
        }

        boolean cancelled;
        switch (event.getEntityType()) {
            case WITHER_SKULL: // shot head
                WitherSkullExplodeEvent witherSkullExplodeEvent = new WitherSkullExplodeEvent(event.getEntity(), event.blockList());
                Bukkit.getServer().getPluginManager().callEvent(witherSkullExplodeEvent);
                cancelled = witherSkullExplodeEvent.isCancelled();
                break;
            case WITHER: // initial "charging" explosion
                WitherExplodeEvent witherExplodeEvent = new WitherExplodeEvent(event.getEntity(), event.blockList());
                Bukkit.getServer().getPluginManager().callEvent(witherExplodeEvent);
                cancelled = witherExplodeEvent.isCancelled();
                break;
            case CREEPER: // includes charged/super creepers
                CreeperExplodeEvent creeperExplodeEvent = new CreeperExplodeEvent(event.getEntity(), event.blockList());
                Bukkit.getServer().getPluginManager().callEvent(creeperExplodeEvent);
                cancelled = creeperExplodeEvent.isCancelled();
                break;
            case FIREBALL: // ghasts shoot them
                FireballExplodeEvent fireballExplodeEvent = new FireballExplodeEvent(event.getEntity(), event.blockList());
                Bukkit.getServer().getPluginManager().callEvent(fireballExplodeEvent);
                cancelled = fireballExplodeEvent.isCancelled();
                break;
            case ENDER_CRYSTAL:
                EnderCrystalExplodeEvent enderCrystalExplodeEvent = new EnderCrystalExplodeEvent(event.getEntity(), event.blockList());
                Bukkit.getServer().getPluginManager().callEvent(enderCrystalExplodeEvent);
                cancelled = enderCrystalExplodeEvent.isCancelled();
                break;
            case PRIMED_TNT:
                TNTExplodeEvent tntExplodeEvent = new TNTExplodeEvent(event.getEntity(), event.blockList());
                Bukkit.getServer().getPluginManager().callEvent(tntExplodeEvent);
                cancelled = tntExplodeEvent.isCancelled();
                break;
            default:
                GenericExplodeEvent genericExplodeEvent = new GenericExplodeEvent(event.blockList());
                Bukkit.getServer().getPluginManager().callEvent(genericExplodeEvent);
                cancelled = genericExplodeEvent.isCancelled();
        }

        if (cancelled) {
            event.blockList().clear();
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        if (Config.isWorldDisabled(entity.getWorld())) {
            return; // cities not enabled in this world
        }

        DamageCause cause = event.getCause();
        boolean cancelled = false;
        switch (entity.getType()) {
            case PLAYER:
                Player player = (Player) entity;
                switch (cause) {
                    case BLOCK_EXPLOSION:
                        break;
                    case CONTACT:
                        break;
                    case CUSTOM:
                        break;
                    case DROWNING:
                        break;
                    case ENTITY_ATTACK:
                        break;
                    case ENTITY_EXPLOSION:
                        break;
                    case FALL:
                        PlayerFallDamageEvent playerFallDamageEvent = new PlayerFallDamageEvent(player);
                        Bukkit.getServer().getPluginManager().callEvent(playerFallDamageEvent);
                        cancelled = playerFallDamageEvent.isCancelled();
                        break;
                    case FALLING_BLOCK:
                        break;
                    case FIRE:
                        break;
                    case FIRE_TICK:
                        break;
                    case LAVA:
                        break;
                    case LIGHTNING:
                        break;
                    case MAGIC:
                        break;
                    case MELTING:
                        break;
                    case POISON:
                        break;
                    case PROJECTILE:
                        break;
                    case STARVATION:
                        break;
                    case SUFFOCATION:
                        break;
                    case SUICIDE:
                        break;
                    case THORNS:
                        break;
                    case VOID:
                        break;
                    case WITHER:
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (Config.isWorldDisabled(event.getEntity().getWorld())) {
            return; // cities not enabled in this world
        }

        Entity damaged = event.getEntity();
        Entity damager = event.getDamager();

        boolean cancelled = false;
        if (damaged instanceof Player) {
            if (damager instanceof Player) {
                // player hurt player
                PlayerDamagePlayerEvent pdpEvent = new PlayerDamagePlayerEvent((Player) damaged, (Player) damager);
                Bukkit.getServer().getPluginManager().callEvent(pdpEvent);
                cancelled = pdpEvent.isCancelled();
            } else {
                if (damager instanceof Projectile) {
                    ProjectileSource shooter = ((Projectile) damager).getShooter();
                    if (shooter instanceof Player) {
                        // player shot player
                        PlayerDamagePlayerEvent pdpEvent = new PlayerDamagePlayerEvent((Player) damaged, (Player) shooter);
                        Bukkit.getServer().getPluginManager().callEvent(pdpEvent);
                        cancelled = pdpEvent.isCancelled();
                    } else if (shooter instanceof Entity) {
                        // entity shot player
                        EntityDamagePlayerEvent edpEvent = new EntityDamagePlayerEvent((Player) damaged, (Entity) shooter);
                        Bukkit.getServer().getPluginManager().callEvent(edpEvent);
                        cancelled = edpEvent.isCancelled();
                    }
                } else {
                    // entity hurt player
                    EntityDamagePlayerEvent edpEvent = new EntityDamagePlayerEvent((Player) damaged, damager);
                    Bukkit.getServer().getPluginManager().callEvent(edpEvent);
                    cancelled = edpEvent.isCancelled();
                }
            }
        } else {
            if (damager instanceof Player) {
                // player hurt entity
                if (isAnimal(damaged)) {
                    // player hurt animal
                    PlayerDamageAnimalEvent pdaEvent = new PlayerDamageAnimalEvent(damaged, (Player) damager);
                    Bukkit.getServer().getPluginManager().callEvent(pdaEvent);
                    cancelled = pdaEvent.isCancelled();
                } else if (isMob(damaged)) {
                    // player hurt mob
                    PlayerDamageMobEvent pdmEvent = new PlayerDamageMobEvent(damaged, (Player) damager);
                    Bukkit.getServer().getPluginManager().callEvent(pdmEvent);
                    cancelled = pdmEvent.isCancelled();
                } else if (damaged instanceof Villager) {
                    // player hurt villager
                    PlayerDamageVillagerEvent pdvEvent = new PlayerDamageVillagerEvent(damaged, (Player) damager);
                    Bukkit.getServer().getPluginManager().callEvent(pdvEvent);
                    cancelled = pdvEvent.isCancelled();
                } else {
                    // player hurt tile entity (itemframe, picture, armorstand, etc)
                    PlayerDamageEntityEvent pdeEvent = new PlayerDamageEntityEvent(damaged, (Player) damager);
                    Bukkit.getServer().getPluginManager().callEvent(pdeEvent);
                    cancelled = pdeEvent.isCancelled();
                }
            } else {
                if (damager instanceof Projectile) {
                    ProjectileSource shooter = ((Projectile) damager).getShooter();
                    if (shooter instanceof Player) {
                        // player shot entity
                        if (isAnimal(damaged)) {
                            // player shot animal
                            PlayerDamageAnimalEvent pdaEvent = new PlayerDamageAnimalEvent(damaged, (Player) shooter);
                            Bukkit.getServer().getPluginManager().callEvent(pdaEvent);
                            cancelled = pdaEvent.isCancelled();
                        } else if (isMob(damaged)) {
                            // player shot mob
                            PlayerDamageMobEvent pdmEvent = new PlayerDamageMobEvent(damaged, (Player) shooter);
                            Bukkit.getServer().getPluginManager().callEvent(pdmEvent);
                            cancelled = pdmEvent.isCancelled();
                        } else if (damaged instanceof Villager) {
                            // player shot villager
                            PlayerDamageVillagerEvent pdvEvent = new PlayerDamageVillagerEvent(damaged, (Player) shooter);
                            Bukkit.getServer().getPluginManager().callEvent(pdvEvent);
                            cancelled = pdvEvent.isCancelled();
                        } else {
                            // player shot tile entity (itemframe, picture, armorstand, etc)
                            PlayerDamageEntityEvent pdeEvent = new PlayerDamageEntityEvent(damaged, (Player) shooter);
                            Bukkit.getServer().getPluginManager().callEvent(pdeEvent);
                            cancelled = pdeEvent.isCancelled();
                        }
                    } else {
                        // entity shot entity
                        // check for tile entities (mob griefing)
                        if (isAnimal(damaged)) {
                            // entity shot animal
                            EntityDamageAnimalEvent edaEvent = new EntityDamageAnimalEvent(damaged, damager);
                            Bukkit.getServer().getPluginManager().callEvent(edaEvent);
                            cancelled = edaEvent.isCancelled();
                        } else if (isMob(damaged)) {
                            // entity shot mob
                            EntityDamageMobEvent edmEvent = new EntityDamageMobEvent(damaged, damager);
                            Bukkit.getServer().getPluginManager().callEvent(edmEvent);
                            cancelled = edmEvent.isCancelled();
                        } else if (damaged instanceof Villager) {
                            // entity shot villager
                            EntityDamageVillagerEvent edvEvent = new EntityDamageVillagerEvent(damaged, damager);
                            Bukkit.getServer().getPluginManager().callEvent(edvEvent);
                            cancelled = edvEvent.isCancelled();
                        } else {
                            // entity shot tile entity (itemframe, picture, armorstand, etc)
                            EntityDamageEntityEvent pdeEvent = new EntityDamageEntityEvent(damaged, damager);
                            Bukkit.getServer().getPluginManager().callEvent(pdeEvent);
                            cancelled = pdeEvent.isCancelled();
                        }
                    }
                } else {
                    // entity hurt entity
                    if (isAnimal(damaged)) {
                        // entity hurt animal
                        EntityDamageAnimalEvent pdaEvent = new EntityDamageAnimalEvent(damaged, damager);
                        Bukkit.getServer().getPluginManager().callEvent(pdaEvent);
                        cancelled = pdaEvent.isCancelled();
                    } else if (isMob(damaged)) {
                        // entity hurt mob
                        EntityDamageMobEvent pdmEvent = new EntityDamageMobEvent(damaged, damager);
                        Bukkit.getServer().getPluginManager().callEvent(pdmEvent);
                        cancelled = pdmEvent.isCancelled();
                    } else if (damaged instanceof Villager) {
                        // entity hurt villager
                        EntityDamageVillagerEvent edvEvent = new EntityDamageVillagerEvent(damaged, damager);
                        Bukkit.getServer().getPluginManager().callEvent(edvEvent);
                        cancelled = edvEvent.isCancelled();
                    } else {
                        // entity hurt tile entity (itemframe, picture, armorstand, etc)
                        EntityDamageEntityEvent pdeEvent = new EntityDamageEntityEvent(damaged, damager);
                        Bukkit.getServer().getPluginManager().callEvent(pdeEvent);
                        cancelled = pdeEvent.isCancelled();
                    }
                }
            }
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityChangeBlock(EntityChangeBlockEvent event) {
        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }

        if (event.getEntity().getType().equals(EntityType.FALLING_BLOCK)) {
            return; // falling block event. ignore.
        }

        MobGriefBlockEvent entityEvent = new MobGriefBlockEvent(event.getEntity(), event.getBlock());
        Bukkit.getServer().getPluginManager().callEvent(entityEvent);

        if (entityEvent.isCancelled()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityInteract(EntityInteractEvent event) {
        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }

        Block block = event.getBlock();

        if (isRedstone(block)) {
            if (event.getEntity() instanceof Player) {
                return;
            }
            EntityUseRedstoneEvent entityUseRedstoneEvent = new EntityUseRedstoneEvent(event.getEntity(), event.getBlock());
            Bukkit.getServer().getPluginManager().callEvent(entityUseRedstoneEvent);
            if (entityUseRedstoneEvent.isCancelled()) {
                event.setCancelled(true);
            }
            return;
        }

        boolean cancelled = false;
        switch (block.getType()) {
            case SOIL:
            case CROPS:
                MobGriefBlockEvent mobGriefEvent = new MobGriefBlockEvent(event.getEntity(), event.getBlock());
                Bukkit.getServer().getPluginManager().callEvent(mobGriefEvent);
                cancelled = mobGriefEvent.isCancelled();
                break;
            default:
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityTame(EntityTameEvent event) {
        if (Config.isWorldDisabled(event.getEntity().getWorld())) {
            return; // cities not enabled in this world
        }

        if (!(event.getOwner() instanceof Player)) {
            return; // not a player - does this even happen? o_O
        }

        PlayerTameAnimalEvent playerTameAnimalEvent = new PlayerTameAnimalEvent((Player) event.getOwner(), event.getEntity());
        Bukkit.getServer().getPluginManager().callEvent(playerTameAnimalEvent);

        if (playerTameAnimalEvent.isCancelled()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event) {
        Entity entity = event.getEntity();
        LivingEntity target = event.getTarget();

        if (!(target instanceof Player)) {
            return; // not targeting a player
        }

        if (!isMob(entity)) {
            return;
        }

        MobTargetPlayerEvent mobTargetPlayerEvent = new MobTargetPlayerEvent(entity, (Player) target);
        Bukkit.getServer().getPluginManager().callEvent(mobTargetPlayerEvent);

        if (mobTargetPlayerEvent.isCancelled()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerLeashEntity(PlayerLeashEntityEvent event) {
        if (Config.isWorldDisabled(event.getLeashHolder().getWorld())) {
            return; // cities not enabled in this world
        }

        if (!event.getLeashHolder().getType().equals(EntityType.PLAYER)) {
            return;
        }

        Entity entity = event.getEntity();
        Player player = event.getPlayer();

        boolean cancelled = false;
        if (isAnimal(entity)) {
            PlayerLeashAnimalEvent playerLeashAnimalEvent = new PlayerLeashAnimalEvent(player, entity);
            Bukkit.getServer().getPluginManager().callEvent(playerLeashAnimalEvent);
            cancelled = playerLeashAnimalEvent.isCancelled();
        } else if (isMob(entity)) {
            PlayerLeashMobEvent playerLeashMobEvent = new PlayerLeashMobEvent(player, entity);
            Bukkit.getServer().getPluginManager().callEvent(playerLeashMobEvent);
            cancelled = playerLeashMobEvent.isCancelled();
        }

        if (cancelled) {
            event.setCancelled(true);
            player.updateInventory();
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerUnleash(PlayerUnleashEntityEvent event) {
        Entity entity = event.getEntity();
        if (!(entity instanceof LivingEntity)) {
            return;
        }

        LivingEntity livingEntity = (LivingEntity) entity;
        Player player = event.getPlayer();

        boolean cancelled = false;
        switch (livingEntity.getLeashHolder().getType()) {
            case PLAYER:
                if (isAnimal(entity)) {
                    PlayerUnleashAnimalEvent playerUnleashAnimalEvent = new PlayerUnleashAnimalEvent(player, entity);
                    Bukkit.getServer().getPluginManager().callEvent(playerUnleashAnimalEvent);
                    cancelled = playerUnleashAnimalEvent.isCancelled();
                } else if (isMob(entity)) {
                    PlayerUnleashMobEvent playerUnleashMobEvent = new PlayerUnleashMobEvent(player, entity);
                    Bukkit.getServer().getPluginManager().callEvent(playerUnleashMobEvent);
                    cancelled = playerUnleashMobEvent.isCancelled();
                }
                break;
            case LEASH_HITCH:
                // this ONLY happens if right clicking the leash hitch. for left clicking see HangingBreakByEntityEvent
                if (isAnimal(entity)) {
                    PlayerUnleashAnimalFromFenceEvent playerUnleashAnimalFromFenceEvent = new PlayerUnleashAnimalFromFenceEvent(player, entity, livingEntity.getLocation());
                    Bukkit.getServer().getPluginManager().callEvent(playerUnleashAnimalFromFenceEvent);
                    cancelled = playerUnleashAnimalFromFenceEvent.isCancelled();
                } else if (isMob(entity)) {
                    PlayerUnleashMobFromFenceEvent playerUnleashMobFromFenceEvent = new PlayerUnleashMobFromFenceEvent(player, entity, livingEntity.getLocation());
                    Bukkit.getServer().getPluginManager().callEvent(playerUnleashMobFromFenceEvent);
                    cancelled = playerUnleashMobFromFenceEvent.isCancelled();
                }
                break;
            default:
                return;
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onHangingPlace(HangingPlaceEvent event) {
        if (Config.isWorldDisabled(event.getEntity().getWorld())) {
            return; // cities not enabled in this world
        }

        Player player = event.getPlayer();
        Hanging hanging = event.getEntity();

        boolean cancelled = false;
        switch (hanging.getType()) {
            case LEASH_HITCH:
                Entity leashedEntity = getLeashedEntity(player, (LeashHitch) hanging);
                if (isAnimal(leashedEntity)) {
                    PlayerLeashAnimalToFenceEvent playerLeashAnimalToFenceEvent = new PlayerLeashAnimalToFenceEvent(player, leashedEntity, hanging.getLocation());
                    Bukkit.getServer().getPluginManager().callEvent(playerLeashAnimalToFenceEvent);
                    cancelled = playerLeashAnimalToFenceEvent.isCancelled();
                } else if (isMob(leashedEntity)) {
                    PlayerLeashMobToFenceEvent playerLeashMobToFenceEvent = new PlayerLeashMobToFenceEvent(player, leashedEntity, hanging.getLocation());
                    Bukkit.getServer().getPluginManager().callEvent(playerLeashMobToFenceEvent);
                    cancelled = playerLeashMobToFenceEvent.isCancelled();
                }
                break;
            case ITEM_FRAME:
                PlayerPlaceItemFrameEvent playerPlaceItemFrameEvent = new PlayerPlaceItemFrameEvent(player, hanging, event.getBlock(), event.getBlockFace());
                Bukkit.getServer().getPluginManager().callEvent(playerPlaceItemFrameEvent);
                cancelled = playerPlaceItemFrameEvent.isCancelled();
                break;
            case PAINTING:
                PlayerPlacePaintingEvent playerPlacePaintingEvent = new PlayerPlacePaintingEvent(player, hanging, event.getBlock(), event.getBlockFace());
                Bukkit.getServer().getPluginManager().callEvent(playerPlacePaintingEvent);
                cancelled = playerPlacePaintingEvent.isCancelled();
                break;
            default:
                return;
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onHangingBreak(HangingBreakEvent event) {
        if (Config.isWorldDisabled(event.getEntity().getWorld())) {
            return; // cities not enabled in this world
        }

        Entity hanging = event.getEntity();

        boolean cancelled = false;
        switch (event.getCause()) {
            case ENTITY:
                // Handled by HangingBreakByEntityEvent
                break;
            case EXPLOSION:
                HangingExplodeEvent hangingExplodeEvent = new HangingExplodeEvent(hanging);
                Bukkit.getServer().getPluginManager().callEvent(hangingExplodeEvent);
                cancelled = hangingExplodeEvent.isCancelled();
                break;
            case OBSTRUCTION:
                HangingObstructionEvent hangingObstructionEvent = new HangingObstructionEvent(hanging);
                Bukkit.getServer().getPluginManager().callEvent(hangingObstructionEvent);
                cancelled = hangingObstructionEvent.isCancelled();
                break;
            case PHYSICS:
                HangingPhysicsEvent hangingPhysicsEvent = new HangingPhysicsEvent(hanging);
                Bukkit.getServer().getPluginManager().callEvent(hangingPhysicsEvent);
                cancelled = hangingPhysicsEvent.isCancelled();
                break;
            case DEFAULT:
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onHangingBreakByEntity(HangingBreakByEntityEvent event) {
        if (Config.isWorldDisabled(event.getEntity().getWorld())) {
            return; // cities not enabled in this world
        }

        Entity remover = event.getRemover();
        Hanging hanging = event.getEntity();

        boolean cancelled = false;
        if (remover instanceof Player) {
            Player player = (Player) remover;
            switch (event.getEntity().getType()) {
                case ITEM_FRAME:
                    PlayerBreakItemFrameEvent playerBreakItemFrameEvent = new PlayerBreakItemFrameEvent(player, hanging, event.getCause());
                    Bukkit.getServer().getPluginManager().callEvent(playerBreakItemFrameEvent);
                    cancelled = playerBreakItemFrameEvent.isCancelled();
                    break;
                case PAINTING:
                    PlayerBreakPaintingEvent playerBreakPaintingEvent = new PlayerBreakPaintingEvent(player, hanging, event.getCause());
                    Bukkit.getServer().getPluginManager().callEvent(playerBreakPaintingEvent);
                    cancelled = playerBreakPaintingEvent.isCancelled();
                    break;
                case LEASH_HITCH:
                    LivingEntity leashed = getLeashedEntity(hanging, (LeashHitch) hanging);
                    if (isAnimal(leashed)) {
                        PlayerUnleashAnimalFromFenceEvent playerUnleashAnimalFromFenceEvent = new PlayerUnleashAnimalFromFenceEvent(player, leashed, hanging.getLocation());
                        Bukkit.getServer().getPluginManager().callEvent(playerUnleashAnimalFromFenceEvent);
                        cancelled = playerUnleashAnimalFromFenceEvent.isCancelled();
                    } else if (isMob(leashed)) {
                        PlayerUnleashMobFromFenceEvent playerUnleashMobFromFenceEvent = new PlayerUnleashMobFromFenceEvent(player, leashed, hanging.getLocation());
                        Bukkit.getServer().getPluginManager().callEvent(playerUnleashMobFromFenceEvent);
                        cancelled = playerUnleashMobFromFenceEvent.isCancelled();
                    }
                    break;
                default:
                    return;
            }
        } else if (remover instanceof Projectile) {
            ProjectileSource shooter = ((Projectile) remover).getShooter();
            if (shooter instanceof Player) {
                Player player = (Player) shooter;
                switch (event.getEntity().getType()) {
                    case ITEM_FRAME:
                        PlayerBreakItemFrameEvent playerBreakItemFrameEvent = new PlayerBreakItemFrameEvent(player, hanging, event.getCause());
                        Bukkit.getServer().getPluginManager().callEvent(playerBreakItemFrameEvent);
                        cancelled = playerBreakItemFrameEvent.isCancelled();
                        break;
                    case PAINTING:
                        PlayerBreakPaintingEvent playerBreakPaintingEvent = new PlayerBreakPaintingEvent(player, hanging, event.getCause());
                        Bukkit.getServer().getPluginManager().callEvent(playerBreakPaintingEvent);
                        cancelled = playerBreakPaintingEvent.isCancelled();
                        break;
                    case LEASH_HITCH:
                        LivingEntity leashed = getLeashedEntity(hanging, (LeashHitch) hanging);
                        if (isAnimal(leashed)) {
                            PlayerUnleashAnimalFromFenceEvent playerUnleashAnimalFromFenceEvent = new PlayerUnleashAnimalFromFenceEvent(player, leashed, hanging.getLocation());
                            Bukkit.getServer().getPluginManager().callEvent(playerUnleashAnimalFromFenceEvent);
                            cancelled = playerUnleashAnimalFromFenceEvent.isCancelled();
                        } else if (isMob(leashed)) {
                            PlayerUnleashMobFromFenceEvent playerUnleashMobFromFenceEvent = new PlayerUnleashMobFromFenceEvent(player, leashed, hanging.getLocation());
                            Bukkit.getServer().getPluginManager().callEvent(playerUnleashMobFromFenceEvent);
                            cancelled = playerUnleashMobFromFenceEvent.isCancelled();
                        }
                        break;
                    default:
                        return;
                }
            } else {
                switch (event.getEntity().getType()) {
                    case ITEM_FRAME:
                        EntityBreakItemFrameEvent entityBreakItemFrameEvent = new EntityBreakItemFrameEvent((Entity) shooter, hanging, event.getCause());
                        Bukkit.getServer().getPluginManager().callEvent(entityBreakItemFrameEvent);
                        cancelled = entityBreakItemFrameEvent.isCancelled();
                        break;
                    case PAINTING:
                        EntityBreakPaintingEvent entityBreakPaintingEvent = new EntityBreakPaintingEvent((Entity) shooter, hanging, event.getCause());
                        Bukkit.getServer().getPluginManager().callEvent(entityBreakPaintingEvent);
                        cancelled = entityBreakPaintingEvent.isCancelled();
                        break;
                    case LEASH_HITCH:
                        LivingEntity leashed = getLeashedEntity(hanging, (LeashHitch) hanging);
                        if (isAnimal(leashed)) {
                            EntityUnleashAnimalFromFenceEvent entityUnleashAnimalFromFenceEvent = new EntityUnleashAnimalFromFenceEvent(remover, leashed, hanging.getLocation());
                            Bukkit.getServer().getPluginManager().callEvent(entityUnleashAnimalFromFenceEvent);
                            cancelled = entityUnleashAnimalFromFenceEvent.isCancelled();
                        } else if (isMob(leashed)) {
                            EntityUnleashMobFromFenceEvent entityUnleashMobFromFenceEvent = new EntityUnleashMobFromFenceEvent(remover, leashed, hanging.getLocation());
                            Bukkit.getServer().getPluginManager().callEvent(entityUnleashMobFromFenceEvent);
                            cancelled = entityUnleashMobFromFenceEvent.isCancelled();
                        }
                        break;
                    default:
                        return;
                }
            }
        } else {
            switch (event.getEntity().getType()) {
                case ITEM_FRAME:
                    EntityBreakItemFrameEvent entityBreakItemFrameEvent = new EntityBreakItemFrameEvent(remover, hanging, event.getCause());
                    Bukkit.getServer().getPluginManager().callEvent(entityBreakItemFrameEvent);
                    cancelled = entityBreakItemFrameEvent.isCancelled();
                    break;
                case PAINTING:
                    EntityBreakPaintingEvent entityBreakPaintingEvent = new EntityBreakPaintingEvent(remover, hanging, event.getCause());
                    Bukkit.getServer().getPluginManager().callEvent(entityBreakPaintingEvent);
                    cancelled = entityBreakPaintingEvent.isCancelled();
                    break;
                case LEASH_HITCH:
                    LivingEntity leashed = getLeashedEntity(hanging, (LeashHitch) hanging);
                    if (isAnimal(leashed)) {
                        EntityUnleashAnimalFromFenceEvent entityUnleashAnimalFromFenceEvent = new EntityUnleashAnimalFromFenceEvent(remover, leashed, hanging.getLocation());
                        Bukkit.getServer().getPluginManager().callEvent(entityUnleashAnimalFromFenceEvent);
                        cancelled = entityUnleashAnimalFromFenceEvent.isCancelled();
                    } else if (isMob(leashed)) {
                        EntityUnleashMobFromFenceEvent entityUnleashMobFromFenceEvent = new EntityUnleashMobFromFenceEvent(remover, leashed, hanging.getLocation());
                        Bukkit.getServer().getPluginManager().callEvent(entityUnleashMobFromFenceEvent);
                        cancelled = entityUnleashMobFromFenceEvent.isCancelled();
                    }
                    break;
                default:
                    return;
            }
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onVehicleDestroy(VehicleDestroyEvent event) {
        if (Config.isWorldDisabled(event.getVehicle().getWorld())) {
            return; // cities not enabled in this world
        }

        Entity attacker = event.getAttacker();
        if (attacker == null) {
            return;
        }

        boolean cancelled;
        switch (attacker.getType()) {
            case PLAYER:
                PlayerDestroyVehicleEvent playerDestroyVehicleEvent = new PlayerDestroyVehicleEvent((Player) attacker, event.getVehicle());
                Bukkit.getServer().getPluginManager().callEvent(playerDestroyVehicleEvent);
                cancelled = playerDestroyVehicleEvent.isCancelled();
                break;
            default:
                EntityDestroyVehicleEvent entityDestroyVehicleEvent = new EntityDestroyVehicleEvent(attacker, event.getVehicle());
                Bukkit.getServer().getPluginManager().callEvent(entityDestroyVehicleEvent);
                cancelled = entityDestroyVehicleEvent.isCancelled();
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onVehicleEnter(VehicleEnterEvent event) {
        if (Config.isWorldDisabled(event.getVehicle().getWorld())) {
            return; // cities not enabled in this world
        }

        boolean cancelled;
        switch (event.getEntered().getType()) {
            case PLAYER:
                PlayerEnterVehicleEvent playerEnterVehicleEvent = new PlayerEnterVehicleEvent((Player) event.getEntered(), event.getVehicle());
                Bukkit.getServer().getPluginManager().callEvent(playerEnterVehicleEvent);
                cancelled = playerEnterVehicleEvent.isCancelled();
                break;
            default:
                EntityEnterVehicleEvent entityEnterVehicleEvent = new EntityEnterVehicleEvent(event.getEntered(), event.getVehicle());
                Bukkit.getServer().getPluginManager().callEvent(entityEnterVehicleEvent);
                cancelled = entityEnterVehicleEvent.isCancelled();
        }

        if (cancelled) {
            event.setCancelled(true);
        }
    }

    private LivingEntity getLeashedEntity(Entity holder, LeashHitch leash) {
        if (leash == null || holder == null) {
            return null;
        }
        for (Entity near : leash.getNearbyEntities(15, 15, 15)) {
            if (!(near instanceof LivingEntity)) {
                continue;
            }
            LivingEntity living = (LivingEntity) near;
            try {
                if (living.getLeashHolder().equals(holder)) {
                    return living;
                }
            } catch (IllegalStateException ignore) {
            }
        }
        return null;
    }

    private boolean isAnimal(Entity entity) {
        if (entity == null) {
            return false;
        }
        switch (entity.getType()) {
            case BAT:
            case CHICKEN:
            case COW:
            case HORSE:
            case IRON_GOLEM:
            case MUSHROOM_COW:
            case OCELOT:
            case PIG:
            case RABBIT:
            case SHEEP:
            case SNOWMAN:
            case SQUID:
            case WOLF:
                return true;
            default:
                return false;
        }
    }

    private boolean isMob(Entity entity) {
        if (entity == null) {
            return false;
        }
        switch (entity.getType()) {
            case BLAZE:
            case CAVE_SPIDER:
            case CREEPER:
            case ENDER_DRAGON:
            case ENDER_SIGNAL:
            case ENDERMAN:
            case ENDERMITE:
            case GHAST:
            case GIANT:
            case GUARDIAN:
            case MAGMA_CUBE:
            case PIG_ZOMBIE:
            case SILVERFISH:
            case SKELETON:
            case SLIME:
            case SPIDER:
            case WITCH:
            case WITHER:
            case ZOMBIE:
                return true;
            default:
                return false;
        }
    }

    private boolean isRedstone(Block block) {
        if (block == null) {
            return false;
        }
        switch (block.getType()) {
            case DAYLIGHT_DETECTOR:
            case DAYLIGHT_DETECTOR_INVERTED:
            case DIODE:
            case DIODE_BLOCK_OFF:
            case DIODE_BLOCK_ON:
            case GOLD_PLATE:
            case IRON_PLATE:
            case LEVER:
            case REDSTONE_COMPARATOR:
            case REDSTONE_COMPARATOR_OFF:
            case REDSTONE_COMPARATOR_ON:
            case STONE_BUTTON:
            case STONE_PLATE:
            case TRIPWIRE:
            case TRIPWIRE_HOOK:
            case WOOD_BUTTON:
            case WOOD_PLATE:
            case NOTE_BLOCK:
            case JUKEBOX:
                return true;
            default:
                return false;
        }
    }
}
