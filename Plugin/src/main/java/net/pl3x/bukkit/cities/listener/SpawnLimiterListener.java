package net.pl3x.bukkit.cities.listener;

import net.pl3x.bukkit.cities.Logger;
import net.pl3x.bukkit.cities.configuration.SpawnLimiterConfig;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.util.HashMap;
import java.util.Map;

public class SpawnLimiterListener implements Listener {
    private final Map<EntityType, Long> cooldown = new HashMap<>();

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        EntityType type = event.getEntityType();

        if (SpawnLimiterConfig.isReasonDisabled(event.getSpawnReason())) {
            return; // reason disabled
        }

        LivingEntity entity = event.getEntity();
        if (SpawnLimiterConfig.isWorldDisabled(entity.getWorld())) {
            return; // world disabled
        }

        int limit = SpawnLimiterConfig.getEntityLimit(type);
        if (limit == -1) {
            return; // type disabled
        }

        // check for possible duplication issues
        int milli = SpawnLimiterConfig.getCooldown();
        if (milli > 0) {
            long now = System.currentTimeMillis();
            if (cooldown.containsKey(type) && cooldown.get(type) >= now) {
                if (SpawnLimiterConfig.debugMode()) {
                    Logger.info("&7" + type.name() + " &cis on &7cooldown");
                }
                event.setCancelled(true);
                return; // not spawning this type yet
            }
            if (SpawnLimiterConfig.debugMode()) {
                Logger.info("&7" + type.name() + " &anot on &7cooldown");
            }
            cooldown.put(type, now + milli);
        }

        int radius = SpawnLimiterConfig.getRadius();

        int count = 0;
        for (Entity target : entity.getNearbyEntities(radius, radius, radius)) {
            if (!target.getType().equals(type)) {
                continue;
            }
            count++;
        }

        if (count >= limit) {
            if (SpawnLimiterConfig.debugMode()) {
                Logger.info("&7" + type.name() + " &cspawn limited &7(" + count + " > " + limit);
            }
            event.setCancelled(true);
        }
    }
}
