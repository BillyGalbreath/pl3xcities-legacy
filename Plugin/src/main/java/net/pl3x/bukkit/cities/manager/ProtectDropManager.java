package net.pl3x.bukkit.cities.manager;

import net.pl3x.bukkit.cities.ProtectDrop;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.HashSet;

public class ProtectDropManager {
    private static ProtectDropManager manager;

    public static ProtectDropManager getManager() {
        if (manager == null) {
            manager = new ProtectDropManager();
        }
        return manager;
    }

    private final Collection<ProtectDrop> protectedDrops = new HashSet<>();

    public void addDrops(Collection<ProtectDrop> drops) {
        protectedDrops.addAll(drops);
    }

    public void removeDrops(Collection<ProtectDrop> drops) {
        protectedDrops.removeAll(drops);
    }

    @SuppressWarnings("deprecation")
    public ProtectDrop getProtectDrop(Item item) {
        ItemStack stack = item.getItemStack();

        Material mat = stack.getType();
        byte data = stack.getData().getData();
        long tick = item.getWorld().getFullTime();
        Location location = item.getLocation();

        // cleanup location to block location
        location = new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ());


        for (ProtectDrop drop : protectedDrops) {
            if (!drop.getMaterial().equals(mat)) {
                continue;
            }
            if (drop.getData() != data) {
                continue;
            }
            if (drop.getTick() < tick - 1 || drop.getTick() > tick + 1) {
                continue;
            }

            int x1 = drop.getLocation().getBlockX();
            int y1 = drop.getLocation().getBlockY();
            int z1 = drop.getLocation().getBlockZ();

            int x2 = location.getBlockX();
            int y2 = location.getBlockY();
            int z2 = location.getBlockZ();

            if (x1 < x2 - 2 || x1 > x2 + 2 ||
                    y1 < y2 - 2 || y1 > y2 + 2 ||
                    z1 < z2 - 2 || z1 > z2 + 2) {
                continue;
            }
            return drop;
        }

        return null;
    }
}
