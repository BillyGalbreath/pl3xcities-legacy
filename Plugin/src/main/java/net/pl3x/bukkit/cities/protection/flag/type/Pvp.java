package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.player.PlayerDamagePlayerEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to control player vs player damage
 */
public class Pvp extends FlagListener {
    private final String name = "pvp";

    public Pvp(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onDamagePlayer(PlayerDamagePlayerEvent event) {
        if (Flags.isAllowed(name, null, event.getDamaged().getLocation())) {
            return;
        }
        new Chat(Lang.PVP_DENY).send(event.getDamager());
        event.setCancelled(true);
    }
}
