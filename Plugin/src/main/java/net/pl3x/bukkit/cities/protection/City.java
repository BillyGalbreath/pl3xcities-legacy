package net.pl3x.bukkit.cities.protection;

import net.pl3x.bukkit.cities.protection.flag.Flag;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.usercache.api.CachedPlayer;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class City extends Protection {
    private final String name;
    private int plotLimit = 0;
    private Set<CityChunk> chunks = new HashSet<>();
    private Set<Plot> plots = new HashSet<>();
    private List<String> plotRestrictedFlags = new ArrayList<>();

    public City(String cityName) {
        this.name = cityName;
    }

    public String getName() {
        return name;
    }

    public int getPlotLimit() {
        return plotLimit;
    }

    public void setPlotLimit(int limit) {
        this.plotLimit = limit;
    }

    public List<String> getPlotRestrictedFlags() {
        return plotRestrictedFlags;
    }

    public void setPlotRestrictedFlags(List<String> plotRestrictedFlags) {
        this.plotRestrictedFlags = plotRestrictedFlags;
    }

    public boolean isPlotRestrictedFlag(String flag) {
        for (String restricted : plotRestrictedFlags) {
            if (restricted.equalsIgnoreCase(flag)) {
                return true;
            }
        }
        return false;
    }

    public void addPlotRestrictedFlag(String flag) {
        plotRestrictedFlags.add(flag);
    }

    public void removePlotRestrictedFlag(String flag) {
        plotRestrictedFlags.remove(flag);
    }

    public int getArea() {
        return getChunks().size() * 16 * 16;
    }

    /**
     * Check if city has PVP enabled.
     * <p>
     * PVP is on by default. Only a city flag can turn it off.
     *
     * @return True if PVP is on
     */
    public boolean hasPvp() {
        Flag flag = getFlag("pvp");
        return flag == null || !flag.getState().equals(FlagState.DENY);
    }

    public Set<CityChunk> getChunks() {
        return chunks;
    }

    public void setChunks(Set<CityChunk> chunks) {
        this.chunks = chunks;
    }

    public void addChunk(CityChunk chunk) {
        chunks.add(chunk);
    }

    public void removeChunk(CityChunk chunk) {
        chunks.remove(chunk);
    }

    public Set<Plot> getPlots() {
        return plots;
    }

    public void unloadPlots() {
        this.plots = null;
    }

    public void addPlot(Plot plot) {
        plots.add(plot);
    }

    public void removePlot(Plot plot) {
        plots.remove(plot);
    }

    public int getPopulation() {
        return getResidents().size();
    }

    public Set<UUID> getResidents() {
        Set<UUID> residents = new HashSet<>(getOwners().keySet());
        try {
            residents.addAll(getMembers().keySet());
        } catch (Exception ignore) {
        }
        for (Plot plot : getPlots()) {
            try {
                residents.addAll(plot.getOwners().keySet());
            } catch (Exception ignore) {
            }
            try {
                residents.addAll(plot.getMembers().keySet());
            } catch (Exception ignore) {
            }
        }
        return residents;
    }

    public Plot getPlot(Location location) {
        for (Plot plot : getPlots()) {
            PlotRegion region = plot.getRegion();
            if (region == null) {
                continue;
            }
            if (region.contains(location)) {
                return plot;
            }
        }
        return null;
    }

    public String getNextPlotId() {
        int i = 0;
        while (true) {
            String id = getName() + "_" + String.format("%06d", i);
            if (hasPlotWithId(id)) {
                i++;
                continue;
            }
            return id;
        }
    }

    private boolean hasPlotWithId(String id) {
        for (Plot plot : getPlots()) {
            if (plot.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public int numberOfOwnedPlots(CachedPlayer player) {
        return numberOfOwnedPlots(player.getUniqueId());
    }

    public int numberOfOwnedPlots(Player player) {
        return numberOfOwnedPlots(player.getUniqueId());
    }

    public int numberOfOwnedPlots(UUID uuid) {
        int count = 0;
        for (Plot plot : getPlots()) {
            if (plot.isOwner(uuid)) {
                count++;
            }
        }
        return count;
    }
}
