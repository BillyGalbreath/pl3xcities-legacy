package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.entity.MobSpawnEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to stop endermites from spawning
 */
public class SpawnEndermites extends FlagListener {
    private final String name = "spawn-endermites";

    public SpawnEndermites(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onMobSpawn(MobSpawnEvent event) {
        if (event.getEntity().getType() != EntityType.ENDERMITE) {
            return;
        }
        if (Flags.isAllowed(name, null, event.getLocation(), false, false)) {
            event.setCancelled(false); // override spawn-mobs flag
        }
        event.setCancelled(true);
    }
}
