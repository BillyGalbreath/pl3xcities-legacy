package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.entity.EntityDestroyVehicleEvent;
import net.pl3x.bukkit.cities.event.player.PlayerDestroyVehicleEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to stop vehicles from breaking
 */
public class VehicleBreak extends FlagListener {
    private final String name = "vehicle-break";

    public VehicleBreak(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops vehicles from breaking
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerDestroyVehicle(PlayerDestroyVehicleEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.vehicledestroy")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getVehicle().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.VEHICLE_DESTROY_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops vehicles from breaking
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityDestroyVehicle(EntityDestroyVehicleEvent event) {
        if (Flags.isAllowed(name, null, event.getVehicle().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
