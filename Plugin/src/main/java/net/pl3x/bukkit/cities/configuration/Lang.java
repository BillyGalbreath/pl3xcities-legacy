package net.pl3x.bukkit.cities.configuration;

import net.pl3x.bukkit.cities.Logger;
import net.pl3x.bukkit.cities.Pl3xCities;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Lang {
    WORLD_DISABLED("&4This command is not allowed in this world!"),
    INVALID_DIRECTION("&4Invalid direction: {direction}"),

    UNKNOWN_SUBCOMMAND("&4Unknown Subcommand!"),
    UNKNOWN_ERROR("&4An unknown error has happened!"),
    PLAYER_COMMAND("&4This command is only available to players."),
    COMMAND_NO_PERMISSION("&4You do not have permission for this command!"),

    NO_PERMISSION_BUY_CITY("You do not have permission to purchase a city!"),
    NO_PERMISSION_BUY_PLOT("You do not have permission to purchase a plot!"),

    NOT_A_NUMBER("&4Not a number!"),
    NOT_VALID_VALUE("&4Not a on/off value: {value}"),
    ZERO_NUMBER("&4Number must be greater than 0!"),
    INVALID_PRICE("&4Invalid price amount!"),
    CITY_NOT_FOR_SALE("&4Uh oh! This city isn't for sale!"),
    PLOT_NOT_FOR_SALE("&4Uh oh! This plot isn't for sale!"),
    VAULT_WITHDRAW_ERROR("&4An error has occurred trying to withdraw from account! {error}"),
    VAULT_DEPOSIT_ERROR("&4An error has occurred trying to deposit to account! {error}"),
    CANNOT_AFFORD_CITY_BULKCLAIM("&4You cannot afford to bulk claim these chunks! (cost: {amount})"),
    CANNOT_AFFORD_CITY_CLAIM("&4You cannot afford to claim this city chunk! (cost: {amount})"),
    CANNOT_AFFORD_CITY_UNCLAIM("&4You cannot afford to unclaim this city chunk! (cost: {amount})"),
    CANNOT_AFFORD_PLOT_PURCHASE("&4You cannot afford to buy this plot!"),
    CANNOT_AFFORD_CITY_PURCHASE("&4You cannot afford to buy this city!"),
    VAULT_ACCOUNT_CHARGED("&e{amount} &dhas been deducted from your account."),
    VAULT_ACCOUNT_PAID("&e{amount} &dhas been paid to your account."),
    FORSALE_SIGN_TEXT("For Sale"),

    USER_NOT_FOUND("&4Could not find that player!"),
    NO_NAME_SPECIFIED("&4You must specify a player name!"),
    USER_ALREADY_OWNER("&4That player is already on the owners list!"),
    USER_ALREADY_MEMBER("&4That player is already on the members list!"),
    USER_NOT_OWNER("&4That player is not on the owners list!"),
    USER_NOT_MEMBER("&4That player is not on the members list!"),
    CANNOT_REMOVE_SELF_FROM_OWNERS("&4Cannot remove yourself from owners list!"),
    CANNOT_REMOVE_SELF_FROM_MEMBERS("&4Cannot remove yourself from members list!"),
    USER_CITY_LIMIT_REACHED("&4That player has reached their city limit!"),
    USER_PLOT_LIMIT_REACHED("&4That player has reached their plot limit for this city!"),

    FAILED_FIND_CITY_NAME("&4Failed to find a city by that name!"),
    FAILED_FIND_CITY_LOCATION("&4There is no city here!"),
    FAILED_FIND_PLOT_LOCATION("&4There is no plot here!"),
    PLOT_MUST_BE_STANDING_IN("&4Must be inside the plot!"),

    CITY_NAME_BLACKLISTED("&4That city name is not allowed!"),
    CITY_NAME_NOT_SPECIFIED("&4You must specify a city name!"),
    CITY_NAME_INVALID_CHARACTERS("&4City names must contain valid characters only!"),
    CITY_LIMIT_REACHED("&4You have reached your city limit!"),
    CITY_ALREADY_EXISTS_AT_LOCATION("&4A city already exists here!"),
    CITY_CLAIM_NOT_CONNECTED("&4This chunk is not connected to the city!"),
    CITY_CLAIM_TOUCHING_OTHER_CITY("&4This chunk is touching another city!"),
    CITY_BULKCLAIM_TOUCHING_OTHER_CITY("&4Your bulk selection is touching another city!"),
    CITY_BULKCLAIM_NO_UNCLAIMED_CHUNKS("&4Your selection does not contain any unclaimed chunks!"),
    CITY_BULKCLAIM_NOT_CONNECTED("&4Your selection is not connected to the city!"),
    CITY_UNCLAIM_SPAWN_CHUNK_ERROR("&4Cannot unclaim the city spawn chunk!"),
    CITY_UNCLAIM_PLOT_CHUNK_ERROR("&4This chunk contains a plot!"),
    CITY_CREATE_SUCCESS("&dYou have successfully claimed your city!"),
    CITY_CREATE_BROADCAST("&d{owner} has just created a new city!\n&dUse &7/city spawn {city-name} &dto travel there."),
    CITY_EXPAND_SUCCESS("&dYou have successfully expanded your city!"),
    CITY_UNCLAIM_SUCCESS("&dYou have successfully unclaimed a city chunk!"),
    CITY_DELETE_SUCCESS("&dYou have successfully deleted your city!"),
    CITY_FLAGS_LIST("&dCity Flags:"),
    CITY_FLAGS_SET("&dCity flag set."),
    CITY_FLAGS_UNSET("&dCity flag successfully removed."),
    CITY_MEMBERS_LIST("&dCity Members:"),
    CITY_MEMBER_ADDED("&dCity member successfully added."),
    CITY_MEMBER_REMOVED("&dCity member successfully removed."),
    CITY_OWNERS_LIST("&dCity Owners:"),
    CITY_OWNER_ADDED("&dCity owner successfully added."),
    CITY_OWNER_REMOVED("&dCity owner successfully removed."),
    CITY_PLOTLIMIT_SET("&dCity plot-limit successfully set."),
    CITY_PLOTRESTRICTED_LIST("&dPlot Restricted Flags:"),
    CITY_PLOTRESTRICTED_ADDED("&dCity plot restricted flag added."),
    CITY_PLOTRESTRICTED_REMOVED("&dCity plot restricted flag removed."),
    CITY_PURCHASED("&dYou have purchased the city &7{city} &dfor &7{price}&d!"),
    SETSPAWN("&dSpawn has been set for &7{city}&d."),
    SPAWN("&dTeleporting to &7{city} &dspawn."),

    PLOT_CREATE_SUCCESS("&dPlot sucessfully created."),
    PLOT_DELETE_SUCCESS("&dPlot sucessfully deleted."),
    PLOT_FLAGS_LIST("&dPlot Flags:"),
    PLOT_FLAGS_SET("&dPlot flag set."),
    PLOT_FLAGS_UNSET("&dPlot flag successfully removed."),
    PLOT_LIMIT_REACHED("&4You have reached your plot limit for this city!"),
    PLOT_MEMBERS_LIST("&dPlot Members:"),
    PLOT_MEMBER_ADDED("&dPlot member successfully added."),
    PLOT_MEMBER_REMOVED("&dPlot member successfully removed."),
    PLOT_OWNERS_LIST("&dPlot Owners:"),
    PLOT_OWNER_ADDED("&dPlot owner successfully added."),
    PLOT_OWNER_REMOVED("&dPlot owner successfully removed."),
    PLOT_PURCHASED("&dYou have purchased this plot for &7{price}&d!"),

    FLAG_NOT_SPECIFIED("&4You must specify a flag!"),
    FLAG_NOT_FOUND("&4Flag not found!"),
    FLAG_STATE_INVALID("&4Flag state is invalid. Expected allow, deny, or none"),
    FLAG_ALREADY_RESTRICTED("&4Flag is already restricted!"),
    FLAG_NOT_RESTRICTED("&4Flag is not restricted!"),
    FLAG_IS_RESTRICTED("&4That flag is restricted by the city owner!"),
    FLAGS_AVAILABLE("&dFlags Available&e: &7{flags}"),

    NOT_CITY_OWNER("&4You do not own this city!"),
    NOT_PLOT_OWNER("&4You do not own this plot!"),
    ALREADY_CITY_OWNER("&4Already city owner!"),
    ALREADY_PLOT_OWNER("&4Already plot owner!"),

    ANIMAL_CHANGE_COLOR_DENY("&4You do not have permission to change that animal's color!"),
    ANIMAL_FEED_DENY("&4You do not have permission to feed that animal!"),
    ANIMAL_HURT_DENY("&4You do not have permission to hurt that animal!"),
    ANIMAL_LEASH_DENY("&4You do not have permission to leash that animal!"),
    ANIMAL_MILK_COW_DENY("&4You do not have permission to milk that cow!"),
    ANIMAL_RENAME_DENY("&4You do not have permission to rename that animal!"),
    ANIMAL_RIDE_HORSE_DENY("&4You do not have permission to ride that horse!"),
    ANIMAL_SHEAR_DENY("&4You do not have permission to shear that animal!"),
    ANIMAL_SOUP_COW_DENY("&4You do not have permission to soup that cow!"),
    ANIMAL_SPAWN_BABY_DENY("&4You do not have permission to spawn baby animal!"),
    ANIMAL_TAME_DENY("&4You do not have permission to tame that animal!"),
    BUILD_DENY("&4You do not have permission to build here!"),
    EAT_CAKE_DENY("&4You do not have permission to eat that cake!"),
    ENTRY_DENY("&4You do not have permission to enter here!"),
    EXIT_DENY("&4You do not have permission to exit here!"),
    ENDERPEARL_DENY("&4You do not have permission to use enderpearls here!"),
    FISHING_DENY("&4You do not have permission to fish here!"),
    LIGHTER_DENY("&4You do not have permission to light fires here!"),
    MOB_HURT_DENY("&4You do not have permission to hurt that mob!"),
    MOB_LEASH_DENY("&4You do not have permission to leash that mob!"),
    MOB_RENAME_DENY("&4You do not have permission to rename that animal!"),
    MOB_SPAWN_BABY_DENY("&4You do not have permission to spawn baby animal!"),
    PVP_DENY("&4PvP is disabled here!"),
    SLEEP_DENY("&4You cannot sleep in that bed!"),
    USE_CONTAINER_DENY("&4You do not have permission to open that!"),
    USE_DOOR_DENY("&4You do not have permission to use that door!"),
    USE_REDSTONE_DENY("&4You do not have permission to use that redstone!"),
    VEHICLE_PLACE_DENY("&4You do not have permission to place that here!"),
    VEHICLE_DESTROY_DENY("&4You do not have permission to break that!"),
    VILLAGER_HURT_DENY("&4You do not have permission to hurt that villager!"),
    VILLAGER_SPAWN_BABY_DENY("&4You do not have permission to spawn baby villager!"),

    SELECTION_NOT_FOUND("&4You have not made a selection!"),
    SELECTION_NOT_COMPLETE("&4Your selection is not complete!"),
    SELECTION_NOT_IN_CITY("&4Your selection is not completely inside the city!"),
    SELECTION_OVERLAPS_PLOT("&4Your selection overlaps another plot"),
    SELECTION_CLEARED("&dSelection has been cleared."),
    SELECTION_CONTRACTED("&dSelection contracted."),
    SELECTION_EXPANDED("&dSelection expanded."),
    SELECTION_PRIMARY_SET("&dPrimary point set: &3[&7{x}&e, &7{y}&e, &7{z}&3]"),
    SELECTION_SECONDARY_SET("&dSecondary point set: &3[&7{x}&e, &7{y}&e, &7{z}&3]"),
    SELECTION_SHIFTED("&dSelection shifted."),

    SOUNDS_CHECK("&dSounds are &e{enabled}&d."),
    SOUNDS_ENABLED("&dSounds are now &eenabled&d."),
    SOUNDS_DISABLED("&dSounds are now &edisabled&d."),

    TITLE("&2&l{city-name}"),
    SUBTITLE("&6PvP: {pvp}"),
    ACTIONBAR("&2&l{city-name}"),

    WORD_ON("On"),
    WORD_OFF("Off"),

    WORD_YES("Yes"),
    WORD_NO("No"),

    AVAILABLE_SUBCOMMANDS("&dAvailable Subcommands:"),
    COMMAND_DESCRIPTION("  &3{command} &e- &7{description}"),

    CMD_DESC_CITIES("Manage plugin settings"),
    CMD_DESC_CITIES_RELOAD("Reload config.yml and all data"),
    CMD_DESC_CITIES_SOUNDS("Turn on plugin sounds"),
    CMD_DESC_CITY("Manage a city"),
    CMD_DESC_CITY_CLAIM("Claim a city chunk"),
    CMD_DESC_CITY_BULKCLAIM("Claim city chunks in selection"),
    CMD_DESC_CITY_FLAGS("Manage the city flags"),
    CMD_DESC_CITY_FLAGS_LIST("List the city flags"),
    CMD_DESC_CITY_FLAGS_SET("Set a city flag"),
    CMD_DESC_CITY_INFO("Display info about a city"),
    CMD_DESC_CITY_MEMBERS("Manage the city members list"),
    CMD_DESC_CITY_MEMBERS_LIST("List city members"),
    CMD_DESC_CITY_MEMBERS_ADD("Add a city member"),
    CMD_DESC_CITY_MEMBERS_REMOVE("Remove a city member"),
    CMD_DESC_CITY_OWNERS("Manage the city owners list"),
    CMD_DESC_CITY_OWNERS_LIST("List city owners"),
    CMD_DESC_CITY_OWNERS_ADD("Add a city owner"),
    CMD_DESC_CITY_OWNERS_REMOVE("Remove a city owner"),
    CMD_DESC_CITY_PLOTLIMIT("Set the city plot limit"),
    CMD_DESC_CITY_PLOTRESTRICTED("Manage plot restricted flags"),
    CMD_DESC_CITY_PLOTRESTRICTED_LIST("List plot restricted flags"),
    CMD_DESC_CITY_PLOTRESTRICTED_ADD("Add a plot restricted flag"),
    CMD_DESC_CITY_PLOTRESTRICTED_REMOVE("Remove a plot restricted flag"),
    CMD_DESC_CITY_SETSPAWN("Set the city spawn point"),
    CMD_DESC_CITY_SPAWN("Teleport to the city spawn"),
    CMD_DESC_CITY_UNCLAIM("Unclaim a city chunk"),
    CMD_DESC_PLOT("Manage a plot"),
    CMD_DESC_PLOT_CREATE("Create a new plot"),
    CMD_DESC_PLOT_DELETE("Delete a plot"),
    CMD_DESC_PLOT_FLAGS("Manage the plot flags"),
    CMD_DESC_PLOT_FLAGS_LIST("List the plot flags"),
    CMD_DESC_PLOT_FLAGS_SET("Set a plot flag"),
    CMD_DESC_PLOT_INFO("Display info about a plot"),
    CMD_DESC_PLOT_MEMBERS("Manage the plot members list"),
    CMD_DESC_PLOT_MEMBERS_LIST("List plot members"),
    CMD_DESC_PLOT_MEMBERS_ADD("Add a plot member"),
    CMD_DESC_PLOT_MEMBERS_REMOVE("Remove a plot member"),
    CMD_DESC_PLOT_OWNERS("Manage the plot owners list"),
    CMD_DESC_PLOT_OWNERS_LIST("List plot owners"),
    CMD_DESC_PLOT_OWNERS_ADD("Add a plot owner"),
    CMD_DESC_PLOT_OWNERS_REMOVE("Remove a plot owner"),
    CMD_DESC_PLOT_SELECT("Create new selection from existing plot"),
    CMD_DESC_PLOT_SHOW("Show the plot visualizer"),
    CMD_DESC_SELECTION("Manage current selection"),
    CMD_DESC_SELECTION_CLEAR("Clears current selection"),
    CMD_DESC_SELECTION_CONTRACT("Contracts current selection"),
    CMD_DESC_SELECTION_EXPAND("Expands current selection"),
    CMD_DESC_SELECTION_SHIFT("Shifts current selection"),

    CMD_HELP_CITIES_RELOAD("&dReload config.yml and all data:\n   &e/&7cities reload"),
    CMD_HELP_CITIES_SOUNDS("&dTurn on plugin sounds:\n   &e/&7cities sounds [value]"),
    CMD_HELP_CITY_CLAIM("&dClaim a city chunk:\n   &e/&7city claim [name]"),
    CMD_HELP_CITY_BULKCLAIM("&dClaim city chunks in bulk:\n   &e/&7city bulk-claim [name]"),
    CMD_HELP_CITY_FLAGS_LIST("&dView the flags of the city:\n   &e/&7city flags list"),
    CMD_HELP_CITY_FLAGS_SET("&dSet a flag to the city:\n   &e/&7city flags set [flag] [state]"),
    CMD_HELP_CITY_INFO("&dGet information about the city:\n   &e/&7city info [name]"),
    CMD_HELP_CITY_MEMBERS_LIST("&dView the members of the city:\n   &e/&7city members list"),
    CMD_HELP_CITY_MEMBERS_ADD("&dAdd a member to the city:\n   &e/&7city members add [player]"),
    CMD_HELP_CITY_MEMBERS_REMOVE("&dRemove a member from the city:\n   &e/&7city members remove [player]"),
    CMD_HELP_CITY_OWNERS_LIST("&dView the owners of the city:\n   &e/&7city owners list"),
    CMD_HELP_CITY_OWNERS_ADD("&dAdd an owner to the city:\n   &e/&7city owners add [player]"),
    CMD_HELP_CITY_OWNERS_REMOVE("&dRemove an owner from the city:\n   &e/&7city owners remove [player]"),
    CMD_HELP_CITY_PLOTLIMIT("&dSet the city plot limit:\n   &e/&7city plot-limit [value]"),
    CMD_HELP_CITY_PLOTRESTRICTED_LIST("&dView the plot restricted flags:\n   &e/&7city plot-restricted list"),
    CMD_HELP_CITY_PLOTRESTRICTED_ADD("&dAdd a plot restricted flag:\n   &e/&7city plot-restricted add [value]"),
    CMD_HELP_CITY_PLOTRESTRICTED_REMOVE("&dRemove a plot restricted flag:\n   &e/&7city plot-restricted remove [value]"),
    CMD_HELP_CITY_SETSPAWN("&dSet the city spawn point:\n   &e/&7city setspawn"),
    CMD_HELP_CITY_SPAWN("&dTeleport to the city spawn point:\n   &e/&7city spawn [city]"),
    CMD_HELP_CITY_UNCLAIM("&dUnclaim a city chunk:\n   &e/&7city unclaim (name)"),
    CMD_HELP_PLOT_CREATE("&dCreate a new plot where you are standing:\n   &e/&7plot create"),
    CMD_HELP_PLOT_DELETE("&dDelete a plot where you are standing:\n   &e/&7plot delete"),
    CMD_HELP_PLOT_INFO("&dGet information about the plot:\n   &e/&7plot info"),
    CMD_HELP_PLOT_FLAGS_LIST("&dView the flags of the plot:\n   &e/&7plot flags list"),
    CMD_HELP_PLOT_FLAGS_SET("&dset a flag to the plot:\n   &e/&7plot flags set [flag] [state]"),
    CMD_HELP_PLOT_MEMBERS_LIST("&dView the members of the plot:\n   &e/&7plot members list"),
    CMD_HELP_PLOT_MEMBERS_ADD("&dAdd a member to the plot:\n   &e/&7plot members add [player]"),
    CMD_HELP_PLOT_MEMBERS_REMOVE("&dRemove a member from the plot:\n   &e/&7plot members remove [player]"),
    CMD_HELP_PLOT_OWNERS_LIST("&dView the owners of the plot:\n   &e/&7plot owners list"),
    CMD_HELP_PLOT_OWNERS_ADD("&dAdd an owner to the plot:\n   &e/&7plot owners add [player]"),
    CMD_HELP_PLOT_OWNERS_REMOVE("&dRemove an owner from the plot:\n   &e/&7plot owners remove [player]"),
    CMD_HELP_PLOT_SELECT("&dCreate new selection from existing plot:\n   &e/&7plot select"),
    CMD_HELP_PLOT_SHOW("&dShow the plot where you are standing:\n   &e/&7plot show"),
    CMD_HELP_SELECTION_CLEAR("&dClear your current selection:\n   &e/&7selection clear"),
    CMD_HELP_SELECTION_CONTRACT("&dContract your current selection:\n   &e/&7selection contract [amount] [direction]"),
    CMD_HELP_SELECTION_EXPAND("&dExpand your current selection:\n   &e/&7selection expand [amount] [direction]"),
    CMD_HELP_SELECTION_SHIFT("&dShift your current selection:\n   &e/&7selection shift [amount] [direction]");

    private final String def;

    private static File configFile;
    private static FileConfiguration config;

    Lang(String def) {
        this.def = def;
        reload();
    }

    public static void reload() {
        reload(false);
    }

    public static void reload(boolean force) {
        if (configFile == null || force) {
            String lang = Config.LANGUAGE_FILE.getString();
            Logger.debug("Loading language file: " + lang);
            configFile = new File(Pl3xCities.getPlugin(Pl3xCities.class).getDataFolder(), lang);
            if (!configFile.exists()) {
                Pl3xCities.getPlugin(Pl3xCities.class).saveResource(Config.LANGUAGE_FILE.getString(), false);
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    @Override
    public String toString() {
        String value = config.getString(name());
        if (value == null) {
            value = config.getString(getKey());
        }
        if (value == null) {
            Logger.warn("Missing lang data in file: " + getKey());
            value = def;
        }
        if (value == null) {
            Logger.error("Missing default lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}
