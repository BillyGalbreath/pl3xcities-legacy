package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerWalkEvent extends PlayerEvent {
    private final Location to;
    private final Location from;

    public PlayerWalkEvent(Player player, Location to, Location from) {
        super(player);
        this.to = to;
        this.from = from;
    }

    public Location getTo() {
        return to;
    }

    public Location getFrom() {
        return from;
    }
}
