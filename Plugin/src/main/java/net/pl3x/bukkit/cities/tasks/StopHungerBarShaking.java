package net.pl3x.bukkit.cities.tasks;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class StopHungerBarShaking extends BukkitRunnable {
    private final Player player;

    public StopHungerBarShaking(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        player.setSaturation(0.001F);
    }
}
