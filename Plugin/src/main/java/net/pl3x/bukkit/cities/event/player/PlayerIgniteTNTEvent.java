package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerIgniteTNTEvent extends PlayerEvent {
    private final Block block;

    public PlayerIgniteTNTEvent(Player player, Block block) {
        super(player);
        this.block = block;
    }

    public Block getBlock() {
        return block;
    }

    public Block getTNTBlock() {
        return block;
    }
}
