package net.pl3x.bukkit.cities.tasks;

import net.pl3x.bukkit.cities.ProtectDrop;
import net.pl3x.bukkit.cities.manager.ProtectDropManager;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collection;

public class ForgetProtectDrops extends BukkitRunnable {
    private final Collection<ProtectDrop> drops;

    public ForgetProtectDrops(Collection<ProtectDrop> drops) {
        this.drops = drops;
    }

    @Override
    public void run() {
        ProtectDropManager.getManager().removeDrops(drops);
    }
}
