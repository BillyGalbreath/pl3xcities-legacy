package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.entity.EntityDamageVillagerEvent;
import net.pl3x.bukkit.cities.event.player.PlayerDamageVillagerEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to protect villagers
 */
public class HurtVillagers extends FlagListener {
    private final String name = "hurt-villagers";

    public HurtVillagers(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops players from hurting villagers
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onDamageVillager(PlayerDamageVillagerEvent event) {
        Player player = event.getDamager();
        if (PermManager.hasPerm(player, "cities.override.use.villagers")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.VILLAGER_HURT_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops entities (mobs) from hurting villagers
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onDamageVillager(EntityDamageVillagerEvent event) {
        if (Flags.isAllowed(name, null, event.getEntity().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
