package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.player.PlayerSplashPlayerEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to control player vs player splash potion damage
 */
public class SplashPotion extends FlagListener {
    private final String name = "splash-potion";

    public SplashPotion(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerSplashPlayer(PlayerSplashPlayerEvent event) {
        if (Flags.isAllowed(name, null, event.getSplashed().getLocation(), false, false)) {
            return;
        }

        event.setCancelled(true);
    }
}
