package net.pl3x.bukkit.cities.listener;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.configuration.CityConfig;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.configuration.PlotConfig;
import net.pl3x.bukkit.cities.hook.Vault;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

public class ForSaleListener implements Listener {
    /*
     * Mark protection for sale
     */
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onSaleSignCreate(SignChangeEvent event) {
        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return;
        }
        String[] lines = event.getLines().clone();
        lines[0] = ChatColor.stripColor(lines[0]);
        if (!lines[0].equalsIgnoreCase(ChatColor.stripColor("[" + Lang.FORSALE_SIGN_TEXT + "]"))) {
            return;
        }
        lines[1] = ChatColor.stripColor(lines[1]);
        lines[2] = ChatColor.stripColor(lines[2]);
        lines[3] = ChatColor.stripColor(lines[3]);
        Player player = event.getPlayer();
        Block block = event.getBlock();
        City city = CityManager.getManager().getCity(block.getLocation());
        if (city == null) {
            return;
        }
        Plot plot = city.getPlot(block.getLocation());
        if (plot != null) {
            if (!plot.isOwner(player) && !PermManager.hasPerm(player, "cities.override.forsale")) {
                boolean hasOwner = !plot.getOwners().isEmpty();
                if (hasOwner || !city.isOwner(player)) {
                    new Chat(Lang.NOT_PLOT_OWNER).send(player);
                    event.setCancelled(true);
                    block.breakNaturally();
                    return;
                }
            }

            double price;
            try {
                price = Double.valueOf(lines[1].replace("$", "").replace(",", ""));
            } catch (NumberFormatException ignore) {
                // try again in case of editing sign via /signedit command (price on line 3)
                try {
                    price = Double.valueOf(lines[2].replace("$", "").replace(",", ""));
                } catch (NumberFormatException ignore2) {
                    price = -1;
                }
            }
            if (price <= 0) {
                new Chat(Lang.INVALID_PRICE).send(player);
                event.setCancelled(true);
                block.breakNaturally();
                return;
            }

            plot.setPrice(price);
            plot.setForSale(true);

            PlotConfig.PRICE.set(plot.getId(), price);
            PlotConfig.FORSALE.set(plot.getId(), true);

            event.setLine(0, ChatColor.translateAlternateColorCodes('&', "&4&l[&r&1" + Lang.FORSALE_SIGN_TEXT + "&4&l]"));
            event.setLine(1, ChatColor.translateAlternateColorCodes('&', "&4&lPlot"));
            event.setLine(2, ChatColor.translateAlternateColorCodes('&', "&2&l" + Vault.format(plot.getPrice())));
            event.setLine(3, ChatColor.translateAlternateColorCodes('&', "&8&oRight click to Buy"));

            return;
        }
        if (!city.isOwner(player) && !PermManager.hasPerm(player, "cities.override.forsale")) {
            new Chat(Lang.NOT_CITY_OWNER).send(player);
            event.setCancelled(true);
            block.breakNaturally();
            return;
        }

        double price;
        try {
            price = Double.valueOf(lines[1].replace("$", "").replace(",", ""));
        } catch (NumberFormatException ignore) {
            // try again in case of editing sign via /signedit command (price on line 3)
            try {
                price = Double.valueOf(lines[2].replace("$", "").replace(",", ""));
            } catch (NumberFormatException ignore2) {
                price = -1;
            }
        }
        if (price <= 0) {
            new Chat(Lang.INVALID_PRICE).send(player);
            event.setCancelled(true);
            block.breakNaturally();
            return;
        }

        city.setPrice(price);
        city.setForSale(true);

        CityConfig.PRICE.set(city.getName(), price);
        CityConfig.FORSALE.set(city.getName(), true);

        event.setLine(0, ChatColor.translateAlternateColorCodes('&', "&4&l[&r&1" + Lang.FORSALE_SIGN_TEXT + "&4&l]"));
        event.setLine(1, ChatColor.translateAlternateColorCodes('&', "&4&lCity"));
        event.setLine(2, ChatColor.translateAlternateColorCodes('&', "&2&l" + Vault.format(city.getPrice())));
        event.setLine(3, ChatColor.translateAlternateColorCodes('&', "&8&oRight click to Buy"));
    }

    /*
     * Delete sale sign from city/plot
     */
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onSaleSignDelete(BlockBreakEvent event) {
        Block block = event.getBlock();
        if (Config.isWorldDisabled(block.getWorld())) {
            return;
        }
        if (!(block.getState() instanceof Sign)) {
            return;
        }
        Sign sign = (Sign) block.getState();
        if (!ChatColor.stripColor(sign.getLine(0)).equalsIgnoreCase(ChatColor.stripColor("[" + Lang.FORSALE_SIGN_TEXT + "]"))) {
            return;
        }
        City city = CityManager.getManager().getCity(block.getLocation());
        if (city == null) {
            return;
        }
        Player player = event.getPlayer();
        Plot plot = city.getPlot(block.getLocation());
        if (plot != null) {
            if (!plot.isOwner(player) && !PermManager.hasPerm(player, "cities.override.forsale")) {
                boolean hasOwner = !plot.getOwners().isEmpty();
                if (hasOwner || !city.isOwner(player)) {
                    new Chat(Lang.NOT_PLOT_OWNER).send(player);
                    event.setCancelled(true);
                    return;
                }
            }

            plot.setPrice(0);
            plot.setForSale(false);

            PlotConfig.PRICE.set(plot.getId(), 0);
            PlotConfig.FORSALE.set(plot.getId(), false);
            return;
        }
        if (!city.isOwner(player) && !PermManager.hasPerm(player, "cities.override.forsale")) {
            new Chat(Lang.NOT_CITY_OWNER).send(player);
            event.setCancelled(true);
            return;
        }

        city.setPrice(0);
        city.setForSale(false);

        CityConfig.PRICE.set(city.getName(), 0);
        CityConfig.FORSALE.set(city.getName(), false);
    }

    /*
     * Player right clicks sign to purchase protection
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onRightClickForSaleSign(PlayerInteractEvent event) {
        if (event.getHand() == null || event.getHand().equals(EquipmentSlot.OFF_HAND)) {
            return; // ignore offhand 2nd packet
        }

        Action action = event.getAction();
        if (!action.equals(Action.RIGHT_CLICK_BLOCK)) {
            return;
        }

        Block block = event.getClickedBlock();
        if (!(block.getState() instanceof Sign)) {
            return;
        }

        Sign sign = (Sign) block.getState();
        if (!ChatColor.stripColor(sign.getLine(0)).equalsIgnoreCase(ChatColor.stripColor("[" + Lang.FORSALE_SIGN_TEXT + "]"))) {
            return;
        }

        Player player = event.getPlayer();

        City city = CityManager.getManager().getCity(block.getLocation());
        if (city == null) {
            return;
        }

        Plot plot = city.getPlot(block.getLocation());
        if (plot != null) {
            if (!player.hasPermission("cities.buy.plot")) {
                new Chat(Lang.NO_PERMISSION_BUY_PLOT).send(player);
                event.setCancelled(true);
                return;
            }

            if (plot.isOwner(player)) {
                new Chat(Lang.ALREADY_PLOT_OWNER).send(player);
                event.setCancelled(true);
                return;
            }

            if (!plot.isForSale()) {
                new Chat(Lang.PLOT_NOT_FOR_SALE).send(player);
                return;
            }

            double price = plot.getPrice();

            if (Vault.getBalance(player) < price) {
                new Chat(Lang.CANNOT_AFFORD_PLOT_PURCHASE).send(player);
                event.setCancelled(true);
                return;
            }

            int limit = city.getPlotLimit();
            int count = 0;
            for (Plot chkPlot : city.getPlots()) {
                if (chkPlot.isOwner(player)) {
                    count++;
                }
                if (limit > 0 && count >= limit && !PermManager.hasPerm(player, "cities.override.plotlimit")) {
                    new Chat(Lang.PLOT_LIMIT_REACHED).send(player);
                    event.setCancelled(true);
                    return;
                }
            }

            try {
                Vault.withdrawPlayer(player.getUniqueId(), price);
                Map<UUID, String> owners = plot.getOwners();
                if (owners == null || owners.isEmpty()) {
                    // pay the city owner(s) if there are no plot owner(s)
                    owners = city.getOwners();
                }
                double deposit = price / owners.size();
                for (Entry<UUID, String> entry : owners.entrySet()) {
                    Vault.depositPlayer(entry.getKey(), deposit);
                }
            } catch (Exception error) {
                new Chat("&4" + error.getMessage()).send(player);
                event.setCancelled(true);
                return;
            }

            Map<UUID, String> map = new HashMap<>();
            map.put(player.getUniqueId(), player.getName());

            plot.setMembers(new HashMap<>()); // simply removes all members
            plot.setOwners(map);
            plot.setForSale(false);

            PlotConfig.MEMBERS.setPlayers(plot.getId(), plot.getMembers());
            PlotConfig.OWNERS.setPlayers(plot.getId(), plot.getOwners());
            PlotConfig.FORSALE.set(plot.getId(), false);

            block.setType(Material.AIR); // remove the sign
            new Chat(Lang.PLOT_PURCHASED
                    .replace("{price}", Vault.format(price)))
                    .send(player);
            return;
        }

        if (!player.hasPermission("cities.buy.city")) {
            new Chat(Lang.NO_PERMISSION_BUY_CITY).send(player);
            event.setCancelled(true);
            return;
        }

        if (city.isOwner(player)) {
            new Chat(Lang.ALREADY_CITY_OWNER).send(player);
            event.setCancelled(true);
            return;
        }

        if (!city.isForSale()) {
            new Chat(Lang.CITY_NOT_FOR_SALE).send(player);
            return;
        }

        double price = city.getPrice();

        if (Vault.getBalance(player) < price) {
            new Chat(Lang.CANNOT_AFFORD_CITY_PURCHASE).send(player);
            event.setCancelled(true);
            return;
        }

        int limit = CityManager.getManager().getLimit(player);
        int owned = CityManager.getManager().numberOfOwnedCities(player);
        if (limit > 0 && owned >= limit && !PermManager.hasPerm(player, "cities.override.plotlimit")) {
            new Chat(Lang.CITY_LIMIT_REACHED).send(player);
            event.setCancelled(true);
            return;
        }

        try {
            Vault.withdrawPlayer(player.getUniqueId(), price);
            Map<UUID, String> owners = city.getOwners();
            double deposit = price / owners.size();
            for (Entry<UUID, String> entry : owners.entrySet()) {
                Vault.depositPlayer(entry.getKey(), deposit);
            }
        } catch (Exception error) {
            new Chat("&4" + error.getMessage()).send(player);
            event.setCancelled(true);
            return;
        }

        Map<UUID, String> map = new HashMap<>();
        map.put(player.getUniqueId(), player.getName());

        city.setMembers(new HashMap<>()); // simply removes all members
        city.setOwners(map);
        city.setForSale(false);

        CityConfig.MEMBERS.setPlayers(city.getName(), city.getMembers());
        CityConfig.OWNERS.setPlayers(city.getName(), city.getOwners());
        CityConfig.FORSALE.set(city.getName(), false);

        block.setType(Material.AIR); // remove the sign
        new Chat(Lang.CITY_PURCHASED
                .replace("{city}", city.getName())
                .replace("{price}", Vault.format(price)))
                .send(player);
    }
}
