package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerLeashAnimalToFenceEvent extends PlayerEvent {
    private final Entity animal;
    private final Location fence;

    public PlayerLeashAnimalToFenceEvent(Player player, Entity animal, Location fence) {
        super(player);
        this.animal = animal;
        this.fence = fence;
    }

    public Entity getAnimal() {
        return animal;
    }

    public Entity getEntity() {
        return getAnimal();
    }

    public Location getFenceLocation() {
        return fence;
    }
}
