package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.entity.EntityBreakItemFrameEvent;
import net.pl3x.bukkit.cities.event.entity.EntityBreakPaintingEvent;
import net.pl3x.bukkit.cities.event.entity.EntityDamageEntityEvent;
import net.pl3x.bukkit.cities.event.entity.EntityUnleashAnimalFromFenceEvent;
import net.pl3x.bukkit.cities.event.entity.EntityUnleashMobFromFenceEvent;
import net.pl3x.bukkit.cities.event.player.PlayerBreakBlockEvent;
import net.pl3x.bukkit.cities.event.player.PlayerBreakItemFrameEvent;
import net.pl3x.bukkit.cities.event.player.PlayerBreakPaintingEvent;
import net.pl3x.bukkit.cities.event.player.PlayerDamageEntityEvent;
import net.pl3x.bukkit.cities.event.player.PlayerExtinguishFireEvent;
import net.pl3x.bukkit.cities.event.player.PlayerIgniteTNTEvent;
import net.pl3x.bukkit.cities.event.player.PlayerLeashAnimalToFenceEvent;
import net.pl3x.bukkit.cities.event.player.PlayerLeashMobToFenceEvent;
import net.pl3x.bukkit.cities.event.player.PlayerPlaceArmorStandEvent;
import net.pl3x.bukkit.cities.event.player.PlayerPlaceBlockEvent;
import net.pl3x.bukkit.cities.event.player.PlayerPlaceItemFrameEvent;
import net.pl3x.bukkit.cities.event.player.PlayerPlacePaintingEvent;
import net.pl3x.bukkit.cities.event.player.PlayerPlantFlowerPotEvent;
import net.pl3x.bukkit.cities.event.player.PlayerReelEntityEvent;
import net.pl3x.bukkit.cities.event.player.PlayerRightClickItemFrameEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUnleashAnimalFromFenceEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUnleashMobFromFenceEvent;
import net.pl3x.bukkit.cities.event.protection.DragonEggTeleportEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.Protection;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import net.pl3x.bukkit.cities.tasks.RefreshChunks;
import net.pl3x.bukkit.cities.tasks.UpdateBlocks;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockMultiPlaceEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.stream.Collectors;

/*
 * Protection flag to stop players from building/breaking blocks
 */
public class Build extends FlagListener {
    private final String name = "build";

    public Build(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.DENY);
    }

    /*
     * Stops players from igniting TNT
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerIgniteTNT(PlayerIgniteTNTEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getBlock().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from placing blocks
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerPlaceBlock(PlayerPlaceBlockEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getBlock().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from breaking blocks
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerBreakBlock(PlayerBreakBlockEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getBlock().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from breaking item frames
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerBreakItemFrame(PlayerBreakItemFrameEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getHanging().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from breaking paintings
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerBreakPainting(PlayerBreakPaintingEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getHanging().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops mobs from breaking item frames
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityBreakItemFrame(EntityBreakItemFrameEvent event) {
        if (Flags.isAllowed(name, null, event.getHanging().getLocation(), false, true)) {
            return;
        }
        event.setCancelled(true);
    }

    /*
     * Stops mobs from breaking paintings
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityBreakPainting(EntityBreakPaintingEvent event) {
        if (Flags.isAllowed(name, null, event.getHanging().getLocation(), false, true)) {
            return;
        }
        event.setCancelled(true);
    }

    /*
     * Stops players from damaging entities (itemframes, pictures, armorstands, etc)
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onDamagedEntity(PlayerDamageEntityEvent event) {
        Player player = event.getDamager();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getDamaged().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops entities (mobs) from damaging entities (itemframes, pictures, armorstands, etc)
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onDamagedEntity(EntityDamageEntityEvent event) {
        if (Flags.isAllowed(name, null, event.getDamaged().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }

    /*
     * Stops players from stopping fires
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerExtinguishFire(PlayerExtinguishFireEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return; // player has permission override
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getBlock().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from placing and rotating items in item frames
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onRotateItemFrame(PlayerRightClickItemFrameEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return; // player has permission override
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getItemFrame().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stop players from placing armor stands
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlaceArmorStand(PlayerPlaceArmorStandEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return; // player has permission override
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stop players from placing item frames
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlaceItemFrame(PlayerPlaceItemFrameEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return; // player has permission override
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getItemframe().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stop players from placing armor stands
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlacePainting(PlayerPlacePaintingEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return; // player has permission override
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getPainting().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stop players from reeling non-living entities (armor stands, etc)
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onReelEntity(PlayerReelEntityEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return; // player has permission override
        }
        Entity reeled = event.getReeled();
        System.out.println("1");
        if (reeled == null || (reeled instanceof LivingEntity && reeled.getType() != EntityType.ARMOR_STAND)) {
            System.out.println("2");
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), reeled.getLocation(), true, false)) {
            System.out.println("3");
            return;
        }
        System.out.println("4");
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from leashing animals onto fences
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerLeashAnimalOnFence(PlayerLeashAnimalToFenceEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getFenceLocation(), true, false)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from unleashing animals from fences
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerUnleashAnimalFromFence(PlayerUnleashAnimalFromFenceEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getFenceLocation(), true, false)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from leashing mobs onto fences
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerLeashMobOnFence(PlayerLeashMobToFenceEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getFenceLocation(), true, false)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from unleashing mobs from fences
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerUnleashMobFromFence(PlayerUnleashMobFromFenceEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getFenceLocation(), true, false)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops entities from unleashing mobs from fences
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityUnleashAnimalFromFence(EntityUnleashAnimalFromFenceEvent event) {
        if (Flags.isAllowed(name, null, event.getFenceLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }

    /*
     * Stops entities from unleashing mobs from fences
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEntityUnleashMobFromFence(EntityUnleashMobFromFenceEvent event) {
        if (Flags.isAllowed(name, null, event.getFenceLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }

    /*
     * Stop players from placing plants in flower pots
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlantFlowerPot(PlayerPlantFlowerPotEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        Location potLocation = event.getFlowerPot().getLocation();
        if (Flags.isAllowed(name, player.getUniqueId(), potLocation, true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
        new RefreshChunks(potLocation).runTaskLater(plugin, 1);
    }

    /**
     * Bukkit events already specific enough
     */

	/*
     * Stop players from pouring liquids from buckets
	 */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBucketEmpty(PlayerBucketEmptyEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getBlockClicked().getRelative(event.getBlockFace()).getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stop players from taking liquids with buckets
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBucketFill(PlayerBucketFillEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getBlockClicked().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stop dragon eggs from teleporting to/from protection
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onDragonEggTeleport(DragonEggTeleportEvent event) {
        if (Flags.isAllowed(name, null, event.getBlock().getLocation(), false, true)) {
            if (Flags.isAllowed(name, null, event.getToBlock().getLocation(), false, true)) {
                return;
            }
        }
        event.setCancelled(true);
    }

    /*
     * Stops players from changing items on armor stands
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onChangeArmorStandItem(PlayerArmorStandManipulateEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getRightClicked().getLocation(), true, false)) {
            return;
        }
        event.setCancelled(true);
    }

    /*
     * Stops trees/large mushrooms from growing beyond protection borders
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onStructureGrow(StructureGrowEvent event) {
        Protection protection = Protection.getProtection(event.getLocation());

        List<BlockState> allowed = event.getBlocks().stream()
                .filter(state -> protection == Protection.getProtection(state.getLocation()))
                .collect(Collectors.toList());

        event.getBlocks().clear();
        event.getBlocks().addAll(allowed);
    }

    /*
     * Stops bonemeal from growing grass/flowers beyond protection borders
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBonemealGrass(BlockMultiPlaceEvent event) {
        ItemStack hand = event.getItemInHand();
        if (!hand.getType().equals(Material.INK_SACK) && hand.getDurability() != 15) {
            return; // not using bonemeal
        }

        Location origin = event.getBlock().getLocation();
        Protection protection = Protection.getProtection(origin);

        List<BlockState> toRemove = event.getReplacedBlockStates().stream()
                .filter(state -> protection != Protection.getProtection(state.getLocation()))
                .collect(Collectors.toList());

        new UpdateBlocks(toRemove).runTaskLater(plugin, 1);
    }
}
