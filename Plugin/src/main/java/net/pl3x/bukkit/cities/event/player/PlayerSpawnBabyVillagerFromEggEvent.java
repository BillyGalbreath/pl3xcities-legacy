package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

@SuppressWarnings("unused")
public class PlayerSpawnBabyVillagerFromEggEvent extends PlayerEvent {
    private final Entity villager;

    public PlayerSpawnBabyVillagerFromEggEvent(Player player, Entity villager) {
        super(player);
        this.villager = villager;
    }

    public Entity getEntity() {
        return villager;
    }

    public Villager getVillager() {
        return (Villager) getEntity();
    }
}
