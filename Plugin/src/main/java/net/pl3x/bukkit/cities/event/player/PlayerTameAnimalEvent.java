package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerTameAnimalEvent extends PlayerEvent {
    private final Entity animal;

    public PlayerTameAnimalEvent(Player player, Entity animal) {
        super(player);
        this.animal = animal;
    }

    public Entity getEntity() {
        return animal;
    }

    public Entity getAnimal() {
        return getEntity();
    }

    public Player getOwner() {
        return getPlayer();
    }
}
