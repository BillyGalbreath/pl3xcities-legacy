package net.pl3x.bukkit.cities.exception;

import net.pl3x.bukkit.cities.configuration.Lang;

public class CommandException extends RuntimeException {
    public CommandException(Lang lang) {
        this(lang.toString());
    }

    public CommandException(String str) {
        super(str);
    }
}
