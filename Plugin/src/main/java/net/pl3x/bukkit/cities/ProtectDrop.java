package net.pl3x.bukkit.cities;

import org.bukkit.Location;
import org.bukkit.Material;

import java.util.UUID;

public class ProtectDrop {
    private final Material material;
    private final byte data;
    private final long tick;
    private final Location location;
    private final UUID player;

    public ProtectDrop(Material material, byte data, long tick, Location location, UUID player) {
        this.material = material;
        this.data = data;
        this.tick = tick;
        this.player = player;

        // clean up location to block location
        this.location = new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    public Material getMaterial() {
        return material;
    }

    public byte getData() {
        return data;
    }

    public long getTick() {
        return tick;
    }

    public Location getLocation() {
        return location;
    }

    public UUID getPlayerUUID() {
        return player;
    }

    @Override
    public String toString() {
        return "ProtectDrop[" +
                "material: " + material + ", " +
                "data: " + data + ", " +
                "tick: " + tick + ", " +
                "location: " + location + ", " +
                "player: " + player + "]";
    }
}
