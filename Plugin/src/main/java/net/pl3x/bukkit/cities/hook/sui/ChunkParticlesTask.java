package net.pl3x.bukkit.cities.hook.sui;

import net.pl3x.bukkit.cities.hook.SUI;
import net.pl3x.bukkit.cities.protection.ProtectionType;
import net.pl3x.bukkit.pl3xsui.api.ParticleColor;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Cuboid;
import net.pl3x.bukkit.pl3xsui.task.ParticlesTask;
import net.pl3x.bukkit.usercache.api.UserCache;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class ChunkParticlesTask extends ParticlesTask {
    private final Chunk chunk;
    private final Cuboid selection;

    public ChunkParticlesTask(Player player, Chunk chunk) {
        super(player);

        selection = new Cuboid();
        selection.setPrimary(chunk.getBlock(0, 0, 0).getLocation().toVector());
        selection.setSecondary(chunk.getBlock(15, 255, 15).getLocation().toVector());

        ProtectionType type = ProtectionType.findType(UserCache.getCachedPlayer(player.getUniqueId()), chunk);

        setFillColor(SUI.getColor(type.getFillColor()));
        setEdgeColor(SUI.getColor(type.getEdgeColor()));

        this.chunk = chunk;
    }

    public void calculatePoints() {
        if (hasAlreadyCalculated()) {
            return;
        }
        setAlreadyCalculated(true);

        Vector v1 = selection.getPrimary();
        Vector v2 = selection.getSecondary();

        int minX = Math.min(v1.getBlockX(), v2.getBlockX());
        int minY = Math.min(v1.getBlockY(), v2.getBlockY());
        int minZ = Math.min(v1.getBlockZ(), v2.getBlockZ());
        int maxX = Math.max(v1.getBlockX(), v2.getBlockX()) + 1; // add 1 to compensate for block size
        int maxY = Math.max(v1.getBlockY(), v2.getBlockY()) + 1;
        int maxZ = Math.max(v1.getBlockZ(), v2.getBlockZ()) + 1;

        for (int y = minY; y <= maxY; y++) {
            for (int x = minX; x <= maxX; x++) {
                ParticleColor color = x == minX || x == maxX || y == minY || y == maxY ? getEdgeColor() : getFillColor();
                addPacket(new Vector(x, y, minZ), color); // north
                addPacket(new Vector(x, y, maxZ), color); // south
            }
            for (int z = minZ + 1; z < maxZ; z++) {
                ParticleColor color = y == minY || y == maxY ? getEdgeColor() : getFillColor();
                addPacket(new Vector(minX, y, z), color); // west
                addPacket(new Vector(maxX, y, z), color); // east
            }
        }
    }

    @Override
    public void run() {
        try {
            if (!getPlayer().getLocation().getChunk().equals(chunk)) {
                cancel(); // player left chunk. cancels any stray tasks that dont get cancelled asynchronously
            }
        } catch (IllegalStateException e) {
            return; // player is most likely not loaded in world yet. skip this run
        }

        calculatePoints();

        super.run();
    }
}
