package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("unused")
public class PlayerRenameMobEvent extends PlayerEvent {
    private final Entity mob;
    private final ItemStack nametag;

    public PlayerRenameMobEvent(Player player, Entity mob, ItemStack nametag) {
        super(player);
        this.mob = mob;
        this.nametag = nametag;
    }

    public Entity getEntity() {
        return mob;
    }

    public Entity getMob() {
        return getEntity();
    }

    public ItemStack getNameTag() {
        return nametag;
    }
}
