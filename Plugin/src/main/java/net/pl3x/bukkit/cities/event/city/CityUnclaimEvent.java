package net.pl3x.bukkit.cities.event.city;

import net.pl3x.bukkit.cities.event.CityEvent;
import net.pl3x.bukkit.cities.protection.City;
import org.bukkit.Location;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class CityUnclaimEvent extends CityEvent {
    private final Location location;
    private final boolean deleting;

    public CityUnclaimEvent(City city, Player player, Location location, boolean deleting) {
        super(city, player);
        this.location = location;
        this.deleting = deleting;
    }

    public Location getLocation() {
        return location;
    }

    public boolean isDeletingEntireCity() {
        return deleting;
    }
}
