package net.pl3x.bukkit.cities.protection;

import net.pl3x.bukkit.cities.manager.ChunkManager;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("WeakerAccess")
public class PlotRegion {
    private final World world;
    private final int minX;
    private final int minY;
    private final int minZ;
    private final int maxX;
    private final int maxY;
    private final int maxZ;

    public PlotRegion(World world, int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {
        this.world = world;
        this.minX = minX;
        this.minY = minY;
        this.minZ = minZ;
        this.maxX = maxX;
        this.maxY = maxY;
        this.maxZ = maxZ;
    }

    public PlotRegion(Location location1, Location location2) {
        this.world = location1.getWorld();
        this.minX = Math.min(location1.getBlockX(), location2.getBlockX());
        this.minY = Math.min(location1.getBlockY(), location2.getBlockY());
        this.minZ = Math.min(location1.getBlockZ(), location2.getBlockZ());
        this.maxX = Math.max(location1.getBlockX(), location2.getBlockX());
        this.maxY = Math.max(location1.getBlockY(), location2.getBlockY());
        this.maxZ = Math.max(location1.getBlockZ(), location2.getBlockZ());
    }

    public PlotRegion(Selection selection) {
        this(selection.getPrimary(), selection.getSecondary());
    }

    public World getWorld() {
        return world;
    }

    public int getMinX() {
        return minX;
    }

    public int getMinY() {
        return minY;
    }

    public int getMinZ() {
        return minZ;
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public int getMaxZ() {
        return maxZ;
    }

    public Location getMinPoint() {
        return new Location(getWorld(), getMinX(), getMinY(), getMinZ());
    }

    public Location getMaxPoint() {
        return new Location(getWorld(), getMaxX(), getMaxY(), getMaxZ());
    }

    public boolean inCity(City city) {
        ChunkManager chunkManager = ChunkManager.getManager();
        String worldName = world.getName();
        String cityName = city.getName();
        Set<String> alreadyChecked = new HashSet<>();
        for (int x = getMinX(); x <= getMaxX(); x++) {
            for (int z = getMinZ(); z <= getMaxZ(); z++) {
                int chunkX = x >> 4;
                int chunkZ = z >> 4;
                if (alreadyChecked.contains(chunkX + "," + chunkZ)) {
                    continue;
                }
                alreadyChecked.add(chunkX + "," + chunkZ);
                CityChunk chunk = chunkManager.getChunk(worldName, chunkX, chunkZ);
                if (chunk == null) {
                    return false;
                }
                if (!cityName.equals(chunk.getCityName())) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean overlaps(PlotRegion region) {
        if (!region.getWorld().equals(getWorld())) {
            return false;
        }
        for (int x = getMinX(); x <= getMaxX(); x++) {
            for (int y = getMinY(); y <= getMaxY(); y++) {
                for (int z = getMinZ(); z <= getMaxZ(); z++) {
                    if (region.contains(x, y, z)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean contains(Location location) {
        return location.getWorld().equals(getWorld()) && contains(location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    public boolean contains(int x, int y, int z) {
        return x >= getMinX() && x <= getMaxX() && y >= getMinY() && y <= getMaxY() && z >= getMinZ() && z <= getMaxZ();
    }

    public String getBounds() {
        return "&e[&7" + getMinX() + "&e, &7" + getMinY() + "&e, &7" + getMinZ() + "&e] &d-> &e[&7" + getMaxX() + "&e, &7" + getMaxY() + "&e, &7" + getMaxZ() + "&e]";
    }

    public Set<CityChunk> getChunks() {
        Set<CityChunk> chunks = new HashSet<>();
        Set<String> alreadyAdded = new HashSet<>();
        for (int x = getMinX(); x <= getMaxX(); x++) {
            for (int z = getMinZ(); z <= getMaxZ(); z++) {
                int chunkX = x >> 4;
                int chunkZ = z >> 4;
                if (alreadyAdded.contains(chunkX + "," + chunkZ)) {
                    continue;
                }
                alreadyAdded.add(chunkX + "," + chunkZ);
                chunks.add(new CityChunk(null, world, chunkX, chunkZ));
            }
        }
        return chunks;
    }
}