package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerPlaceVehicleEvent extends PlayerEvent {
    private final Location location;

    public PlayerPlaceVehicleEvent(Player player, Location location) {
        super(player);
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }
}
