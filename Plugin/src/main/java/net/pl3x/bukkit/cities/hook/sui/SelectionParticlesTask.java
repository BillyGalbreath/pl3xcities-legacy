package net.pl3x.bukkit.cities.hook.sui;

import net.pl3x.bukkit.cities.protection.Selection;
import net.pl3x.bukkit.pl3xsui.api.ParticleColor;
import net.pl3x.bukkit.pl3xsui.task.CuboidParticlesTask;
import org.bukkit.entity.Player;

public class SelectionParticlesTask extends CuboidParticlesTask {

    public SelectionParticlesTask(Player player, Selection selection, ParticleColor fillColor, ParticleColor edgeColor) {
        super(player, new CuboidWrapper(selection));

        setFillColor(fillColor);
        setEdgeColor(edgeColor);
    }
}
