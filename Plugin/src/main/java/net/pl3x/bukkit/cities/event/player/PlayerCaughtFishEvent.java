package net.pl3x.bukkit.cities.event.player;

import org.bukkit.entity.FishHook;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

public class PlayerCaughtFishEvent extends PlayerCaughtItemEvent {
    public PlayerCaughtFishEvent(Player player, Item caught, FishHook hook) {
        super(player, caught, hook);
    }
}
