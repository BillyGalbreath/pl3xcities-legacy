package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.player.PlayerLeashMobEvent;
import net.pl3x.bukkit.cities.event.player.PlayerReelMobEvent;
import net.pl3x.bukkit.cities.event.player.PlayerRenameMobEvent;
import net.pl3x.bukkit.cities.event.player.PlayerSpawnBabyMobFromEggEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUnleashMobEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to protect mobs
 */
public class UseMobs extends FlagListener {
    private final String name = "use-mobs";

    public UseMobs(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops players from leashing mobs
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onLeashMob(PlayerLeashMobEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectmobs")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.MOB_LEASH_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from unleashing mobs
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onUnleashMob(PlayerUnleashMobEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectmobs")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.MOB_LEASH_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from changing mob names
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerRenameMob(PlayerRenameMobEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectmobs")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.MOB_RENAME_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from changing color/name, and using spawner eggs to make babies
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerSpawnBabyMobFromEgg(PlayerSpawnBabyMobFromEggEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectmobs")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.MOB_SPAWN_BABY_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from pulling mobs with fishing pole
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerReelMobEgg(PlayerReelMobEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectmobs")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getReeled().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.MOB_HURT_DENY).send(player);
        event.setCancelled(true);
    }
}
