package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;

/*
 * Protection flag to control pistons in protections
 */
public class Pistons extends FlagListener {
    private final String name = "pistons";

    public Pistons(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops pistons from pushing
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void protectFromPistonPush(BlockPistonExtendEvent event) {
        if (Flags.isAllowed(name, null, event.getBlock().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }

    /*
     * Stops pistons from pulling
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void protectFromPistonPull(BlockPistonRetractEvent event) {
        if (Flags.isAllowed(name, null, event.getBlock().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
