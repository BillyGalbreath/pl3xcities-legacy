package net.pl3x.bukkit.cities.command.cities;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.configuration.PlayerConfig;
import net.pl3x.bukkit.cities.exception.CommandException;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdCitiesSounds extends PlayerCommand {
    public CmdCitiesSounds(Pl3xCities plugin) {
        super(plugin, "sounds", Lang.CMD_DESC_CITIES_SOUNDS, "cities.command.sounds", Lang.CMD_HELP_CITIES_SOUNDS);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        List<String> results = new ArrayList<>();
        if (args.size() == 1) {
            String arg = args.peek().toLowerCase();
            if ("enabled".startsWith(arg)) {
                results.add("enabled");
            }
            if ("disabled".startsWith(arg)) {
                results.add("disabled");
            }
            if ("true".startsWith(arg)) {
                results.add("true");
            }
            if ("false".startsWith(arg)) {
                results.add("false");
            }
            if ("yes".startsWith(arg)) {
                results.add("yes");
            }
            if ("no".startsWith(arg)) {
                results.add("no");
            }
            if ("on".startsWith(arg)) {
                results.add("on");
            }
            if ("off".startsWith(arg)) {
                results.add("off");
            }
            if ("1".startsWith(arg)) {
                results.add("1");
            }
            if ("0".startsWith(arg)) {
                results.add("0");
            }
        }
        return results;
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        if (args.size() == 0) {
            new Chat(Lang.SOUNDS_CHECK
                    .replace("{enabled}", PlayerConfig.SOUNDS_ENABLED.getBoolean(player.getUniqueId()) ? "enabled" : "disabled"))
                    .send(player);
            return;
        }

        String value = args.peek().toLowerCase();
        boolean enabling;

        if (value.startsWith("enable") || value.startsWith("true") || value.startsWith("yes") || value.equals("on") || value.equals("1")) {
            enabling = true;
        } else if (value.startsWith("disable") || value.startsWith("false") || value.startsWith("no") || value.equals("off") || value.equals("0")) {
            enabling = false;
        } else {
            throw new CommandException(Lang.NOT_VALID_VALUE.replace("{value}", value));
        }

        PlayerConfig.SOUNDS_ENABLED.set(player.getUniqueId(), enabling);

        new Chat(enabling ? Lang.SOUNDS_ENABLED : Lang.SOUNDS_DISABLED).send(player);
    }
}
