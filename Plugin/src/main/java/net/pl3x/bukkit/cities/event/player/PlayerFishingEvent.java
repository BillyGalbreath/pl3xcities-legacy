package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Player;

public class PlayerFishingEvent extends PlayerEvent {
    private final FishHook hook;

    public PlayerFishingEvent(Player player, FishHook hook) {
        super(player);
        this.hook = hook;
    }

    public FishHook getHook() {
        return hook;
    }
}
