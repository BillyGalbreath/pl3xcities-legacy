package net.pl3x.bukkit.cities.event.city;

import net.pl3x.bukkit.cities.event.CityEvent;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.CityChunk;
import org.bukkit.entity.Player;

import java.util.Set;

@SuppressWarnings("unused")
public class CityBulkClaimEvent extends CityEvent {
    private final Set<CityChunk> chunks;

    public CityBulkClaimEvent(City city, Player player, Set<CityChunk> chunks) {
        super(city, player);
        this.chunks = chunks;
    }

    public Set<CityChunk> getChunksToClaim() {
        return chunks;
    }
}
