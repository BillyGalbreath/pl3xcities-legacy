package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

/*
 * Protection flag to allow players to use doors
 */
public class UseDoors extends FlagListener {
    private final String name = "use-doors";

    public UseDoors(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.DENY);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getHand() == null || event.getHand().equals(EquipmentSlot.OFF_HAND)) {
            return; // ignore offhand 2nd packet
        }
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return; // player has permission override
        }
        // Check if door interaction
        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return; // not right clicking
        }
        Block block = event.getClickedBlock();
        if (isNotDoor(block)) {
            return; // not a door
        }

        // Check UseDoor flag
        if (Flags.isAllowed(name, player.getUniqueId(), block.getLocation(), true, true)) {
            return; // door can be used
        }

        // Deny use of door
        new Chat(Lang.USE_DOOR_DENY).send(player);
        event.setCancelled(true);
    }

    private boolean isNotDoor(Block block) {
        switch (block.getType()) {
            case ACACIA_DOOR:
            case ACACIA_FENCE_GATE:
            case BIRCH_DOOR:
            case BIRCH_FENCE_GATE:
            case DARK_OAK_DOOR:
            case DARK_OAK_FENCE_GATE:
            case FENCE_GATE:
            case IRON_DOOR:
            case IRON_DOOR_BLOCK:
            case IRON_TRAPDOOR:
            case JUNGLE_DOOR:
            case JUNGLE_FENCE_GATE:
            case SPRUCE_DOOR:
            case SPRUCE_FENCE_GATE:
            case TRAP_DOOR:
            case WOOD_DOOR:
            case WOODEN_DOOR:
                return false;
            default:
                return true;
        }
    }
}
