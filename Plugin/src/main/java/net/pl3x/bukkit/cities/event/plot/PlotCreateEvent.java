package net.pl3x.bukkit.cities.event.plot;

import net.pl3x.bukkit.cities.event.CityEvent;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlotCreateEvent extends CityEvent {
    private final Plot plot;

    public PlotCreateEvent(City city, Plot plot, Player player) {
        super(city, player);
        this.plot = plot;
    }

    public Plot getPlot() {
        return plot;
    }
}
