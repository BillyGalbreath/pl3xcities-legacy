package net.pl3x.bukkit.cities.command;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.manager.SoundManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public abstract class CommandHandler implements TabExecutor {
    protected final Pl3xCities plugin;
    private final String name;
    private final String description;
    private final String help;
    private final String permission;

    CommandHandler(Pl3xCities plugin, String name, Lang description, String permission, Lang help) {
        this.plugin = plugin;
        this.name = name;
        this.description = description.toString();
        this.help = help == null ? null : help.toString();
        this.permission = permission;
    }

    protected void showHelp(CommandSender sender) {
        if (getHelp() != null) {
            for (String msg : getHelp().split("\\n")) {
                new Chat(msg).send(sender);
            }
        }
    }

    String getName() {
        return name;
    }

    String getDescription() {
        return description;
    }

    private String getHelp() {
        return help;
    }

    String getPermission() {
        return permission;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            return onCommand(sender, command, new LinkedList<>(Arrays.asList(args)));
        } catch (CommandException e) {
            if (sender instanceof Player) {
                SoundManager.playSoundToPlayer((Player) sender, Config.SOUND_COMMAND_ERROR.getString());
            }
            if (e.getMessage() == null || e.getMessage().equals("")) {
                new Chat(Lang.UNKNOWN_ERROR).send(sender);
            } else {
                new Chat(e.getMessage()).send(sender); // TODO: Check if Lang.yml is needed for this!
            }
            if (Config.SHOW_STACKTRACES.getBoolean()) {
                e.printStackTrace();
            }
            return true;
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] strings) {
        return onTabComplete(sender, command, new LinkedList<>(Arrays.asList(strings)));
    }

    List<String> onTabComplete(CommandSender sender, Command command, LinkedList<String> args) {
        return null;
    }

    boolean onCommand(CommandSender sender, Command command, LinkedList<String> args) throws CommandException {
        return false;
    }
}
