package net.pl3x.bukkit.cities.command.selection;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.hook.SUI;
import net.pl3x.bukkit.cities.protection.PlotRegion;
import net.pl3x.bukkit.cities.protection.Selection;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdSelectionShift extends PlayerCommand {
    public CmdSelectionShift(Pl3xCities plugin) {
        super(plugin, "shift", Lang.CMD_DESC_SELECTION_SHIFT, "cities.command.selection.shift", Lang.CMD_HELP_SELECTION_SHIFT);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // Show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        Selection selection = pl3xPlayer.getSelection();

        if (args.size() < 2) {
            throw new CommandException(Lang.CMD_HELP_SELECTION_SHIFT);
        }

        int amount;
        try {
            amount = Integer.valueOf(args.pop().trim());
        } catch (NumberFormatException e) {
            throw new CommandException(Lang.NOT_A_NUMBER);
        }

        if (amount <= 0) {
            throw new CommandException(Lang.ZERO_NUMBER);
        }

        if (selection.getPrimary() == null || selection.getSecondary() == null) {
            throw new CommandException(Lang.SELECTION_NOT_COMPLETE);
        }

        PlotRegion region = new PlotRegion(selection);
        Location primary = region.getMinPoint();
        Location secondary = region.getMaxPoint();

        String direction = args.pop().trim().toLowerCase();
        if ("up".startsWith(direction)) {
            int max = primary.getWorld().getMaxHeight() - 1;
            double oldValue = secondary.getY();
            double newValue = oldValue + amount;
            if (newValue > max) {
                newValue = max;
            }
            int difference = (int) Math.floor(newValue - oldValue);
            primary.setY(primary.getY() + difference);
            secondary.setY(secondary.getY() + difference);
        } else if ("down".startsWith(direction)) {
            double oldValue = primary.getY();
            double newValue = oldValue + amount;
            if (newValue < 0) {
                newValue = 0;
            }
            int difference = (int) Math.floor(oldValue - newValue);
            primary.setY(primary.getY() + difference);
            secondary.setY(secondary.getY() + difference);
        } else if ("north".startsWith(direction)) {
            primary.setZ(primary.getZ() - amount);
            secondary.setZ(secondary.getZ() - amount);
        } else if ("south".startsWith(direction)) {
            primary.setZ(primary.getZ() + amount);
            secondary.setZ(secondary.getZ() + amount);
        } else if ("east".startsWith(direction)) {
            primary.setX(primary.getX() + amount);
            secondary.setX(secondary.getX() + amount);
        } else if ("west".startsWith(direction)) {
            primary.setX(primary.getX() - amount);
            secondary.setX(secondary.getX() - amount);
        } else {
            throw new CommandException(Lang.INVALID_DIRECTION.replace("{direction}", direction));
        }

        selection.setSecondary(secondary).setPrimary(primary);

        new Chat(Lang.SELECTION_SHIFTED).send(player);

        if (Pl3xCities.hasSUI()) {
            SUI.showSelection(player);
        }
    }
}
