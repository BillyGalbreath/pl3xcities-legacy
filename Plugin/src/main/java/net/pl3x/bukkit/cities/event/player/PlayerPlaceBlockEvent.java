package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerPlaceBlockEvent extends PlayerEvent {
    private final Block block;
    private final Block against;

    public PlayerPlaceBlockEvent(Player player, Block block, Block against) {
        super(player);
        this.block = block;
        this.against = against;
    }

    public Block getBlock() {
        return block;
    }

    public Block getBlockAgainst() {
        return against;
    }
}
