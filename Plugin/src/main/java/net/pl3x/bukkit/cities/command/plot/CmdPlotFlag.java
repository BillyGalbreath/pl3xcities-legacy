package net.pl3x.bukkit.cities.command.plot;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.BaseCommand;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.configuration.PlotConfig;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.flag.Flag;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CmdPlotFlag extends BaseCommand {
    public CmdPlotFlag(Pl3xCities plugin) {
        super(plugin, "flags", Lang.CMD_DESC_PLOT_FLAGS, "cities.command.plot.flags", null);
        registerSubcommand(new ListCommand(plugin));
        registerSubcommand(new SetCommand(plugin));
    }

    public class ListCommand extends PlayerCommand {
        public ListCommand(Pl3xCities plugin) {
            super(plugin, "list", Lang.CMD_DESC_PLOT_FLAGS_LIST, "cities.command.plot.flags.list", Lang.CMD_HELP_PLOT_FLAGS_LIST);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            return new ArrayList<>();
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            // show command help
            if ("?".equals(args.peek())) {
                showHelp(player);
            }

            // get the city
            City city = CityManager.getManager().getCity(player.getLocation());
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
            }

            // get the plot
            Plot plot = city.getPlot(player.getLocation());
            if (plot == null) {
                throw new CommandException(Lang.FAILED_FIND_PLOT_LOCATION);
            }

            StringBuilder flags = new StringBuilder();
            for (Flag flag : plot.getFlags()) {
                if (flags.length() > 0) {
                    flags.append("&e, &7");
                }
                flags.append(flag.getName());
                flags.append("&d: &7");
                flags.append(flag.getState().name());
            }
            if (flags.length() == 0) {
                flags.append("&onone");
            }

            // notify player of success
            new Chat(Lang.PLOT_FLAGS_LIST).send(player);
            new Chat("&7" + flags.toString()).send(player);

            // send player sound
            SoundManager.playSoundToPlayer(player, Config.SOUND_PLOT_FLAGS_LIST.getString());
        }
    }

    public class SetCommand extends PlayerCommand {
        public SetCommand(Pl3xCities plugin) {
            super(plugin, "set", Lang.CMD_DESC_PLOT_FLAGS_SET, "cities.command.plot.flags.set", Lang.CMD_HELP_PLOT_FLAGS_SET);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            List<String> results = new ArrayList<>();
            if (args.size() == 1) {
                List<String> list = new ArrayList<>(Flags.getFlags().keySet());
                Collections.sort(list);
                results.addAll(list.stream()
                        .filter(flag -> flag.startsWith(args.peek().toLowerCase()))
                        .collect(Collectors.toList()));
            }
            return results;
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            // show command help
            if ("?".equals(args.peek())) {
                showHelp(player);
                return;
            }

            // list available flags if none specified
            if (args.size() == 0) {
                List<String> list = new ArrayList<>(Flags.getFlags().keySet());
                Collections.sort(list);
                StringBuilder flags = new StringBuilder();
                for (String flag : list) {
                    if (flags.length() > 0) {
                        flags.append("&e, &7");
                    }
                    flags.append(flag);
                }
                if (flags.length() == 0) {
                    flags.append("&onone");
                }
                new Chat(Lang.FLAGS_AVAILABLE
                        .replace("{flags}", flags.toString()))
                        .send(player);
                return;
            }

            // get the city
            City city = CityManager.getManager().getCity(player.getLocation());
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
            }

            // get the plot
            Plot plot = city.getPlot(player.getLocation());
            if (plot == null) {
                throw new CommandException(Lang.FAILED_FIND_PLOT_LOCATION);
            }

            // check ownership/permissions
            if (!plot.isOwner(player) && !PermManager.hasPerm(player, "cities.override.setflag")) {
                boolean hasOwner = !plot.getOwners().isEmpty();
                if (hasOwner || (!city.isOwner(player))) {
                    throw new CommandException(Lang.NOT_PLOT_OWNER);
                }
            }

            // check if flag is real
            String name = args.pop();
            if (!Flags.getFlags().containsKey(name)) {
                throw new CommandException(Lang.FLAG_NOT_FOUND);
            }

            // check state
            String stateStr = args.peek();
            FlagState state = null;
            if (stateStr != null && !stateStr.equalsIgnoreCase("none")) {
                try {
                    state = FlagState.valueOf(stateStr.toUpperCase());
                } catch (IllegalArgumentException e) {
                    throw new CommandException(Lang.FLAG_STATE_INVALID);
                }
            }

            // check if flag is restricted by city owner
            if (city.isPlotRestrictedFlag(name)) {
                throw new CommandException(Lang.FLAG_IS_RESTRICTED);
            }

            // set flag to plot
            plot.setFlag(name, state);
            PlotConfig.FLAGS.setFlags(plot.getId(), plot.getFlags());

            // notify player of success
            if (state == null) {
                new Chat(Lang.PLOT_FLAGS_UNSET).send(player);
            } else {
                new Chat(Lang.PLOT_FLAGS_SET).send(player);
            }

            // send player sound
            SoundManager.playSoundToPlayer(player, Config.SOUND_PLOT_FLAGS_SET.getString());
        }
    }
}
