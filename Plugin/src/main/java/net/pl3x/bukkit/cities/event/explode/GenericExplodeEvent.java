package net.pl3x.bukkit.cities.event.explode;

import net.pl3x.bukkit.cities.event.ExplodeEvent;
import org.bukkit.block.Block;

import java.util.List;

public class GenericExplodeEvent extends ExplodeEvent {
    public GenericExplodeEvent(List<Block> blocks) {
        super(null, blocks);
    }
}
