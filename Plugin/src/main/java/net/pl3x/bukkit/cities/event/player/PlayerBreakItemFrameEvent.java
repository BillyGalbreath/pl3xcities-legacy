package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.Player;
import org.bukkit.event.hanging.HangingBreakEvent.RemoveCause;

@SuppressWarnings("unused")
public class PlayerBreakItemFrameEvent extends PlayerEvent {
    private final Hanging hanging;
    private final RemoveCause cause;

    public PlayerBreakItemFrameEvent(Player player, Hanging hanging, RemoveCause cause) {
        super(player);
        this.hanging = hanging;
        this.cause = cause;
    }

    public Hanging getHanging() {
        return hanging;
    }

    public Player getRemover() {
        return getPlayer();
    }

    public RemoveCause getRemoveCause() {
        return cause;
    }
}
