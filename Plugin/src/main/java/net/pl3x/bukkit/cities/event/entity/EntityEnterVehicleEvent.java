package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Vehicle;

@SuppressWarnings("unused")
public class EntityEnterVehicleEvent extends EntityEvent {
    private final Vehicle vehicle;

    public EntityEnterVehicleEvent(Entity entity, Vehicle vehicle) {
        super(entity);
        this.vehicle = vehicle;
    }

    public Entity getVehicle() {
        return vehicle;
    }
}
