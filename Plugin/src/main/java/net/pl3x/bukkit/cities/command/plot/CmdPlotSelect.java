package net.pl3x.bukkit.cities.command.plot;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.hook.SUI;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.Selection;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdPlotSelect extends PlayerCommand {
    public CmdPlotSelect(Pl3xCities plugin) {
        super(plugin, "select", Lang.CMD_DESC_PLOT_SELECT, "cities.command.plot.select", Lang.CMD_HELP_PLOT_SELECT);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        // get plot player is standing in
        Plot plot = pl3xPlayer.getPlot();
        if (plot == null) {
            throw new CommandException(Lang.FAILED_FIND_PLOT_LOCATION);
        }

        // create selection of this plot
        Selection selection = new Selection(plot.getRegion());
        pl3xPlayer.setSelection(selection);

        Location primary = selection.getPrimary();
        Location secondary = selection.getSecondary();

        new Chat(Lang.SELECTION_PRIMARY_SET
                .replace("{x}", Integer.toString(primary.getBlockX()))
                .replace("{y}", Integer.toString(primary.getBlockY()))
                .replace("{z}", Integer.toString(primary.getBlockZ())))
                .send(player);

        new Chat(Lang.SELECTION_SECONDARY_SET
                .replace("{x}", Integer.toString(secondary.getBlockX()))
                .replace("{y}", Integer.toString(secondary.getBlockY()))
                .replace("{z}", Integer.toString(secondary.getBlockZ())))
                .send(player);

        if (Pl3xCities.hasSUI()) {
            SUI.showSelection(player);
        }

        // no need for sound. visualizer will do its own sound effect.
    }
}
