package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("unused")
public class PlayerColorAnimalEvent extends PlayerEvent {
    private final Entity animal;
    private final ItemStack color;

    public PlayerColorAnimalEvent(Player player, Entity animal, ItemStack color) {
        super(player);
        this.animal = animal;
        this.color = color;
    }

    public Entity getEntity() {
        return animal;
    }

    public Entity getAnimal() {
        return getEntity();
    }

    public ItemStack getItem() {
        return color;
    }
}
