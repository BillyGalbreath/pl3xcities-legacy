package net.pl3x.bukkit.cities.command.selection;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.hook.SUI;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdSelectionClear extends PlayerCommand {
    public CmdSelectionClear(Pl3xCities plugin) {
        super(plugin, "clear", Lang.CMD_DESC_SELECTION_CLEAR, "cities.command.selection.clear", Lang.CMD_HELP_SELECTION_CLEAR);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // Show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        pl3xPlayer.getSelection().clear();

        new Chat(Lang.SELECTION_CLEARED).send(player);

        if (Pl3xCities.hasSUI()) {
            SUI.showSelection(player);
        }
    }
}
