package net.pl3x.bukkit.cities.command.plot;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.plot.PlotDeleteEvent;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdPlotDelete extends PlayerCommand {
    public CmdPlotDelete(Pl3xCities plugin) {
        super(plugin, "delete", Lang.CMD_DESC_PLOT_DELETE, "cities.command.plot.delete", Lang.CMD_HELP_PLOT_DELETE);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // Show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        CityManager cityManager = CityManager.getManager();

        // check if player is standing in city
        Location here = player.getLocation();
        City city = cityManager.getCity(here);
        if (city == null) {
            throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
        }

        // check if player is standing inside plot
        Plot plot = city.getPlot(here);
        if (plot == null) {
            throw new CommandException(Lang.FAILED_FIND_PLOT_LOCATION);
        }

        // check ownership/permissions
        if (!city.isOwner(player) && !PermManager.hasPerm(player, "cities.override.deleteplot")) {
            throw new CommandException(Lang.NOT_CITY_OWNER);
        }
        if (!plot.isOwner(player)) {
            boolean hasOwner = !plot.getOwners().isEmpty();
            if (hasOwner) {
                throw new CommandException(Lang.NOT_PLOT_OWNER);
            }
        }

        // call the event and check for cancellation
        PlotDeleteEvent event = new PlotDeleteEvent(city, plot, player);
        Bukkit.getServer().getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            return;
        }

        // remove plot from city
        city.removePlot(plot);
        cityManager.removePlot(city, plot);

        // notify player of success
        new Chat(Lang.PLOT_DELETE_SUCCESS).send(player);

        // update visualizer if enabled
        //Visualizer.update(player, here, here);

        // send player sound
        SoundManager.playSoundToPlayer(player, Config.SOUND_PLOT_DELETE.getString());
    }
}
