package net.pl3x.bukkit.cities.hook;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.event.visualizer.ChunkParticlesEvent;
import net.pl3x.bukkit.cities.event.visualizer.PlotParticlesEvent;
import net.pl3x.bukkit.cities.event.visualizer.SelectionParticlesEvent;
import net.pl3x.bukkit.cities.hook.sui.ChunkParticlesTask;
import net.pl3x.bukkit.cities.hook.sui.PlotParticlesTask;
import net.pl3x.bukkit.cities.hook.sui.SelectionParticlesTask;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.Selection;
import net.pl3x.bukkit.pl3xsui.api.ParticleColor;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SUI {
    private static final Map<UUID, ChunkParticlesTask> chunkTasks = new HashMap<>();
    private static final Map<UUID, SelectionParticlesTask> selectionTasks = new HashMap<>();
    private static final Map<UUID, PlotParticlesTask> plotTasks = new HashMap<>();

    public static ParticleColor getColor(String name) {
        try {
            return ParticleColor.getColor(name.toUpperCase().replace("-", "_"));
        } catch (IllegalArgumentException e) {
            return ParticleColor.getColor("RED");
        }
    }

    public static void showChunk(Player player, Chunk chunk) {
        hideChunk(player);

        if (!player.getInventory().getItemInMainHand().getType().equals(Config.VISUAL_INHAND_ITEM.getMaterial()) &&
                !player.getInventory().getItemInOffHand().getType().equals(Config.VISUAL_INHAND_ITEM.getMaterial())) {
            return;
        }

        ChunkParticlesEvent chunkEvent = new ChunkParticlesEvent(player, chunk);
        Bukkit.getPluginManager().callEvent(chunkEvent);
        if (chunkEvent.isCancelled()) {
            return;
        }

        ChunkParticlesTask chunkTask = new ChunkParticlesTask(player, chunk);
        chunkTasks.put(player.getUniqueId(), chunkTask);
        chunkTask.runTaskTimerAsynchronously(Pl3xCities.getPlugin(Pl3xCities.class), 5,
                net.pl3x.bukkit.pl3xsui.configuration.Config.PARTICLE_DELAY.getInt());
    }

    public static void hideChunk(Player player) {
        ChunkParticlesTask chunkTask = chunkTasks.remove(player.getUniqueId());
        if (chunkTask != null) {
            chunkTask.cancel();
        }
    }

    public static void showSelection(Player player) {
        hideSelection(player);

        Selection selection = Pl3xPlayer.getPlayer(player).getSelection();

        SelectionParticlesEvent selectionEvent = new SelectionParticlesEvent(player, selection);
        Bukkit.getPluginManager().callEvent(selectionEvent);
        if (selectionEvent.isCancelled()) {
            return;
        }

        SelectionParticlesTask selectionTask = new SelectionParticlesTask(player, selection,
                getColor(Config.VISUAL_SELECTION_FILL_COLOR.getString()),
                getColor(Config.VISUAL_SELECTION_EDGE_COLOR.getString()));
        selectionTasks.put(player.getUniqueId(), selectionTask);
        selectionTask.runTaskTimerAsynchronously(Pl3xCities.getPlugin(Pl3xCities.class), 5,
                net.pl3x.bukkit.pl3xsui.configuration.Config.PARTICLE_DELAY.getInt());
    }

    public static void hideSelection(Player player) {
        SelectionParticlesTask selectionTask = selectionTasks.remove(player.getUniqueId());
        if (selectionTask != null) {
            selectionTask.cancel();
        }
    }

    public static void showPlot(Player player) {
        hidePlot(player);

        Location loc = player.getLocation();
        Plot plot = CityManager.getManager().getCity(loc).getPlot(loc);

        PlotParticlesEvent plotEvent = new PlotParticlesEvent(player, plot);
        Bukkit.getPluginManager().callEvent(plotEvent);
        if (plotEvent.isCancelled()) {
            return;
        }

        PlotParticlesTask plotTask = new PlotParticlesTask(player, plot,
                getColor(Config.VISUAL_PLOT_FILL_COLOR.getString()),
                getColor(Config.VISUAL_PLOT_EDGE_COLOR.getString()));
        plotTasks.put(player.getUniqueId(), plotTask);
        plotTask.runTaskTimerAsynchronously(Pl3xCities.getPlugin(Pl3xCities.class), 5,
                net.pl3x.bukkit.pl3xsui.configuration.Config.PARTICLE_DELAY.getInt());
    }

    public static void hidePlot(Player player) {
        PlotParticlesTask plotTask = plotTasks.remove(player.getUniqueId());
        if (plotTask != null) {
            plotTask.cancel();
        }
    }
}
