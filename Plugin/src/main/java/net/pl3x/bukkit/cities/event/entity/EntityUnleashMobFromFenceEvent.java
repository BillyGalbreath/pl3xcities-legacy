package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.Location;
import org.bukkit.entity.Entity;

@SuppressWarnings("unused")
public class EntityUnleashMobFromFenceEvent extends EntityEvent {
    private final Entity mob;
    private final Location fence;

    public EntityUnleashMobFromFenceEvent(Entity entity, Entity mob, Location fence) {
        super(entity);
        this.mob = mob;
        this.fence = fence;
    }

    public Entity getMob() {
        return mob;
    }

    public Entity getRemoved() {
        return getMob();
    }

    public Entity getRemover() {
        return getEntity();
    }

    public Location getFenceLocation() {
        return fence;
    }
}
