package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerPlacePaintingEvent extends PlayerEvent {
    private final Hanging painting;
    private final BlockFace blockface;
    private final Block against;

    public PlayerPlacePaintingEvent(Player player, Hanging painting, Block against, BlockFace blockface) {
        super(player);
        this.painting = painting;
        this.against = against;
        this.blockface = blockface;
    }

    public Hanging getPainting() {
        return painting;
    }

    public Block getBlockAgainst() {
        return against;
    }

    public BlockFace getBlockface() {
        return blockface;
    }
}
