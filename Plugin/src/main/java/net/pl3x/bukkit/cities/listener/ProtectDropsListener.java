package net.pl3x.bukkit.cities.listener;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.ProtectDrop;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.event.player.PlayerDestroyVehicleEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUnleashAnimalFromFenceEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUnleashMobFromFenceEvent;
import net.pl3x.bukkit.cities.manager.ProtectDropManager;
import net.pl3x.bukkit.cities.protection.Protection;
import net.pl3x.bukkit.cities.tasks.ForgetProtectDrops;
import net.pl3x.bukkit.cities.tasks.UnprotectDrop;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * These listeners are for detecting blockbreaks (etc) and protecting the spawned items.
 * This can be refined once Bukkit adds getDrops() and setDrops() to certain events.
 */
@SuppressWarnings("deprecation")
public class ProtectDropsListener implements Listener {
    private final Pl3xCities plugin;

    public ProtectDropsListener(Pl3xCities plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (Config.PROTECT_DROPS_TIME.getInt() <= 0) {
            return; // feature disabled
        }

        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }

        // get event info
        Block block = event.getBlock();
        Location location = block.getLocation();
        Player player = event.getPlayer();

        // check if this is happening in a protection
        if (Protection.getProtection(location) == null) {
            return; // not happening in a protection. ignore.
        }

        // get more info
        UUID uuid = player.getUniqueId();
        long tick = player.getWorld().getFullTime();

        // get possible drops from API
        Collection<ItemStack> stacks = block.getDrops();
        stacks.addAll(block.getDrops(player.getItemInHand()));

        // set of possible drops to protect
        Collection<ProtectDrop> drops = stacks.stream()
                .map(stack -> new ProtectDrop(
                        stack.getType(),
                        stack.getData().getData(),
                        tick,
                        location,
                        uuid))
                .collect(Collectors.toCollection(HashSet::new));

        int data = -1;
        Material type = null;

        if (block.getState() instanceof Banner) {
            data = ((Banner) block.getState()).getBaseColor().getDyeData(); // ink_sack color (not wool color)
            type = Material.BANNER; // we dont care about standing or wall
        }

        // add the block itself (for silk touch)
        drops.add(new ProtectDrop(
                type != null ? type : block.getType(),
                data > -1 ? (byte) data : block.getData(),
                tick,
                location,
                uuid));

        // store in protect drop manager
        ProtectDropManager.getManager().addDrops(drops);

        // remove from protect drop manager after 2 ticks
        new ForgetProtectDrops(drops).runTaskLater(plugin, 2);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onHangingBreakByPlayer(HangingBreakByEntityEvent event) {
        if (Config.PROTECT_DROPS_TIME.getInt() <= 0) {
            return; // feature disabled
        }

        if (!(event.getRemover() instanceof Player)) {
            return; // not broke by player
        }

        Hanging hanging = event.getEntity();

        if (Config.isWorldDisabled(hanging.getWorld())) {
            return; // cities not enabled in this world
        }

        // get event info
        Location location = hanging.getLocation();

        // check if this is happening in a protection
        if (Protection.getProtection(location) == null) {
            return; // not happening in a protection. ignore.
        }

        // get more info
        UUID uuid = event.getRemover().getUniqueId();
        long tick = hanging.getWorld().getFullTime();


        // get drop
        Collection<ItemStack> stacks = new HashSet<>();
        switch (hanging.getType()) {
            case PAINTING:
                stacks.add(new ItemStack(Material.PAINTING, 1));
                break;
            case ITEM_FRAME:
                stacks.add(new ItemStack(Material.ITEM_FRAME, 1));
                break;
            case LEASH_HITCH:
                stacks.add(new ItemStack(Material.LEASH, 1));
                break;
            //return; // DOES NOT WORK! see PlayerUnleashEntityEvent
            default:
                return; // should not happen
        }

        // set of possible drops to protect
        Collection<ProtectDrop> drops = stacks.stream()
                .map(stack -> new ProtectDrop(
                        stack.getType(),
                        stack.getData().getData(),
                        tick,
                        location,
                        uuid))
                .collect(Collectors.toCollection(HashSet::new));

        // store in protect drop manager
        ProtectDropManager.getManager().addDrops(drops);

        // remove from protect drop manager after 2 ticks
        new ForgetProtectDrops(drops).runTaskLater(plugin, 2);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerUnleashAnimalFromFence(PlayerUnleashAnimalFromFenceEvent event) {
        if (Config.PROTECT_DROPS_TIME.getInt() <= 0) {
            return; // feature disabled
        }

        Entity animal = event.getAnimal();

        if (Config.isWorldDisabled(animal.getWorld())) {
            return; // cities not enabled in this world
        }

        // get event info
        Location location = animal.getLocation();

        // check if this is happening in a protection
        if (Protection.getProtection(location) == null) {
            return; // not happening in a protection. ignore.
        }

        // get more info
        UUID uuid = event.getPlayer().getUniqueId();
        long tick = animal.getWorld().getFullTime();

        // get drop
        Collection<ItemStack> stacks = new HashSet<>();
        stacks.add(new ItemStack(Material.LEASH, 1));

        // set of possible drops to protect
        Collection<ProtectDrop> drops = stacks.stream()
                .map(stack -> new ProtectDrop(
                        stack.getType(),
                        stack.getData().getData(),
                        tick,
                        location,
                        uuid))
                .collect(Collectors.toCollection(HashSet::new));

        // store in protect drop manager
        ProtectDropManager.getManager().addDrops(drops);

        // remove from protect drop manager after 2 ticks
        new ForgetProtectDrops(drops).runTaskLater(plugin, 2);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerUnleashMobFromFence(PlayerUnleashMobFromFenceEvent event) {
        if (Config.PROTECT_DROPS_TIME.getInt() <= 0) {
            return; // feature disabled
        }

        Entity mob = event.getMob();

        // get drop
        Collection<ItemStack> stacks = new HashSet<>();
        stacks.add(new ItemStack(Material.LEASH, 1));

        // get event info
        long tick = mob.getWorld().getFullTime();
        Location location = mob.getLocation();
        UUID uuid = event.getPlayer().getUniqueId();

        // set of possible drops to protect
        Collection<ProtectDrop> drops = stacks.stream()
                .map(stack -> new ProtectDrop(
                        stack.getType(),
                        stack.getData().getData(),
                        tick,
                        location,
                        uuid))
                .collect(Collectors.toCollection(HashSet::new));

        // store in protect drop manager
        ProtectDropManager.getManager().addDrops(drops);

        // remove from protect drop manager after 2 ticks
        new ForgetProtectDrops(drops).runTaskLater(plugin, 2);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerBreakArmorStand(EntityDamageByEntityEvent event) {
        if (Config.PROTECT_DROPS_TIME.getInt() <= 0) {
            return; // feature disabled
        }

        if (!(event.getDamager() instanceof Player)) {
            return; // not broke by a player
        }

        Entity armorstand = event.getEntity();

        if (!armorstand.getType().equals(EntityType.ARMOR_STAND)) {
            return; // not an armor stand
        }

        if (Config.isWorldDisabled(armorstand.getWorld())) {
            return; // cities not enabled in this world
        }

        // get event info
        Location location = armorstand.getLocation();

        // check if this is happening in a protection
        if (Protection.getProtection(location) == null) {
            return; // not happening in a protection. ignore.
        }

        // get more info
        long tick = armorstand.getWorld().getFullTime();
        UUID uuid = event.getDamager().getUniqueId();

        // get drop
        Collection<ItemStack> stacks = new HashSet<>();
        stacks.add(new ItemStack(Material.ARMOR_STAND, 1));

        // set of possible drops to protect
        Collection<ProtectDrop> drops = stacks.stream()
                .map(stack -> new ProtectDrop(
                        stack.getType(),
                        stack.getData().getData(),
                        tick,
                        location,
                        uuid))
                .collect(Collectors.toCollection(HashSet::new));

        // store in protect drop manager
        ProtectDropManager.getManager().addDrops(drops);

        // remove from protect drop manager after 2 ticks
        new ForgetProtectDrops(drops).runTaskLater(plugin, 2);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDestroyVehicle(PlayerDestroyVehicleEvent event) {
        if (Config.PROTECT_DROPS_TIME.getInt() <= 0) {
            return; // feature disabled
        }

        Entity vehicle = event.getVehicle();

        if (Config.isWorldDisabled(vehicle.getWorld())) {
            return; // cities not enabled in this world
        }

        // get info
        Location location = vehicle.getLocation();

        // check if this is happening in a protection
        if (Protection.getProtection(location) == null) {
            return; // not happening in a protection. ignore.
        }

        // get more info
        long tick = vehicle.getWorld().getFullTime();
        UUID uuid = event.getPlayer().getUniqueId();

        // get drop
        Collection<ItemStack> stacks = new HashSet<>();

        // Add the pieces broken from vehicle
        switch (vehicle.getType()) {
            case BOAT:
                stacks.add(new ItemStack(Material.BOAT, 1));
                // Note: do not protect sticks and planks. Only protect if player breaks purposefully
                break;
            case MINECART_CHEST:
                stacks.add(new ItemStack(Material.MINECART, 1));
                stacks.add(new ItemStack(Material.CHEST, 1));
                break;
            case MINECART_COMMAND:
                stacks.add(new ItemStack(Material.MINECART, 1));
                stacks.add(new ItemStack(Material.COMMAND, 1));
                break;
            case MINECART_FURNACE:
                stacks.add(new ItemStack(Material.MINECART, 1));
                stacks.add(new ItemStack(Material.FURNACE, 1));
                stacks.add(new ItemStack(Material.BURNING_FURNACE, 1));
                break;
            case MINECART_HOPPER:
                stacks.add(new ItemStack(Material.MINECART, 1));
                stacks.add(new ItemStack(Material.HOPPER, 1));
                break;
            case MINECART_MOB_SPAWNER:
                stacks.add(new ItemStack(Material.MINECART, 1));
                stacks.add(new ItemStack(Material.MOB_SPAWNER, 1));
                break;
            case MINECART_TNT:
                stacks.add(new ItemStack(Material.MINECART, 1));
                stacks.add(new ItemStack(Material.TNT, 1));
                break;
            case MINECART:
                stacks.add(new ItemStack(Material.MINECART, 1));
                break;
            default:
        }

        // set of possible drops to protect
        Collection<ProtectDrop> drops = stacks.stream()
                .map(stack -> new ProtectDrop(
                        stack.getType(),
                        stack.getData().getData(),
                        tick,
                        location,
                        uuid))
                .collect(Collectors.toCollection(HashSet::new));

        // store in protect drop manager
        ProtectDropManager.getManager().addDrops(drops);

        // remove from protect drop manager after 2 ticks
        new ForgetProtectDrops(drops).runTaskLater(plugin, 2);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemSpawn(ItemSpawnEvent event) {
        if (Config.PROTECT_DROPS_TIME.getInt() <= 0) {
            return; // feature disabled
        }

        Item item = event.getEntity();

        if (Config.isWorldDisabled(item.getWorld())) {
            return; // cities not enabled in this world
        }

        ProtectDrop drop = ProtectDropManager.getManager().getProtectDrop(item);

        if (drop == null) {
            return; // this item is not protected
        }

        item.setMetadata("owner", new FixedMetadataValue(plugin, drop.getPlayerUUID().toString()));

        new UnprotectDrop(item).runTaskLater(plugin, Config.PROTECT_DROPS_TIME.getInt() * 20);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        if (Config.PROTECT_DROPS_TIME.getInt() <= 0) {
            return; // feature disabled
        }

        if (Config.isWorldDisabled(event.getPlayer().getWorld())) {
            return; // cities not enabled in this world
        }

        Item item = event.getItem();

        if (!item.hasMetadata("owner")) {
            return; // no metadata
        }

        Player player = event.getPlayer();

        List<MetadataValue> meta = item.getMetadata("owner");

        if (meta.size() == 0) {
            return; // no metadata
        }

        MetadataValue metaValue = meta.get(0);

        if (metaValue == null) {
            return; // no metadata
        }

        String value = metaValue.asString();

        if (value == null || value.isEmpty() || value.equalsIgnoreCase("none")) {
            return; // owner removed
        }

        if (value.equals(player.getUniqueId().toString())) {
            return; // this is owner. allow to pick up
        }

        event.setCancelled(true);
    }
}
