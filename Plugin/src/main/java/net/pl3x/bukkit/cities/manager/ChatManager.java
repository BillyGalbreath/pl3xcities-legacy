package net.pl3x.bukkit.cities.manager;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatManager {

    public static BaseComponent[] convert(String string) {
        return TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', string));
    }

    public static void add(TextComponent component, BaseComponent[] components) {
        for (BaseComponent add : components) {
            component.addExtra(add);
        }
    }

    public static TextComponent tooltip(TextComponent component, BaseComponent[] tooltip) {
        component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, tooltip));
        return component;
    }

    public static void sendMessage(Player player, BaseComponent message) {
        if (message == null || ChatColor.stripColor(TextComponent.toLegacyText(message)).equals("")) {
            return;
        }
        player.spigot().sendMessage(message);
    }
}
