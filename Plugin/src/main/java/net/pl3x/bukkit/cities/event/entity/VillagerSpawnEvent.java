package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Villager;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

@SuppressWarnings("unused")
public class VillagerSpawnEvent extends EntityEvent {
    private final Location location;
    private final SpawnReason reason;

    public VillagerSpawnEvent(Entity entity, Location location, SpawnReason reason) {
        super(entity);
        this.location = location;
        this.reason = reason;
    }

    public Location getLocation() {
        return location;
    }

    public SpawnReason getSpawnReason() {
        return reason;
    }

    public Villager getVillager() {
        return (Villager) getEntity();
    }
}
