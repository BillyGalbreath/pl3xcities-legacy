package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerSplashPlayerEvent extends PlayerEvent {
    private final Player splashed;

    public PlayerSplashPlayerEvent(Player splashed, Player thrower) {
        super(thrower);
        this.splashed = splashed;
    }

    public Player getSplashed() {
        return splashed;
    }

    public Player getThrower() {
        return getPlayer();
    }
}
