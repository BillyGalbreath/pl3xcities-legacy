package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.entity.EntityDamageMobEvent;
import net.pl3x.bukkit.cities.event.player.PlayerDamageMobEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to protect mobs
 */
public class HurtMobs extends FlagListener {
    private final String name = "hurt-mobs";

    public HurtMobs(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops players from hurting mobs
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onDamageMob(PlayerDamageMobEvent event) {
        Player player = event.getDamager();
        if (PermManager.hasPerm(player, "cities.override.protectmobs")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.MOB_HURT_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops entities (mobs) from hurting mobs
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onDamageMob(EntityDamageMobEvent event) {
        if (Flags.isAllowed(name, null, event.getEntity().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
