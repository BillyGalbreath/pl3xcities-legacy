package net.pl3x.bukkit.cities.command.city;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.BaseCommand;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.CityConfig;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.configuration.PlotConfig;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CmdCityPlotRestricted extends BaseCommand {
    public CmdCityPlotRestricted(Pl3xCities plugin) {
        super(plugin, "plot-restricted", Lang.CMD_DESC_CITY_PLOTRESTRICTED, "cities.command.city.plotrestricted", null);
        registerSubcommand(new ListCommand(plugin));
        registerSubcommand(new AddCommand(plugin));
        registerSubcommand(new RemoveCommand(plugin));
    }

    public static class ListCommand extends PlayerCommand {
        public ListCommand(Pl3xCities plugin) {
            super(plugin, "list", Lang.CMD_DESC_CITY_PLOTRESTRICTED_LIST, "cities.command.city.plotrestricted.list", Lang.CMD_HELP_CITY_PLOTRESTRICTED_LIST);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            return new ArrayList<>();
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            // show command help
            if ("?".equals(args.peek())) {
                showHelp(player);
                return;
            }

            // get the city
            City city = CityManager.getManager().getCity(player.getLocation());
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
            }

            String restrictedList = String.join("&e, &7", city.getPlotRestrictedFlags());
            if (restrictedList.trim().equals("")) {
                restrictedList = "&onone";
            }

            // notify player of success
            new Chat(Lang.CITY_PLOTRESTRICTED_LIST).send(player);
            new Chat("&7" + restrictedList).send(player);

            // send player sound
            SoundManager.playSoundToPlayer(player, Config.SOUND_CITY_PLOTRESTRICTED_LIST.getString());
        }
    }

    public static class AddCommand extends PlayerCommand {
        public AddCommand(Pl3xCities plugin) {
            super(plugin, "add", Lang.CMD_DESC_CITY_PLOTRESTRICTED_ADD, "cities.command.city.plotrestricted.add", Lang.CMD_HELP_CITY_PLOTRESTRICTED_ADD);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            List<String> results = new ArrayList<>();
            City city = Pl3xPlayer.getPlayer(player).getCity();
            if (args.size() == 1 && city != null) {
                List<String> list = new ArrayList<>(Flags.getFlags().keySet());
                Collections.sort(list);
                results.addAll(list.stream()
                        .filter(flag -> flag.startsWith(args.peek().toLowerCase()))
                        .collect(Collectors.toList()));
                results.removeAll(city.getPlotRestrictedFlags().stream()
                        .filter(flag -> flag.startsWith(args.peek().toLowerCase()))
                        .collect(Collectors.toList()));
            }
            return results;
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws
                CommandException {
            // show command help
            if ("?".equals(args.peek())) {
                showHelp(player);
                return;
            }

            if (args.size() < 1) {
                throw new CommandException(Lang.FLAG_NOT_SPECIFIED);
            }

            // get the city
            City city = Pl3xPlayer.getPlayer(player).getCity();
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
            }

            // check ownership/permissions
            if (!city.isOwner(player) && !PermManager.hasPerm(player, "cities.override.addplotrestricted")) {
                throw new CommandException(Lang.NOT_CITY_OWNER);
            }

            // check if flag is real
            String flag = args.pop().toLowerCase();
            if (!Flags.getFlags().containsKey(flag)) {
                throw new CommandException(Lang.FLAG_NOT_FOUND);
            }

            // check if flag is already restricted
            if (city.isPlotRestrictedFlag(flag)) {
                throw new CommandException(Lang.FLAG_ALREADY_RESTRICTED);
            }

            // add flag to restrictions
            city.addPlotRestrictedFlag(flag);
            CityConfig.PLOTRESTRICTED.set(city.getName(), city.getPlotRestrictedFlags());

            // scan city plots and remove the restricted flag
            for (Plot plot : city.getPlots()) {
                plot.removeFlag(flag);
                PlotConfig.FLAGS.setFlags(plot.getId(), plot.getFlags());
            }

            // notify player of success
            new Chat(Lang.CITY_PLOTRESTRICTED_ADDED).send(player);

            // send player sound
            SoundManager.playSoundToPlayer(player, Config.SOUND_CITY_PLOTRESTRICTED_ADDED.getString());
        }
    }

    public static class RemoveCommand extends PlayerCommand {
        public RemoveCommand(Pl3xCities plugin) {
            super(plugin, "remove", Lang.CMD_DESC_CITY_PLOTRESTRICTED_REMOVE, "cities.command.city.plotrestricted.remove", Lang.CMD_HELP_CITY_PLOTRESTRICTED_REMOVE);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            List<String> results = new ArrayList<>();
            City city = Pl3xPlayer.getPlayer(player).getCity();
            if (args.size() == 1 && city != null) {
                results.addAll(city.getPlotRestrictedFlags().stream()
                        .filter(flag -> flag.startsWith(args.peek().toLowerCase()))
                        .collect(Collectors.toList()));
            }
            return results;
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            // show command help
            if ("?".equals(args.peek())) {
                showHelp(player);
                return;
            }

            if (args.size() < 1) {
                throw new CommandException(Lang.FLAG_NOT_SPECIFIED);
            }

            // get the city
            City city = Pl3xPlayer.getPlayer(player).getCity();
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
            }

            // check ownership/permissions
            if (!city.isOwner(player) && !PermManager.hasPerm(player, "cities.override.removeplotrestricted")) {
                throw new CommandException(Lang.NOT_CITY_OWNER);
            }

            // check if flag is real
            String flag = args.pop().toLowerCase();
            if (!Flags.getFlags().containsKey(flag)) {
                throw new CommandException(Lang.FLAG_NOT_FOUND);
            }

            // check if flag is restricted
            if (!city.isPlotRestrictedFlag(flag)) {
                throw new CommandException(Lang.FLAG_NOT_RESTRICTED);
            }

            // remove flag from restrictions
            city.removePlotRestrictedFlag(flag);
            CityConfig.PLOTRESTRICTED.set(city.getName(), city.getPlotRestrictedFlags());

            // notify player of success
            new Chat(Lang.CITY_PLOTRESTRICTED_REMOVED).send(player);

            // send player sound
            SoundManager.playSoundToPlayer(player, Config.SOUND_CITY_PLOTRESTRICTED_REMOVED.getString());
        }
    }
}
