package net.pl3x.bukkit.cities.command.plot;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.BaseCommand;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.configuration.PlotConfig;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.usercache.api.CachedPlayer;
import net.pl3x.bukkit.usercache.api.UserCache;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CmdPlotMembers extends BaseCommand {
    public CmdPlotMembers(Pl3xCities plugin) {
        super(plugin, "members", Lang.CMD_DESC_PLOT_MEMBERS, "cities.command.plot.members", null);
        registerSubcommand(new ListCommand(plugin));
        registerSubcommand(new AddCommand(plugin));
        registerSubcommand(new RemoveCommand(plugin));
    }

    public class ListCommand extends PlayerCommand {
        public ListCommand(Pl3xCities plugin) {
            super(plugin, "list", Lang.CMD_DESC_PLOT_MEMBERS_LIST, "cities.command.plot.members.list", Lang.CMD_HELP_PLOT_MEMBERS_LIST);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            return new ArrayList<>();
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            // show command help
            if ("?".equals(args.peek())) {
                showHelp(player);
                return;
            }

            // get the city
            City city = CityManager.getManager().getCity(player.getLocation());
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
            }

            // get the plot
            Plot plot = city.getPlot(player.getLocation());
            if (plot == null) {
                throw new CommandException(Lang.FAILED_FIND_PLOT_LOCATION);
            }

            String membersList = String.join("&e, &7", plot.getMembers().values());
            if (membersList.trim().equals("")) {
                membersList = "&onone";
            }

            // notify player of success
            new Chat(Lang.PLOT_MEMBERS_LIST).send(player);
            new Chat("&7" + membersList).send(player);

            // send player sound
            SoundManager.playSoundToPlayer(player, Config.SOUND_PLOT_MEMBER_LIST.getString());
        }
    }

    public class AddCommand extends PlayerCommand {
        public AddCommand(Pl3xCities plugin) {
            super(plugin, "add", Lang.CMD_DESC_PLOT_MEMBERS_ADD, "cities.command.plot.members.add", Lang.CMD_HELP_PLOT_MEMBERS_ADD);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            List<String> results = new ArrayList<>();
            if (args.size() == 1) {
                results.addAll(Bukkit.getOnlinePlayers().stream()
                        .filter(online -> online.getName().toLowerCase().startsWith(args.peek().toLowerCase()))
                        .map(Player::getName).collect(Collectors.toList()));
            }
            return results;
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            // show command help
            if ("?".equals(args.peek())) {
                showHelp(player);
                return;
            }

            if (args.size() < 1) {
                throw new CommandException(Lang.NO_NAME_SPECIFIED);
            }

            // get the city
            City city = CityManager.getManager().getCity(player.getLocation());
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
            }

            // get the plot
            Plot plot = city.getPlot(player.getLocation());
            if (plot == null) {
                throw new CommandException(Lang.FAILED_FIND_PLOT_LOCATION);
            }

            // check ownership/permissions
            if (!plot.isOwner(player) && !PermManager.hasPerm(player, "cities.override.addmember")) {
                boolean hasOwner = !plot.getOwners().isEmpty();
                if (hasOwner || (!city.isOwner(player))) {
                    throw new CommandException(Lang.NOT_PLOT_OWNER);
                }
            }

            // get the target player to add as member
            String name = args.pop();
            CachedPlayer target = UserCache.getCachedPlayer(name);
            if (target == null) {
                throw new CommandException(Lang.USER_NOT_FOUND);
            }

            // check if target player is already a member
            if (plot.isMember(target)) {
                throw new CommandException(Lang.USER_ALREADY_MEMBER);
            }

            // add target player as member
            plot.addMember(target);
            PlotConfig.MEMBERS.setPlayers(plot.getId(), plot.getMembers());

            // notify player of success
            new Chat(Lang.PLOT_MEMBER_ADDED).send(player);

            // send player sound
            SoundManager.playSoundToPlayer(player, Config.SOUND_PLOT_MEMBER_ADDED.getString());
        }
    }

    public class RemoveCommand extends PlayerCommand {
        public RemoveCommand(Pl3xCities plugin) {
            super(plugin, "remove", Lang.CMD_DESC_PLOT_MEMBERS_REMOVE, "cities.command.plot.members.remove", Lang.CMD_HELP_PLOT_MEMBERS_REMOVE);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            List<String> results = new ArrayList<>();
            if (args.size() == 1) {
                City city = CityManager.getManager().getCity(player.getLocation());
                if (city == null) {
                    return results;
                }
                Plot plot = city.getPlot(player.getLocation());
                if (plot == null) {
                    return results;
                }
                results.addAll(plot.getMembers().values().stream()
                        .filter(member -> member.toLowerCase().startsWith(args.peek().toLowerCase()))
                        .collect(Collectors.toList()));
            }
            return results;
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            // show command help
            if ("?".equals(args.peek())) {
                showHelp(player);
                return;
            }

            if (args.size() < 1) {
                throw new CommandException(Lang.NO_NAME_SPECIFIED);
            }

            // get the city
            City city = CityManager.getManager().getCity(player.getLocation());
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
            }

            // get the plot
            Plot plot = city.getPlot(player.getLocation());
            if (plot == null) {
                throw new CommandException(Lang.FAILED_FIND_PLOT_LOCATION);
            }

            // check ownership/permissions
            if (!plot.isOwner(player) && !PermManager.hasPerm(player, "cities.override.removemember")) {
                boolean hasOwner = !plot.getOwners().isEmpty();
                if (hasOwner || (!city.isOwner(player))) {
                    throw new CommandException(Lang.NOT_PLOT_OWNER);
                }
            }

            // get the target player to add as member
            String name = args.pop();
            if (player.getName().equalsIgnoreCase(name)) {
                throw new CommandException(Lang.CANNOT_REMOVE_SELF_FROM_MEMBERS);
            }
            CachedPlayer target = UserCache.getCachedPlayer(name);
            if (target == null) {
                throw new CommandException(Lang.USER_NOT_FOUND);
            }

            // check if target player is already a member
            if (!plot.isMember(target)) {
                throw new CommandException(Lang.USER_NOT_MEMBER);
            }

            // remove target player from members
            plot.removeMember(target);
            PlotConfig.MEMBERS.setPlayers(plot.getId(), plot.getMembers());

            // notify player of success
            new Chat(Lang.PLOT_MEMBER_REMOVED).send(player);

            // send player sound
            SoundManager.playSoundToPlayer(player, Config.SOUND_PLOT_MEMBER_REMOVED.getString());
        }
    }
}
