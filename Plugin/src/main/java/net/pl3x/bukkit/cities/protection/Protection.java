package net.pl3x.bukkit.cities.protection;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.pl3x.bukkit.cities.manager.ChatManager;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.protection.flag.Flag;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import net.pl3x.bukkit.usercache.api.CachedPlayer;
import net.pl3x.bukkit.usercache.api.UserCache;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

public class Protection {
    private double price = 0D;
    private boolean forsale = false;
    private Location spawn = null;
    private Map<UUID, String> owners = new HashMap<>();
    private Map<UUID, String> members = new HashMap<>();
    private Set<Flag> flags = new HashSet<>();

    public static Protection getProtection(Location location) {
        City city = CityManager.getManager().getCity(location);
        if (city == null) {
            return null; // not in a city
        }
        Plot plot = city.getPlot(location);
        if (plot != null) {
            return plot;
        }
        return city;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isForSale() {
        return forsale;
    }

    public void setForSale(boolean forsale) {
        this.forsale = forsale;
    }

    public Location getSpawn() {
        return spawn;
    }

    public String getSpawnCoords() {
        return "&e[&7" + spawn.getBlockX() + "&e, &7" + spawn.getBlockY() + "&e, &7" + spawn.getBlockZ() + "&e]";
    }

    public void setSpawn(Location spawn) {
        this.spawn = spawn;
    }

    public Map<UUID, String> getOwners() {
        return owners;
    }

    public void setOwners(Map<UUID, String> owners) {
        this.owners = owners;
    }

    public boolean isOwner(Player player) {
        return isOwner(player.getUniqueId());
    }

    public boolean isOwner(CachedPlayer player) {
        return isOwner(player.getUniqueId());
    }

    public boolean isOwner(UUID uuid) {
        return getOwners().containsKey(uuid);
    }

    public void addOwner(Player player) {
        addOwner(UserCache.getCachedPlayer(player.getUniqueId()));
    }

    public void addOwner(CachedPlayer player) {
        Map<UUID, String> owners = getOwners();
        owners.put(player.getUniqueId(), player.getName());
        setOwners(owners);
    }

    public void removeOwner(CachedPlayer player) {
        Map<UUID, String> owners = getOwners();
        owners.remove(player.getUniqueId());
        setOwners(owners);
    }

    public Map<UUID, String> getMembers() {
        return members;
    }

    public void setMembers(Map<UUID, String> members) {
        this.members = members;
    }

    public boolean isMember(CachedPlayer player) {
        return isMember(player.getUniqueId());
    }

    public boolean isMember(Player player) {
        return isMember(player.getUniqueId());
    }

    public boolean isMember(UUID uuid) {
        return getMembers().containsKey(uuid);
    }

    public void addMember(Player player) {
        addMember(UserCache.getCachedPlayer(player.getUniqueId()));
    }

    public void addMember(CachedPlayer player) {
        Map<UUID, String> members = getMembers();
        members.put(player.getUniqueId(), player.getName());
        setMembers(members);
    }

    public void removeMember(CachedPlayer player) {
        Map<UUID, String> members = getMembers();
        members.remove(player.getUniqueId());
        setMembers(members);
    }

    public Flag getFlag(String name) {
        if (name == null) {
            return null;
        }
        name = name.toLowerCase();
        for (Flag flag : getFlags()) {
            if (flag.getName().equals(name)) {
                return flag;
            }
        }
        return null;
    }

    public Set<Flag> getFlags() {
        return flags;
    }

    public void setFlags(Set<Flag> flags) {
        this.flags = flags;
    }

    public void setFlag(String name, FlagState state) {
        removeFlag(name);
        if (state == null) {
            return;
        }
        flags.add(Flags.newFlag(name, state));
    }

    public void removeFlag(String name) {
        Flag flag = getFlag(name);
        if (flag == null) {
            return;
        }
        flags.remove(flag);
    }

    public TextComponent getUsersTextComponent(String title, Map<UUID, String> users) {
        TextComponent component = new TextComponent("   ");
        if (users.isEmpty()) {
            ChatManager.add(component, ChatManager.convert(title));
            ChatManager.add(component, ChatManager.convert(" &7&onone"));
            return component;
        }
        ChatManager.add(component, ChatManager.convert(title));
        boolean first = true;
        for (Entry<UUID, String> member : users.entrySet()) {
            if (!first) {
                ChatManager.add(component, ChatManager.convert("&e, "));
            }
            first = false;

            TextComponent name = new TextComponent(member.getValue());
            name.setColor(ChatColor.GRAY);

            BaseComponent[] hover = ChatManager.convert("&dUUID: &7" + member.getKey().toString());
            component.addExtra(ChatManager.tooltip(name, hover));
        }
        return component;
    }
}
