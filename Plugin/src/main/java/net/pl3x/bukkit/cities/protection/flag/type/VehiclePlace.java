package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.player.PlayerPlaceVehicleEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to stop vehicles from being placed
 */
public class VehiclePlace extends FlagListener {
    private final String name = "vehicle-place";

    public VehiclePlace(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops placement of vehicles
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerPlaceVehicle(PlayerPlaceVehicleEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.vehicleplace")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.VEHICLE_PLACE_DENY).send(player);
        event.setCancelled(true);
    }
}
