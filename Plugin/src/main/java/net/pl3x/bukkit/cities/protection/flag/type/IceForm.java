package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.protection.IceFormEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to stop ice from forming
 */
public class IceForm extends FlagListener {
    private final String name = "ice-form";

    public IceForm(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops ice from forming
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onIceForm(IceFormEvent event) {
        if (Flags.isAllowed(name, null, event.getBlock().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
