package net.pl3x.bukkit.cities.command;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.plot.CmdPlotCreate;
import net.pl3x.bukkit.cities.command.plot.CmdPlotDelete;
import net.pl3x.bukkit.cities.command.plot.CmdPlotFlag;
import net.pl3x.bukkit.cities.command.plot.CmdPlotInfo;
import net.pl3x.bukkit.cities.command.plot.CmdPlotMembers;
import net.pl3x.bukkit.cities.command.plot.CmdPlotOwners;
import net.pl3x.bukkit.cities.command.plot.CmdPlotSelect;
import net.pl3x.bukkit.cities.command.plot.CmdPlotShow;
import net.pl3x.bukkit.cities.configuration.Lang;

public class CmdPlot extends BaseCommand {
    public CmdPlot(Pl3xCities plugin) {
        super(plugin, "plot", Lang.CMD_DESC_PLOT, "cities.command.plot", null);
        registerSubcommand(new CmdPlotCreate(plugin));
        registerSubcommand(new CmdPlotDelete(plugin));
        registerSubcommand(new CmdPlotFlag(plugin));
        registerSubcommand(new CmdPlotInfo(plugin));
        registerSubcommand(new CmdPlotMembers(plugin));
        registerSubcommand(new CmdPlotOwners(plugin));
        registerSubcommand(new CmdPlotSelect(plugin));
        if (Pl3xCities.hasSUI()) {
            registerSubcommand(new CmdPlotShow(plugin));
        }
    }
}
