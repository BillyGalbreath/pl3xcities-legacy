package net.pl3x.bukkit.cities.manager;

import net.pl3x.bukkit.cities.configuration.CityConfig;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.CityChunk;
import org.bukkit.Chunk;
import org.bukkit.Location;

import java.util.HashMap;

public class ChunkManager {
    private static ChunkManager manager;
    private final HashMap<String, CityChunk> chunksByLocation = new HashMap<>(); // world,x,z , chunk

    public static ChunkManager getManager() {
        if (manager == null) {
            manager = new ChunkManager();
        }
        return manager;
    }

    public static void unload() {
        getManager().chunksByLocation.clear();
        manager = null;
    }

    public void claimCityChunk(City city, Location location) {
        CityChunk chunk = new CityChunk(city.getName(), location);
        addChunk(chunk);
        city.addChunk(chunk);
        CityConfig.CHUNKS.setChunks(city.getName(), city.getChunks());
    }

    public void unclaimCityChunk(City city, Location location) {
        CityChunk chunk = getChunk(location);
        removeChunk(chunk);
        city.removeChunk(chunk);
        CityConfig.CHUNKS.setChunks(city.getName(), city.getChunks());
    }

    public CityChunk getChunk(String worldName, int x, int z) {
        return chunksByLocation.get(getChunkLocation(worldName, x, z));
    }

    public CityChunk getChunk(Location location) {
        return getChunk(location.getChunk());
    }

    public CityChunk getChunk(Chunk chunk) {
        return getChunk(chunk.getWorld().getName(), chunk.getX(), chunk.getZ());
    }

    public void addChunk(CityChunk chunk) {
        chunksByLocation.put(getChunkLocation(chunk), chunk);
    }

    public void removeChunk(CityChunk chunk) {
        chunksByLocation.remove(getChunkLocation(chunk));
    }

    public boolean notTouchingCity(Location location, String cityName) {
        CityManager cityManager = CityManager.getManager();
        City east = cityManager.getCity(location.clone().add(16, 0, 0));
        if (east != null && east.getName().equalsIgnoreCase(cityName)) {
            return false;
        }
        City west = cityManager.getCity(location.clone().add(-16, 0, 0));
        if (west != null && west.getName().equalsIgnoreCase(cityName)) {
            return false;
        }
        City north = cityManager.getCity(location.clone().add(0, 0, -16));
        if (north != null && north.getName().equalsIgnoreCase(cityName)) {
            return false;
        }
        City south = cityManager.getCity(location.clone().add(0, 0, 16));
        return !(south != null && south.getName().equalsIgnoreCase(cityName));
    }

    public boolean isChunkTouchingOtherCity(Location location, String cityName) {
        CityManager cityManager = CityManager.getManager();
        City east = cityManager.getCity(location.clone().add(16, 0, 0));
        if (east != null && !east.getName().equalsIgnoreCase(cityName)) {
            return true;
        }
        City west = cityManager.getCity(location.clone().add(-16, 0, 0));
        if (west != null && !west.getName().equalsIgnoreCase(cityName)) {
            return true;
        }
        City north = cityManager.getCity(location.clone().add(0, 0, -16));
        if (north != null && !north.getName().equalsIgnoreCase(cityName)) {
            return true;
        }
        City south = cityManager.getCity(location.clone().add(0, 0, 16));
        return south != null && !south.getName().equalsIgnoreCase(cityName);
    }

    public String getChunkLocation(CityChunk chunk) {
        return getChunkLocation(chunk.getWorld().getName(), chunk.getX(), chunk.getZ());
    }

    private String getChunkLocation(String worldName, int x, int z) {
        return worldName + "," + x + "," + z;
    }
}
