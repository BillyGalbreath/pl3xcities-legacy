package net.pl3x.bukkit.cities;

import net.pl3x.bukkit.cities.api.EntityTag;
import net.pl3x.bukkit.cities.command.CmdCities;
import net.pl3x.bukkit.cities.command.CmdCity;
import net.pl3x.bukkit.cities.command.CmdPlot;
import net.pl3x.bukkit.cities.command.CmdSelection;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.configuration.SpawnLimiterConfig;
import net.pl3x.bukkit.cities.hook.Vault;
import net.pl3x.bukkit.cities.listener.BukkitListener;
import net.pl3x.bukkit.cities.listener.ChunkParticlesListener;
import net.pl3x.bukkit.cities.listener.ForSaleListener;
import net.pl3x.bukkit.cities.listener.PlayerListener;
import net.pl3x.bukkit.cities.listener.ProtectDropsListener;
import net.pl3x.bukkit.cities.listener.ProtectionListener;
import net.pl3x.bukkit.cities.listener.SpawnLimiterListener;
import net.pl3x.bukkit.cities.manager.ChunkManager;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xCities extends JavaPlugin {
    private static Boolean hasSUI;
    private EntityTag entityTagHandler;

    public static boolean hasSUI() {
        if (hasSUI == null) {
            hasSUI = Bukkit.getPluginManager().isPluginEnabled("Pl3xSUI");
        }
        return hasSUI;
    }

    public void onEnable() {
        // copy default config.yml from jar if none found in plugin directory
        saveDefaultConfig();

        // Load config files into memory
        Lang.reload();
        SpawnLimiterConfig.reload();

        // check for server and plugin dependencies
        if (!hasAllDependencies()) {
            return;
        }

        // load protection flags and register flag listeners
        Flags.load(this);

        // register base listeners
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new BukkitListener(this), this);
        pm.registerEvents(new ForSaleListener(), this);
        pm.registerEvents(new PlayerListener(this), this);
        pm.registerEvents(new ProtectDropsListener(this), this);
        pm.registerEvents(new ProtectionListener(this), this);
        pm.registerEvents(new SpawnLimiterListener(), this);
        if (hasSUI()) {
            pm.registerEvents(new ChunkParticlesListener(this), this);
        }

        // register commands
        getCommand("cities").setExecutor(new CmdCities(this));
        getCommand("city").setExecutor(new CmdCity(this));
        getCommand("plot").setExecutor(new CmdPlot(this));
        getCommand("selection").setExecutor(new CmdSelection(this));

        // load all cities and plots from disk
        CityManager.getManager().loadCitiesFromDisk();

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    public void onDisable() {
        // free up that ram
        CityManager.unload();
        ChunkManager.unload();
        Pl3xPlayer.unloadAll();

        Logger.info(getName() + " disabled.");
    }

    public void reload() {
        //CityManager.unload();
        //ChunkManager.unload();
        Pl3xPlayer.unloadAll();

        reloadConfig();
        Lang.reload(true);
        SpawnLimiterConfig.reload(true);

        //CityManager.getManager().loadCitiesFromDisk();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.DARK_RED + getName() + " is disabled. Please check console logs for more information.");
        return true;
    }

    private boolean hasAllDependencies() {
        // check for server dependency
        try {
            Class.forName("org.spigotmc.SpigotConfig");
        } catch (ClassNotFoundException e) {
            dependencyError("&4#             This plugin is only compatible with Spigot servers!             #");
            return false;
        }

        if (setupNMSHandlersFailed()) {
            dependencyError("&4# Unable to instantiate NMS handlers. Make sure server version is supported.  #");
            return false;
        }

        // check for plugin dependencies
        if (!Bukkit.getPluginManager().isPluginEnabled("ProtocolLib")) {
            dependencyError("&4#          This plugin requires ProtocolLib plugin to be installed!           #");
            return false;
        }

        if (!Bukkit.getPluginManager().isPluginEnabled("Vault")) {
            dependencyError("&4#             This plugin requires Vault plugin to be installed!              #");
            return false;
        }

        if (!Bukkit.getPluginManager().isPluginEnabled("UserCache")) {
            dependencyError("&4#           This plugin requires UserCache plugin to be installed!            #");
            return false;
        }

        if (!Vault.setupEconomy()) {
            dependencyError("&4#   This plugin requires a Vault compatible Economy plugin to be installed!   #");
            return false;
        }

        if (!Vault.setupPermissions()) {
            dependencyError("&4# This plugin requires a Vault compatible Permissions plugin to be installed! #");
            return false;
        }

        return true;
    }

    private void dependencyError(String error) {
        Logger.error("&4###############################################################################");
        Logger.error(error);
        Logger.error("&4#                                                                             #");
        Logger.error("&4#     To prevent server crashes and other undesired behavior this plugin      #");
        Logger.error("&4#       is disabling itself from running. Please remove the plugin jar        #");
        Logger.error("&4#       from your plugins directory to free up the registered commands.       #");
        Logger.error("&4###############################################################################");
    }

    private boolean setupNMSHandlersFailed() {
        String packageName = this.getServer().getClass().getPackage().getName();
        String version = packageName.substring(packageName.lastIndexOf('.') + 1);

        try {
            final Class<?> clazz = Class.forName("net.pl3x.bukkit.cities.nms." + version + ".EntityTagHandler");
            if (EntityTag.class.isAssignableFrom(clazz)) {
                entityTagHandler = (EntityTag) clazz.getConstructor().newInstance();
            }
        } catch (Exception e) {
            if (Config.DEBUG_MODE.getBoolean()) {
                e.printStackTrace();
            }
            return true;
        }

        return false;
    }

    public EntityTag getEntityTagHandler() {
        return entityTagHandler;
    }
}
