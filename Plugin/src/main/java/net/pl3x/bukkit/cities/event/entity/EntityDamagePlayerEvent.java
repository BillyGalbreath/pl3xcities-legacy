package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class EntityDamagePlayerEvent extends EntityEvent {
    private final Player damaged;

    public EntityDamagePlayerEvent(Player damaged, Entity damager) {
        super(damager);
        this.damaged = damaged;
    }

    public Player getDamaged() {
        return damaged;
    }

    public Entity getDamager() {
        return getEntity();
    }
}
