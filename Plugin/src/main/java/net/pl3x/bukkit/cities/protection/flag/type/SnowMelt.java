package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.protection.SnowMeltEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to stop snow from melting
 */
public class SnowMelt extends FlagListener {
    private final String name = "snow-melt";

    public SnowMelt(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops snow from melting
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onSnowMelt(SnowMeltEvent event) {
        if (Flags.isAllowed(name, null, event.getBlock().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
