package net.pl3x.bukkit.cities.configuration;

import net.pl3x.bukkit.cities.Logger;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.protection.PlotRegion;
import net.pl3x.bukkit.cities.protection.flag.Flag;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

@SuppressWarnings("WeakerAccess")
public enum PlotConfig {
    PRICE(0.0),
    FORSALE(false),
    SPAWN(null),
    FLAGS(null),
    OWNERS(null),
    MEMBERS(null),
    REGION(null);

    private final Pl3xCities plugin;
    private final Object def;

    PlotConfig(Object def) {
        this.plugin = Pl3xCities.getPlugin(Pl3xCities.class);
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public void set(String plotId, Object value) {
        saveConfig(plotId, value);
    }

    public List<Map<?, ?>> getMapList(String plotId) {
        return getConfig(plotId).getMapList(getKey());
    }

    public Map<String, ?> getMap(String plotId) {
        ConfigurationSection cs = getConfig(plotId).getConfigurationSection(getKey());
        if (cs == null) {
            return null;
        }
        return cs.getValues(false);
    }

    public boolean getBoolean(String plotId) {
        return getConfig(plotId).getBoolean(getKey(), (Boolean) def);
    }

    public double getDouble(String plotId) {
        return getConfig(plotId).getDouble(getKey(), (Double) def);
    }

    public Map<UUID, String> getPlayers(String plotId) {
        Map<UUID, String> map = new HashMap<>();
        for (Map<?, ?> player : getMapList(plotId)) {
            map.put(UUID.fromString((String) player.get("uuid")), (String) player.get("name"));
        }
        return map;
    }

    public void setPlayers(String plotId, Map<UUID, String> players) {
        List<HashMap<String, String>> maps = new ArrayList<>();
        for (Entry<UUID, String> entry : players.entrySet()) {
            HashMap<String, String> player = new HashMap<>();
            player.put("uuid", entry.getKey().toString());
            player.put("name", entry.getValue());
            maps.add(player);
        }
        set(plotId, maps);
    }

    public Set<Flag> getFlags(String plotId) {
        Set<Flag> set = new HashSet<>();
        Map<String, ?> map = getMap(plotId);
        if (map == null) {
            return set;
        }
        for (Entry<String, ?> entry : map.entrySet()) {
            try {
                set.add(Flags.newFlag(entry.getKey(), FlagState.valueOf(((String) entry.getValue()).toUpperCase())));
            } catch (Exception e) {
                Logger.error("Error loading flag &7" + entry.getKey() + " &4from config for plot &7" + plotId + "&4. Ignoring...");
                if (Config.SHOW_STACKTRACES.getBoolean()) {
                    e.printStackTrace();
                }
            }
        }
        return set;
    }

    public void setFlags(String plotId, Set<Flag> flags) {
        HashMap<String, String> map = new HashMap<>();
        for (Flag flag : flags) {
            map.put(flag.getName(), flag.getState().name());
        }
        set(plotId, map);
    }

    public Location getLocation(String plotId) {
        ConfigurationSection config = getConfig(plotId).getConfigurationSection(getKey());
        if (config == null) {
            return null;
        }
        String worldName = config.getString("world");
        World world = Bukkit.getWorld(worldName);
        if (world == null) {
            return null;
        }
        double x = config.getDouble("x");
        double y = config.getDouble("y");
        double z = config.getDouble("z");
        float pitch = (float) config.getDouble("pitch");
        float yaw = (float) config.getDouble("yaw");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public void setLocation(String plotId, Location location) {
        ConfigurationSection config = getConfig(plotId).createSection(getKey());
        config.set("world", location.getWorld().getName());
        config.set("x", location.getX());
        config.set("y", location.getY());
        config.set("z", location.getZ());
        config.set("pitch", location.getPitch());
        config.set("yaw", location.getYaw());
        set(plotId, config);
    }

    public PlotRegion getRegion(String plotId) {
        ConfigurationSection config = getConfig(plotId).getConfigurationSection(getKey());
        if (config == null) {
            return null;
        }
        String worldName = config.getString("world");
        World world = Bukkit.getWorld(worldName);
        if (world == null) {
            return null;
        }
        int minx = config.getInt("minx");
        int miny = config.getInt("miny");
        int minz = config.getInt("minz");
        int maxx = config.getInt("maxx");
        int maxy = config.getInt("maxy");
        int maxz = config.getInt("maxz");
        return new PlotRegion(world, minx, miny, minz, maxx, maxy, maxz);
    }

    public void setRegion(String plotId, PlotRegion region) {
        ConfigurationSection config = getConfig(plotId).createSection(getKey());
        config.set("world", region.getWorld().getName());
        config.set("minx", region.getMinX());
        config.set("miny", region.getMinY());
        config.set("minz", region.getMinZ());
        config.set("maxx", region.getMaxX());
        config.set("maxy", region.getMaxY());
        config.set("maxz", region.getMaxZ());
        set(plotId, config);
    }

    private YamlConfiguration getConfig(String plotId) {
        YamlConfiguration config = new YamlConfiguration();
        try {
            config.load(new File(plugin.getDataFolder(), "cities" + File.separator + "plots" + File.separator + plotId + ".yml"));
        } catch (Exception e) {
            Logger.error("Error loading plot config file!");
            e.printStackTrace();
        }
        return config;
    }

    private void saveConfig(String plotId, Object value) {
        YamlConfiguration config = new YamlConfiguration();
        try {
            File dir = new File(plugin.getDataFolder(), "cities" + File.separator + "plots");
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    Logger.error("Unable to create plot data directory: " + dir.getAbsolutePath());
                    return;
                }
            }
            File file = new File(dir, plotId + ".yml");
            if (!file.exists()) {
                if (!file.createNewFile()) {
                    Logger.error("Unable to create plot data file: " + file.getAbsolutePath());
                    return;
                }
            }
            config.load(file);
            config.set(getKey(), value);
            config.save(file);
        } catch (Exception e) {
            Logger.error("Unable to save plot data: " + plotId + ": " + getKey() + ": " + value);
            Logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void deleteFile(String plotId) {
        try {
            File dir = new File(Pl3xCities.getPlugin(Pl3xCities.class).getDataFolder(), "cities" + File.separator + "plots");
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    Logger.error("Unable to create plot data directory: " + dir.getAbsolutePath());
                    return;
                }
            }
            File file = new File(dir, plotId + ".yml");
            if (!file.exists()) {
                return;
            }
            if (!file.delete()) {
                Logger.error("Unable to delete plot data file: " + file.getAbsolutePath());
            }
        } catch (Exception e) {
            Logger.error("Unable to delete plot data file: " + plotId + ".yml");
            Logger.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
