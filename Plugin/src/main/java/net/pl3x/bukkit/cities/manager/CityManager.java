package net.pl3x.bukkit.cities.manager;

import net.pl3x.bukkit.cities.Logger;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.CityConfig;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.PlotConfig;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.CityChunk;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.PlotRegion;
import net.pl3x.bukkit.cities.tasks.LoadCities;
import net.pl3x.bukkit.usercache.api.CachedPlayer;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;
import java.util.regex.Pattern;

public class CityManager {
    private static CityManager manager;
    private final HashMap<String, City> citiesByName = new HashMap<>();

    public static CityManager getManager() {
        if (manager == null) {
            manager = new CityManager();
        }
        return manager;
    }

    public static void unload() {
        getManager().citiesByName.clear();
        manager = null;
    }

    public void loadCitiesFromDisk() {
        File dir = new File(Pl3xCities.getPlugin(Pl3xCities.class).getDataFolder() + File.separator + "cities");
        File[] citiesList = dir.listFiles((dir1, name) -> {
            return name.endsWith(".yml");
        });
        if (citiesList == null) {
            return;
        }
        File[] plotsList = new File(dir, "plots").listFiles((dir1, name) -> {
            return name.endsWith(".yml");
        });
        LoadCities loadCitiesTask = new LoadCities(citiesList.length, plotsList.length);
        Thread loadThread = new Thread(loadCitiesTask);
        loadThread.start();
        for (File file : citiesList) {
            Logger.debug("&6[CONFIG] Loading city from file: " + file.getName());
            String cityName = file.getName().split(".yml")[0];
            City city = new City(cityName);
            Location spawn = CityConfig.SPAWN.getLocation(cityName);
            if (spawn == null) {
                Logger.warn("World not loaded for city named " + cityName);
                loadCitiesTask.decreaseCitiesTotal();
                continue;
            }
            addCity(city);
            city.setPrice(CityConfig.PRICE.getDouble(cityName));
            Logger.debug("  price: " + city.getPrice());
            city.setForSale(CityConfig.FORSALE.getBoolean(cityName));
            Logger.debug("  forsale: " + city.isForSale());
            city.setSpawn(spawn);
            Logger.debug("  spawn: " + city.getSpawn());
            city.setPlotLimit(CityConfig.PLOTLIMIT.getInteger(cityName));
            Logger.debug("  plot-limit: " + city.getPlotLimit());
            city.setPlotRestrictedFlags(CityConfig.PLOTRESTRICTED.getStringList(cityName));
            Logger.debug("  plot-restricted: " + city.getPlotRestrictedFlags());
            city.setFlags(CityConfig.FLAGS.getFlags(cityName));
            Logger.debug("  flags: " + city.getFlags().toString());
            city.setOwners(CityConfig.OWNERS.getPlayers(cityName));
            Logger.debug("  owners: " + city.getOwners().toString());
            city.setMembers(CityConfig.MEMBERS.getPlayers(cityName));
            Logger.debug("  members: " + city.getMembers().toString());
            city.setChunks(CityConfig.CHUNKS.getChunks(cityName));
            Logger.debug("  chunks:");
            for (CityChunk chunk : city.getChunks()) {
                ChunkManager chunkManager = ChunkManager.getManager();
                Logger.debug("  - " + chunkManager.getChunkLocation(chunk));
                chunkManager.addChunk(chunk);
                loadCitiesTask.increaseChunksTotal();
            }
            Logger.debug("  plots:");
            for (String plotId : CityConfig.PLOTS.getStringList(cityName)) {
                PlotRegion region = PlotConfig.REGION.getRegion(plotId);
                if (region == null) {
                    loadCitiesTask.decreasePlotsTotal();
                    continue;
                }
                Logger.debug("  - " + plotId);
                Plot plot = new Plot(plotId, region);
                plot.setPrice(PlotConfig.PRICE.getDouble(plotId));
                plot.setForSale(PlotConfig.FORSALE.getBoolean(plotId));
                plot.setSpawn(PlotConfig.SPAWN.getLocation(plotId));
                plot.setOwners(PlotConfig.OWNERS.getPlayers(plotId));
                plot.setMembers(PlotConfig.MEMBERS.getPlayers(plotId));
                plot.setFlags(PlotConfig.FLAGS.getFlags(plotId));
                city.addPlot(plot);
                loadCitiesTask.incrementPlots();
            }
            loadCitiesTask.incrementCities();
        }
        loadCitiesTask.finished();
    }

    public Collection<City> getCities() {
        return citiesByName.values();
    }

    public City getCity(String name) {
        return citiesByName.get(name.toLowerCase());
    }

    public City getCity(Chunk chunk) {
        CityChunk cityChunk = ChunkManager.getManager().getChunk(chunk);
        if (cityChunk == null) {
            return null;
        }
        return getCity(cityChunk.getCityName());
    }

    public City getCity(Location location) {
        return getCity(location.getChunk());
    }

    private void addCity(City city) {
        citiesByName.put(city.getName().toLowerCase(), city);
    }

    private void removeCity(City city) {
        citiesByName.remove(city.getName().toLowerCase());
    }

    public City createCity(String cityName, Player player) {
        City city = new City(cityName);

        city.addOwner(player);
        city.setSpawn(player.getLocation());

        addCity(city);

        CityChunk chunk = new CityChunk(city.getName(), city.getSpawn());
        ChunkManager.getManager().addChunk(chunk);
        city.addChunk(chunk);

        // TODO add server default flags

        saveCity(city);

        Logger.debug(player.getName() + " created city (" + cityName + ")");

        return city;
    }

    public void deleteCity(City city) {
        // delete plot data
        for (Plot plot : city.getPlots()) {
            PlotConfig.deleteFile(plot.getId());
        }
        city.unloadPlots();

        // remove heavy data from memory
        for (CityChunk chunk : city.getChunks()) {
            ChunkManager.getManager().removeChunk(chunk);
        }

        city.setChunks(null);
        city.setFlags(null);
        city.setMembers(null);
        city.setOwners(null);
        city.setSpawn(null);

        // remove city from manager
        removeCity(city);

        // delete city config file
        CityConfig.deleteFile(city.getName());
    }

    private void saveCity(City city) {
        String cityName = city.getName();
        CityConfig.PRICE.set(cityName, city.getPrice());
        CityConfig.FORSALE.set(cityName, city.isForSale());
        CityConfig.SPAWN.setLocation(cityName, city.getSpawn());
        CityConfig.PLOTLIMIT.set(cityName, city.getPlotLimit());
        CityConfig.PLOTRESTRICTED.set(cityName, city.getPlotRestrictedFlags());
        CityConfig.OWNERS.setPlayers(cityName, city.getOwners());
        CityConfig.MEMBERS.setPlayers(cityName, city.getMembers());
        CityConfig.CHUNKS.setChunks(cityName, city.getChunks());
        CityConfig.FLAGS.setFlags(cityName, city.getFlags());
        CityConfig.PLOTS.setPlots(cityName, city.getPlots());
        for (Plot plot : city.getPlots()) {
            String plotId = plot.getId();
            PlotConfig.PRICE.set(plotId, plot.getPrice());
            PlotConfig.FORSALE.set(plotId, plot.isForSale());
            PlotConfig.SPAWN.setLocation(plotId, plot.getSpawn());
            PlotConfig.OWNERS.setPlayers(plotId, plot.getOwners());
            PlotConfig.MEMBERS.setPlayers(plotId, plot.getMembers());
            PlotConfig.REGION.setRegion(plotId, plot.getRegion());
            PlotConfig.FLAGS.setFlags(plotId, plot.getFlags());
        }
    }

    public void addPlot(City city, Plot plot) {
        // add plot id to city config
        CityConfig.PLOTS.setPlots(city.getName(), city.getPlots());

        // save plot config data
        String plotId = plot.getId();
        PlotConfig.PRICE.set(plotId, plot.getPrice());
        PlotConfig.FORSALE.set(plotId, plot.isForSale());
        PlotConfig.SPAWN.setLocation(plotId, plot.getSpawn());
        PlotConfig.OWNERS.setPlayers(plotId, plot.getOwners());
        PlotConfig.MEMBERS.setPlayers(plotId, plot.getMembers());
        PlotConfig.REGION.setRegion(plotId, plot.getRegion());
        PlotConfig.FLAGS.setFlags(plotId, plot.getFlags());
    }

    public void removePlot(City city, Plot plot) {
        // remove plot id from city config
        CityConfig.PLOTS.setPlots(city.getName(), city.getPlots());

        // remove plot config data
        PlotConfig.deleteFile(plot.getId());
    }

    public boolean isInvalidName(String name) {
        return Pattern.compile("[^a-zA-Z0-9_-]").matcher(name).find();
    }

    public boolean isBlacklistedName(String name) {
        for (String blacklisted : Config.CITY_NAME_BLACKLIST.getStringList()) {
            if (name.toLowerCase().contains(blacklisted.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public Integer numberOfOwnedCities(Player player) {
        return numberOfOwnedCities(player.getUniqueId());
    }

    public Integer numberOfOwnedCities(CachedPlayer player) {
        return numberOfOwnedCities(player.getUniqueId());
    }

    public Integer numberOfOwnedCities(UUID uuid) {
        int count = 0;
        for (City chkCity : getCities()) {
            if (chkCity.isOwner(uuid)) {
                count++;
            }
        }
        return count;
    }

    public Integer getLimit(OfflinePlayer player) {
        int limit = 0;
        int maxlimit = 1024;
        if (PermManager.hasPerm(player, "cities.city.limit.*")) {
            return -1;
        }
        for (int i = 0; i < maxlimit; i++) {
            if (PermManager.hasPerm(player, "cities.city.limit." + i) && i > limit) {
                limit = i;
            }
        }
        return limit;
    }
}
