package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.player.PlayerColorAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerEnterVehicleEvent;
import net.pl3x.bukkit.cities.event.player.PlayerFeedAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerLeashAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerMilkCowEvent;
import net.pl3x.bukkit.cities.event.player.PlayerReelAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerRenameAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerShearAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerSoupMushroomCowEvent;
import net.pl3x.bukkit.cities.event.player.PlayerSpawnBabyAnimalFromEggEvent;
import net.pl3x.bukkit.cities.event.player.PlayerTameAnimalEvent;
import net.pl3x.bukkit.cities.event.player.PlayerUnleashAnimalEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import net.pl3x.bukkit.cities.tasks.ResetBreeding;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to protect animals
 */
public class UseAnimals extends FlagListener {
    private final String name = "use-animals";

    public UseAnimals(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops players from leashing animals
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onLeashAnimal(PlayerLeashAnimalEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectanimals")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.ANIMAL_LEASH_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from unleashing animals
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onUnleashAnimal(PlayerUnleashAnimalEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectanimals")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.ANIMAL_LEASH_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from shearing animals
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onShearAnimal(PlayerShearAnimalEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectanimals")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.ANIMAL_SHEAR_DENY).send(player);
        event.setCancelled(true);

        // TODO find a way to resend mooshroom cow packets here (client shows them explode)
    }

    /*
     * Stops players from getting on horses
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onRideHorse(PlayerEnterVehicleEvent event) {
        if (!(event.getVehicle() instanceof Horse)) {
            return;
        }
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectanimals")) {
            return;
        }
        if (isOwner(event.getVehicle(), player)) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getVehicle().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.ANIMAL_RIDE_HORSE_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stop players from taming animals
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onTameAnimal(PlayerTameAnimalEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectanimals")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.ANIMAL_TAME_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from changing animal colors (wolf collar/sheep wool)
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerColorAnimal(PlayerColorAnimalEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectanimals")) {
            return;
        }
        if (isOwner(event.getEntity(), player)) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.ANIMAL_CHANGE_COLOR_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from changing animal names
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerRenameAnimal(PlayerRenameAnimalEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectanimals")) {
            return;
        }
        if (isOwner(event.getEntity(), player)) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.ANIMAL_RENAME_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from changing color/name, and using spawner eggs to make babies
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerSpawnBabyAnimalFromEgg(PlayerSpawnBabyAnimalFromEggEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectanimals")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.ANIMAL_SPAWN_BABY_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stop players from feeding/breeding animals
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerFeedAnimal(PlayerFeedAnimalEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectanimals")) {
            return;
        }
        Entity animal = event.getEntity();
        if (UseAnimals.isOwner(animal, player)) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), animal.getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.ANIMAL_FEED_DENY).send(player);
        event.setCancelled(true);

        // Workaround to stop client's hearts animation
        Ageable ageable = (Ageable) animal;
        ageable.setBreed(false); // cancel heart animation
        new ResetBreeding(ageable).runTaskLater(plugin, 20); // let animal breed again
    }

    /*
     * Stop players from milking cows
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerMilkCow(PlayerMilkCowEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectanimals")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.ANIMAL_MILK_COW_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stop players from souping mushroom cows
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerSoupMushroomCow(PlayerSoupMushroomCowEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectanimals")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.ANIMAL_SOUP_COW_DENY).send(player);
        event.setCancelled(true);
    }

    /*
    * Stops players from pulling mobs with fishing pole
    */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerReelAnimalEgg(PlayerReelAnimalEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.protectmobs")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getReeled().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.MOB_HURT_DENY).send(player);
        event.setCancelled(true);
    }

    // TODO
    // Stop players from luring animals
    // (not currently possible with the Bukkit API)
    // https://hub.spigotmc.org/jira/browse/SPIGOT-1373

    /**
     * Check if player is the owner of a tamed entity
     *
     * @param entity Tameable entity to check
     * @param player Possible owner of tamed entity
     * @return True is player is owner of tamed entity. False if player is not the owner, the entity is not tamed, or the entity is not tameable
     */
    public static boolean isOwner(Entity entity, Player player) {
        if (!(entity instanceof Tameable)) {
            return false; // not tameable - no owner
        }
        Tameable tameable = (Tameable) entity;
        if (!tameable.isTamed()) {
            return false; // not tamed - no owner
        }
        AnimalTamer owner = tameable.getOwner();
        return owner != null && owner.getUniqueId().equals(player.getUniqueId());
    }
}
