package net.pl3x.bukkit.cities.event.protection;

import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockEvent;

@SuppressWarnings("unused")
public class IceFormEvent extends BlockEvent implements Cancellable {
    private static final HandlerList handlerList = new HandlerList();
    private boolean cancelled;

    public IceFormEvent(Block block) {
        super(block);
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
