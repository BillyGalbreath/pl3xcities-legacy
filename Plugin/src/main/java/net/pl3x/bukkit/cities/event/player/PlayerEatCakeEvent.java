package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class PlayerEatCakeEvent extends PlayerEvent {
    private final Block cakeBlock;

    public PlayerEatCakeEvent(Player player, Block cakeBlock) {
        super(player);
        this.cakeBlock = cakeBlock;
    }

    public Block getBlock() {
        return cakeBlock;
    }
}
