package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerDamageAnimalEvent extends PlayerEvent {
    private final Entity damaged;

    public PlayerDamageAnimalEvent(Entity damaged, Player damager) {
        super(damager);
        this.damaged = damaged;
    }

    public Entity getEntity() {
        return damaged;
    }

    public Entity getDamaged() {
        return getEntity();
    }

    public Player getDamager() {
        return getPlayer();
    }
}
