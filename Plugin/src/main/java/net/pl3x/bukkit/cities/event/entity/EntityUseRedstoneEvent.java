package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;

public class EntityUseRedstoneEvent extends EntityEvent {
    private final Block redstone;

    public EntityUseRedstoneEvent(Entity entity, Block redstone) {
        super(entity);
        this.redstone = redstone;
    }

    public Block getBlock() {
        return redstone;
    }
}
