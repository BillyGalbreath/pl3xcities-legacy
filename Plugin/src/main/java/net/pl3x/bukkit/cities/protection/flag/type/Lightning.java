package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.weather.LightningStrikeEvent;

/*
 * Protection flag to stop lightning strikes
 */
public class Lightning extends FlagListener {
    private final String name = "lightning";

    public Lightning(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops lightning from striking
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onLightningStrike(LightningStrikeEvent event) {
        if (Flags.isAllowed(name, null, event.getLightning().getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
