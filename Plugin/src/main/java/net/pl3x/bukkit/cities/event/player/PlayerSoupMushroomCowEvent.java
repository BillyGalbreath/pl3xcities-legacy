package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerSoupMushroomCowEvent extends PlayerEvent {
    private final Entity cow;

    public PlayerSoupMushroomCowEvent(Player player, Entity cow) {
        super(player);
        this.cow = cow;
    }

    public Entity getEntity() {
        return cow;
    }

    public Entity getCow() {
        return getEntity();
    }
}
