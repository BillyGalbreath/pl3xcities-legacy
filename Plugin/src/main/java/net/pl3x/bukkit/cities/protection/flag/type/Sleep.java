package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.player.PlayerSleepEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to stop players from sleeping in beds
 */
public class Sleep extends FlagListener {
    private final String name = "sleep";

    public Sleep(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerSleep(PlayerSleepEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.sleep")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getBedBlock().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.SLEEP_DENY).send(player);
        event.setCancelled(true);
    }
}
