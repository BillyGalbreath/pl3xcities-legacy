package net.pl3x.bukkit.cities.command.city;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.hook.Vault;
import net.pl3x.bukkit.cities.manager.ChatManager;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.flag.Flag;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class CmdCityInfo extends PlayerCommand {
    public CmdCityInfo(Pl3xCities plugin) {
        super(plugin, "info", Lang.CMD_DESC_CITY_INFO, "cities.command.city.info", Lang.CMD_HELP_CITY_INFO);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        List<String> results = new ArrayList<>();
        if (args.size() == 1) {
            results.addAll(CityManager.getManager().getCities().stream()
                    .filter(city -> city.getName().toLowerCase().startsWith(args.peek().toLowerCase()))
                    .map(City::getName).collect(Collectors.toList()));
        }
        return results;
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        // get the city
        City city;
        String cityName = args.peek();
        if (cityName != null) {
            city = CityManager.getManager().getCity(cityName);
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_NAME);
            }
        } else {
            city = CityManager.getManager().getCity(player.getLocation());
            if (city == null) {
                throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
            }
        }

        // get flags list
        TreeMap<String, String> map = new TreeMap<>();
        for (Flag flag : city.getFlags()) {
            map.put(flag.getName(), flag.getState().name());
        }
        StringBuilder flags = new StringBuilder();
        for (Entry<String, String> entry : map.entrySet()) {
            if (flags.length() > 0) {
                flags.append("&e, &7");
            }
            flags.append(entry.getKey());
            flags.append("&d: &7");
            flags.append(entry.getValue());
        }
        if (flags.length() == 0) {
            flags.append("&onone");
        }

        // show the city info
        new Chat("&dCity: &7" + city.getName()).send(player);
        ChatManager.sendMessage(player, city.getUsersTextComponent("&dOwner(s): ", city.getOwners()));
        ChatManager.sendMessage(player, city.getUsersTextComponent("&dMember(s): ", city.getMembers()));
        new Chat("   &dPopulation: &7" + city.getPopulation()).send(player);
        new Chat("   &dSpawn: &7" + city.getSpawnCoords()).send(player);
        new Chat("   &dForSale: &7" + (city.isForSale() ? Lang.WORD_YES + " " + Vault.format(city.getPrice()) : Lang.WORD_NO)).send(player);
        new Chat("   &dArea: &7" + city.getArea() + " m\u00B2 (" + city.getChunks().size() + " chunks)").send(player);
        new Chat("   &dPlot Limit: &7" + city.getPlotLimit()).send(player);
        new Chat("   &dPlots: &7" + city.getPlots().size()).send(player);
        new Chat("   &dFlags: &7 " + flags.toString()).send(player);

        SoundManager.playSoundToPlayer(player, Config.SOUND_CITY_INFO.getString());
    }
}
