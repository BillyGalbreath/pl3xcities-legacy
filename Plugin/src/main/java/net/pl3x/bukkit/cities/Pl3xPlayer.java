package net.pl3x.bukkit.cities;

import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.Selection;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

@SuppressWarnings("WeakerAccess")
public class Pl3xPlayer {
    private static final HashMap<UUID, Pl3xPlayer> players = new HashMap<>();

    /**
     * Get the Pl3xPlayer instance for a Player
     *
     * @param player Bukkit Player
     * @return Pl3xPlayer instance
     */
    public static Pl3xPlayer getPlayer(Player player) {
        return getPlayer(player.getUniqueId());
    }

    /**
     * Get the Pl3xPlayer instance for a Player
     *
     * @param uuid Bukkit Player's UUID
     * @return Pl3xPlayer instance
     */
    public static Pl3xPlayer getPlayer(UUID uuid) {
        if (!players.containsKey(uuid)) {
            players.put(uuid, new Pl3xPlayer(uuid));
        }
        return players.get(uuid);
    }

    /**
     * Remove Pl3xPlayer instance from memory
     *
     * @param player Bukkit Player
     */
    public static void remove(Player player) {
        remove(player.getUniqueId());
    }

    /**
     * Remove Pl3xPlayer instance from memory
     *
     * @param uuid Bukkit Player's UUID
     */
    public static void remove(UUID uuid) {
        if (players.containsKey(uuid)) {
            players.remove(uuid);
        }
    }

    /**
     * Unloads all Pl3xPlayer data from memory
     */
    public static void unloadAll() {
        players.values().forEach(Pl3xPlayer::unload);

        players.clear();
    }

    // ########################################################
    // #####               Instance Stuff                 #####
    // ########################################################

    private UUID uuid = null;
    private City city = null;
    private Plot plot = null;
    private Selection selection = null;

    /**
     * Represents a player instance which stores various plugin information about an online player
     *
     * @param uuid Bukkit Player's UUID
     */
    private Pl3xPlayer(UUID uuid) {
        this.uuid = uuid;
    }

    public void unload() {
        if (selection != null) {
            selection.clear();
        }

        uuid = null;
        city = null;
        plot = null;
        selection = null;
    }

    /**
     * Get this player's Bukkit UUID
     *
     * @return UUID
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Get the Bukkit Player instance for this player
     *
     * @return Bukkit Player
     */
    public Player getPlayer() {
        return Bukkit.getPlayer(uuid);
    }

    /**
     * Get the city this player is in
     *
     * @return City
     */
    public City getCity() {
        return city;
    }

    /**
     * Set the city this player is in. If it is different than the current city a title will be displayed
     *
     * @param city City
     */
    public void setCity(City city) {
        if (city == this.city) {
            return;
        }

        this.city = city;

        Player player = getPlayer();

        String cityName = city == null ? "Wilderness" : city.getName();
        String hasPvp = city == null || city.hasPvp() ? "&4" + Lang.WORD_ON : "&a" + Lang.WORD_OFF;
        String cityOwner = city == null ? "No one" : String.join("&e, &7", city.getOwners().values());

        String title = Lang.TITLE
                .replace("{city-name}", cityName)
                .replace("{pvp}", hasPvp)
                .replace("{owner}", cityOwner);
        String subtitle = Lang.SUBTITLE
                .replace("{city-name}", cityName)
                .replace("{pvp}", hasPvp)
                .replace("{owner}", cityOwner);
        String actionbar = Lang.ACTIONBAR
                .replace("{city-name}", cityName)
                .replace("{pvp}", hasPvp)
                .replace("{owner}", cityOwner);

        new Title(title, subtitle, actionbar).send(player);
    }

    /**
     * Get the Plot this player is in
     *
     * @return Plot
     */
    public Plot getPlot() {
        return plot;
    }

    /**
     * Set the Plot this player is in
     *
     * @param plot Plot
     */
    public void setPlot(Plot plot) {
        this.plot = plot;
    }

    public void setPlot(Location location) {
        City city = CityManager.getManager().getCity(location);
        setPlot(city == null ? null : city.getPlot(location));
    }

    /**
     * Get the player's wand selection. Creates new selection if none exists
     *
     * @return Selection
     */
    public Selection getSelection() {
        if (selection == null) {
            selection = new Selection();
        }
        return selection;
    }

    public void setSelection(Selection selection) {
        this.selection = selection;
    }
}
