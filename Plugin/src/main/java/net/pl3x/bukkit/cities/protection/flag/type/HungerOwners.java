package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.player.PlayerHungerEvent;
import net.pl3x.bukkit.cities.event.player.PlayerWalkEvent;
import net.pl3x.bukkit.cities.protection.Protection;
import net.pl3x.bukkit.cities.protection.flag.Flag;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import net.pl3x.bukkit.cities.tasks.StopHungerBarShaking;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerTeleportEvent;

/*
 * Protection flag to stop hunger for protection owners
 */
public class HungerOwners extends FlagListener {
    private final String name = "hunger-owners";

    public HungerOwners(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops hunger for protection owners
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerHunger(PlayerHungerEvent event) {
        Player player = event.getPlayer();
        if (shouldCancel(player, player.getLocation())) {
            event.setCancelled(true);
        }
    }

    /*
     * Stops hunger bar from shaking for owners
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerWalk(PlayerWalkEvent event) {
        Player player = event.getPlayer();
        if (shouldCancel(player, event.getTo())) {
            new StopHungerBarShaking(player).runTask(Pl3xCities.getPlugin(Pl3xCities.class));
        }
    }

    /*
     * Stops hunger bar from shaking for owners
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        Player player = event.getPlayer();
        if (shouldCancel(player, event.getTo())) {
            new StopHungerBarShaking(player).runTask(Pl3xCities.getPlugin(Pl3xCities.class));
        }
    }

    private boolean shouldCancel(Player player, Location location) {
        Protection protection = Protection.getProtection(location);
        if (protection == null) {
            return false; // no protection here
        }

        if (!protection.isOwner(player)) {
            return false; // not an owner
        }

        Flag flag = protection.getFlag(name);
        if (flag == null) {
            return false; // no flag
        }

        if (flag.getState().equals(FlagState.ALLOW)) {
            return false; // allow hunger
        }

        // cancel hunger
        return true;
    }
}
