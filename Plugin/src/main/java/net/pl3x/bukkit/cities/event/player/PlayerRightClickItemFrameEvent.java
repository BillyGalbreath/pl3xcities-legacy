package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("WeakerAccess")
public class PlayerRightClickItemFrameEvent extends PlayerEvent {
    private final Entity frame;

    public PlayerRightClickItemFrameEvent(Player player, Entity frame) {
        super(player);
        this.frame = frame;
    }

    public Entity getEntity() {
        return frame;
    }

    public Entity getItemFrame() {
        return getEntity();
    }
}
