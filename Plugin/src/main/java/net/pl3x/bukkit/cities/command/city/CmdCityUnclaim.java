package net.pl3x.bukkit.cities.command.city;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.city.CityUnclaimEvent;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.hook.Vault;
import net.pl3x.bukkit.cities.manager.ChunkManager;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdCityUnclaim extends PlayerCommand {
    public CmdCityUnclaim(Pl3xCities plugin) {
        super(plugin, "unclaim", Lang.CMD_DESC_CITY_UNCLAIM, "cities.command.city.unclaim", Lang.CMD_HELP_CITY_UNCLAIM);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // Show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        CityManager cityManager = CityManager.getManager();
        ChunkManager chunkManager = ChunkManager.getManager();

        // get city at location
        Location here = player.getLocation();
        City city = cityManager.getCity(here);
        if (city == null) {
            throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
        }

        // check if owner of city
        if (!city.isOwner(player) && !PermManager.hasPerm(player, "cities.override.unclaimcity")) {
            throw new CommandException(Lang.NOT_CITY_OWNER);
        }

        // check chunk for any plots
        Chunk bukkitChunk = here.getChunk();
        int maxheight = here.getWorld().getMaxHeight();
        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                for (int y = 0; y < maxheight; y++) {
                    if (city.getPlot(bukkitChunk.getBlock(x, y, z).getLocation()) != null) {
                        throw new CommandException(Lang.CITY_UNCLAIM_PLOT_CHUNK_ERROR);
                    }
                }
            }
        }

        // mark for entire city deletion if last chunk is unclaimed
        boolean deleting = false;
        double cost = Config.CITY_UNCLAIM_COST.getDouble();
        if (city.getChunks().size() <= 1) {
            deleting = true;
            cost = Config.CITY_DELETION_COST.getDouble();
        } else {
            // check if attempting to delete "spawn" chunk
            if (chunkManager.getChunk(here).equals(chunkManager.getChunk(city.getSpawn()))) {
                throw new CommandException(Lang.CITY_UNCLAIM_SPAWN_CHUNK_ERROR);
            }
        }

        // check player funds
        if (!player.hasPermission("cities.override.economy.city.unclaim")) {
            double balance = Vault.getBalance(player.getUniqueId());
            if (cost > balance) {
                throw new CommandException(Lang.CANNOT_AFFORD_CITY_UNCLAIM.replace("{amount}", Vault.format(cost)));
            }
        }

        // call the event and check for cancellation
        CityUnclaimEvent event = new CityUnclaimEvent(city, player, here, deleting);
        Bukkit.getServer().getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            return;
        }

        // charge the player
        if (!player.hasPermission("cities.override.economy.city.unclaim")) {
            if (cost > 0) {
                Vault.withdrawPlayer(player.getUniqueId(), cost);
            }
            if (cost < 0) {
                Vault.depositPlayer(player.getUniqueId(), Math.abs(cost));
            }
        }

        // unclaim chunk/delete city
        if (deleting) {
            cityManager.deleteCity(city);
            new Chat(Lang.CITY_DELETE_SUCCESS).send(player);
        } else {
            chunkManager.unclaimCityChunk(city, here);
            new Chat(Lang.CITY_UNCLAIM_SUCCESS).send(player);
        }

        // notify of charge/deposit
        if (!player.hasPermission("cities.override.economy.city.unclaim")) {
            if (cost > 0) {
                new Chat(Lang.VAULT_ACCOUNT_CHARGED
                        .replace("{amount}", Vault.format(cost)))
                        .send(player);
            }
            if (cost < 0) {
                new Chat(Lang.VAULT_ACCOUNT_PAID
                        .replace("{amount}", Vault.format(cost)))
                        .send(player);
            }
        }

        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        // update current city (wilderness now)
        pl3xPlayer.setCity(null);

        // send player sound
        SoundManager.playSoundToPlayer(player, Config.SOUND_CITY_UNCLAIM.getString());
    }
}
