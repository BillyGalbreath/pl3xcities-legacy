package net.pl3x.bukkit.cities.command.city;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Logger;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.city.CityClaimEvent;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.hook.Vault;
import net.pl3x.bukkit.cities.manager.ChunkManager;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdCityClaim extends PlayerCommand {
    public CmdCityClaim(Pl3xCities plugin) {
        super(plugin, "claim", Lang.CMD_DESC_CITY_CLAIM, "cities.command.city.claim", Lang.CMD_HELP_CITY_CLAIM);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // Show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        // check for specified city name in command
        if (args.isEmpty()) {
            throw new CommandException(Lang.CITY_NAME_NOT_SPECIFIED);
        }
        String name = args.pop().trim();

        CityManager cityManager = CityManager.getManager();
        ChunkManager chunkManager = ChunkManager.getManager();

        // check for invalid city name characters
        if (cityManager.isInvalidName(name)) {
            throw new CommandException(Lang.CITY_NAME_INVALID_CHARACTERS);
        }

        // check for blacklisted items in city name
        if (cityManager.isBlacklistedName(name)) {
            throw new CommandException(Lang.CITY_NAME_BLACKLISTED);
        }

        // check if chunk is already claimed
        Location here = player.getLocation();
        City city = cityManager.getCity(here);
        if (city != null) {
            throw new CommandException(Lang.CITY_ALREADY_EXISTS_AT_LOCATION);
        }

        // check if city name already exists
        boolean expanding = false;
        city = cityManager.getCity(name);
        if (city != null) {
            // check if owner of city
            if (!city.isOwner(player) && !PermManager.hasPerm(player, "cities.override.claimcity")) {
                throw new CommandException(Lang.NOT_CITY_OWNER);
            }

            // check border chunks for same city
            if (chunkManager.notTouchingCity(here, city.getName())) {
                throw new CommandException(Lang.CITY_CLAIM_NOT_CONNECTED);
            }

            // we are expanding
            expanding = true;
        } else {
            // check player's city limits
            int limit = cityManager.getLimit(player);
            int owned = cityManager.numberOfOwnedCities(player);
            Logger.debug("Limit: " + limit + " Owned: " + owned);
            if (limit > 0 && owned >= limit) {
                throw new CommandException(Lang.CITY_LIMIT_REACHED);
            }
        }

        // check border chunks for other city if touching cities has been disabled in config
        if (!Config.ALLOW_TOUCHING_CITIES.getBoolean()) {
            if (chunkManager.isChunkTouchingOtherCity(here, name)) {
                throw new CommandException(Lang.CITY_CLAIM_TOUCHING_OTHER_CITY);
            }
        }

        // check player funds
        double cost = 0;
        if (!player.hasPermission("cities.override.economy.city.claim")) {
            cost = Config.CITY_STARTING_COST.getDouble();
            if (expanding) {
                cost = Config.CITY_EXPANDING_COST.getDouble();
            }
            double balance = Vault.getBalance(player.getUniqueId());
            if (cost > balance) {
                throw new CommandException(Lang.CANNOT_AFFORD_CITY_CLAIM.replace("{amount}", Vault.format(cost)));
            }
        }

        // call the event and check for cancellation
        CityClaimEvent event = new CityClaimEvent(city, player, here, expanding);
        Bukkit.getServer().getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            return;
        }

        // charge the player
        if (!player.hasPermission("cities.override.economy.city.claim")) {
            Vault.withdrawPlayer(player.getUniqueId(), cost);
        }

        // create/expand the actual city
        if (expanding) {
            chunkManager.claimCityChunk(city, here);
            new Chat(Lang.CITY_EXPAND_SUCCESS).send(player);
        } else {
            city = cityManager.createCity(name, player);
            new Chat(Lang.CITY_CREATE_SUCCESS).send(player);
            new Chat(Lang.CITY_CREATE_BROADCAST
                    .replace("{owner}", player.getName())
                    .replace("{city-name}", city.getName()))
                    .broadcast();
        }

        if (!player.hasPermission("cities.override.economy.city.claim")) {
            new Chat(Lang.VAULT_ACCOUNT_CHARGED
                    .replace("{amount}", Vault.format(cost)))
                    .send(player);
        }

        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        // update current city
        pl3xPlayer.setCity(city);

        // send player sound
        SoundManager.playSoundToPlayer(player, Config.SOUND_CITY_CLAIM.getString());
    }
}
