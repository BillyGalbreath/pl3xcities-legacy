package net.pl3x.bukkit.cities.configuration;

import net.pl3x.bukkit.cities.Logger;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.protection.CityChunk;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.flag.Flag;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@SuppressWarnings("WeakerAccess")
public enum CityConfig {
    PRICE(0.0),
    FORSALE(false),
    SPAWN(null),
    PLOTLIMIT(-1),
    PLOTRESTRICTED(null),
    FLAGS(null),
    OWNERS(null),
    MEMBERS(null),
    CHUNKS(null),
    PLOTS(null);

    private final Pl3xCities plugin;
    private final Object def;

    CityConfig(Object def) {
        this.plugin = Pl3xCities.getPlugin(Pl3xCities.class);
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public void set(String cityName, Object value) {
        saveConfig(cityName, value);
    }

    public List<String> getStringList(String cityName) {
        return getConfig(cityName).getStringList(getKey());
    }

    public List<Map<?, ?>> getMapList(String cityName) {
        return getConfig(cityName).getMapList(getKey());
    }

    public Map<String, ?> getMap(String cityName) {
        ConfigurationSection cs = getConfig(cityName).getConfigurationSection(getKey());
        if (cs == null) {
            return null;
        }
        return cs.getValues(false);
    }

    public boolean getBoolean(String cityName) {
        return getConfig(cityName).getBoolean(getKey(), (Boolean) def);
    }

    public int getInteger(String cityName) {
        return getConfig(cityName).getInt(getKey(), (Integer) def);
    }

    public double getDouble(String cityName) {
        return getConfig(cityName).getDouble(getKey(), (Double) def);
    }

    public Map<UUID, String> getPlayers(String cityName) {
        Map<UUID, String> map = new HashMap<>();
        for (Map<?, ?> player : getMapList(cityName)) {
            map.put(UUID.fromString((String) player.get("uuid")), (String) player.get("name"));
        }
        return map;
    }

    public void setPlayers(String cityName, Map<UUID, String> players) {
        List<HashMap<String, String>> maps = new ArrayList<>();
        for (Entry<UUID, String> entry : players.entrySet()) {
            HashMap<String, String> player = new HashMap<>();
            player.put("uuid", entry.getKey().toString());
            player.put("name", entry.getValue());
            maps.add(player);
        }
        set(cityName, maps);
    }

    public Location getLocation(String cityName) {
        ConfigurationSection config = getConfig(cityName).getConfigurationSection(getKey());
        if (config == null) {
            return null;
        }
        String worldName = config.getString("world");
        World world = Bukkit.getWorld(worldName);
        if (world == null) {
            return null;
        }
        double x = config.getDouble("x");
        double y = config.getDouble("y");
        double z = config.getDouble("z");
        float pitch = (float) config.getDouble("pitch");
        float yaw = (float) config.getDouble("yaw");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public void setLocation(String cityName, Location location) {
        ConfigurationSection config = getConfig(cityName).createSection(getKey());
        config.set("world", location.getWorld().getName());
        config.set("x", location.getX());
        config.set("y", location.getY());
        config.set("z", location.getZ());
        config.set("pitch", location.getPitch());
        config.set("yaw", location.getYaw());
        set(cityName, config);
    }

    public Set<CityChunk> getChunks(String cityName) {
        Set<CityChunk> list = new HashSet<>();
        World world = CityManager.getManager().getCity(cityName).getSpawn().getWorld();
        for (String coord : getStringList(cityName)) {
            String[] coords = coord.split(",");
            int x = Integer.valueOf(coords[0]);
            int z = Integer.valueOf(coords[1]);
            Location location = new Location(world, x << 4, 0, z << 4);
            list.add(new CityChunk(cityName, location));
        }
        return list;
    }

    public void setChunks(String cityName, Set<CityChunk> chunks) {
        set(cityName, chunks.stream()
                .map(chunk -> chunk.getX() + "," + chunk.getZ()).collect(Collectors.toList()));
    }

    public Set<Flag> getFlags(String cityName) {
        Set<Flag> set = new HashSet<>();
        Map<String, ?> map = getMap(cityName);
        if (map == null) {
            return set;
        }
        for (Entry<String, ?> entry : map.entrySet()) {
            try {
                Flag flag = Flags.newFlag(entry.getKey(), FlagState.valueOf(((String) entry.getValue()).toUpperCase()));
                if (flag == null) {
                    throw new Exception("Flag name not found!");
                }
                set.add(flag);
            } catch (Exception e) {
                Logger.error("Error loading flag &7" + entry.getKey() + " &4from config for city &7" + cityName + "&4. Ignoring...");
                if (Config.SHOW_STACKTRACES.getBoolean()) {
                    e.printStackTrace();
                }
            }
        }
        return set;
    }

    public void setFlags(String cityName, Set<Flag> flags) {
        HashMap<String, String> map = new HashMap<>();
        for (Flag flag : flags) {
            map.put(flag.getName(), flag.getState().name());
        }
        set(cityName, map);
    }

    public void setPlots(String cityName, Set<Plot> plots) {
        set(cityName, plots.stream()
                .map(Plot::getId).collect(Collectors.toList()));
    }

    private YamlConfiguration getConfig(String cityName) {
        YamlConfiguration config = new YamlConfiguration();
        try {
            config.load(new File(plugin.getDataFolder(), "cities" + File.separator + cityName + ".yml"));
        } catch (Exception e) {
            Logger.error("Error loading city config file!");
            e.printStackTrace();
        }
        return config;
    }

    private void saveConfig(String cityName, Object value) {
        YamlConfiguration config = new YamlConfiguration();
        try {
            File dir = new File(plugin.getDataFolder(), "cities");
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    Logger.error("Unable to create city data directory: " + dir.getAbsolutePath());
                    return;
                }
            }
            File file = new File(dir, cityName + ".yml");
            if (!file.exists()) {
                if (!file.createNewFile()) {
                    Logger.error("Unable to create city data file: " + file.getAbsolutePath());
                    return;
                }
            }
            config.load(file);
            config.set(getKey(), value);
            config.save(file);
        } catch (Exception e) {
            Logger.error("Unable to save city data: " + cityName + ": " + getKey() + ": " + value);
            Logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void deleteFile(String cityName) {
        try {
            File dir = new File(Pl3xCities.getPlugin(Pl3xCities.class).getDataFolder(), "cities");
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    Logger.error("Unable to create city data directory: " + dir.getAbsolutePath());
                    return;
                }
            }
            File file = new File(dir, cityName + ".yml");
            if (!file.exists()) {
                return;
            }
            if (!file.delete()) {
                Logger.error("Unable to delete city data file: " + file.getAbsolutePath());
            }
        } catch (Exception e) {
            Logger.error("Unable to delete city data file: " + cityName + ".yml");
            Logger.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
