package net.pl3x.bukkit.cities.protection.flag;

public class Flag {
    private final String name;
    private final FlagState state;

    public Flag(String name, FlagState state) {
        this.name = name.toLowerCase();
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public FlagState getState() {
        return state;
    }
}
