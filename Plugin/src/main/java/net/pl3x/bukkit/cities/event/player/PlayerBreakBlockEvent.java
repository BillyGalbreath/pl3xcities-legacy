package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerBreakBlockEvent extends PlayerEvent {
    private final Block block;
    private final int exp;

    public PlayerBreakBlockEvent(Player player, Block block, int exp) {
        super(player);
        this.block = block;
        this.exp = exp;
    }

    public Block getBlock() {
        return block;
    }

    public int getExpToDrop() {
        return exp;
    }
}
