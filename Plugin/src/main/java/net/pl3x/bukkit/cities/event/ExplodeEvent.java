package net.pl3x.bukkit.cities.event;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;

import java.util.List;

@SuppressWarnings("WeakerAccess")
public abstract class ExplodeEvent extends EntityEvent {
    private final List<Block> blocks;

    public ExplodeEvent(Entity entity, List<Block> blocks) {
        super(entity);
        this.blocks = blocks;
    }

    public List<Block> getBlocks() {
        return blocks;
    }
}
