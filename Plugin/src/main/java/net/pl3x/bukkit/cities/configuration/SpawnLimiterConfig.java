package net.pl3x.bukkit.cities.configuration;

import net.pl3x.bukkit.cities.Logger;
import net.pl3x.bukkit.cities.Pl3xCities;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.io.File;

public class SpawnLimiterConfig {
    private static final String fileName = "spawn-limiter.yml";
    private static File configFile;
    private static FileConfiguration config;

    public static void reload() {
        reload(false);
    }

    public static void reload(boolean force) {
        if (configFile == null || force) {
            Logger.debug("Loading spawn limiter config file: " + fileName);
            configFile = new File(Pl3xCities.getPlugin(Pl3xCities.class).getDataFolder(), fileName);
            if (!configFile.exists()) {
                Pl3xCities.getPlugin(Pl3xCities.class).saveResource(fileName, false);
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    public static boolean isWorldDisabled(World world) {
        return !config.getStringList("enabled-worlds").contains(world.getName());
    }

    public static boolean isReasonDisabled(CreatureSpawnEvent.SpawnReason reason) {
        return !config.getBoolean("reason-codes." + reason.name(), false);
    }

    public static int getEntityLimit(EntityType type) {
        return config.getInt("entity-types." + type.name(), -1);
    }

    public static int getRadius() {
        return config.getInt("radius", 50);
    }

    public static int getCooldown() {
        return config.getInt("cooldown", 250);
    }

    public static boolean debugMode() {
        return config.getBoolean("debug-mode", false);
    }
}
