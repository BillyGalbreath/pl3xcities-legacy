package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.player.PlayerFishingEvent;
import net.pl3x.bukkit.cities.event.player.PlayerReelEntityEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to stop players from fishing
 */
public class Fishing extends FlagListener {
    private final String name = "fishing";

    public Fishing(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops players from fishing
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerFishing(PlayerFishingEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.fishing")) {
            return;
        }
        boolean allowed = true;
        System.out.println("5");
        if (!Flags.isAllowed(name, player.getUniqueId(), event.getHook().getLocation(), true, false)) {
            allowed = false;
            System.out.println("6");
        } else if (!Flags.isAllowed(name, player.getUniqueId(), player.getLocation(), true, false)) {
            allowed = false;
            System.out.println("7");
        }
        if (allowed) {
            System.out.println("8");
            return;
        }
        System.out.println("9");
        new Chat(Lang.FISHING_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from reeling anything
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerReelEntity(PlayerReelEntityEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.fishing")) {
            return;
        }
        boolean allowed = true;
        Entity reeled = event.getReeled();
        System.out.println("10");
        if (reeled != null && !Flags.isAllowed(name, player.getUniqueId(), reeled.getLocation(), true, false)) {
            allowed = false;
            System.out.println("11");
        } else if (!Flags.isAllowed(name, player.getUniqueId(), player.getLocation(), true, false)) {
            allowed = false;
            System.out.println("12");
        }
        if (allowed) {
            System.out.println("13");
            return;
        }
        System.out.println("14");
        new Chat(Lang.FISHING_DENY).send(player);
        event.setCancelled(true);
    }
}
