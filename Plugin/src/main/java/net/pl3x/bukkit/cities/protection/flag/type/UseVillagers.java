package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.player.PlayerSpawnBabyVillagerFromEggEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.InventoryHolder;

/*
 * Protection flag to protect villagers
 */
public class UseVillagers extends FlagListener {
    private final String name = "use-villagers";

    public UseVillagers(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops players from using spawner eggs to make babies
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerSpawnBabyVillagerFromEgg(PlayerSpawnBabyVillagerFromEggEvent event) {
        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.use.villagers")) {
            return;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), event.getEntity().getLocation(), true, false)) {
            return;
        }
        new Chat(Lang.VILLAGER_SPAWN_BABY_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops players from trading with villagers
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onContainerOpen(InventoryOpenEvent event) {
        if (!(event.getPlayer() instanceof Player)) {
            return; // not a player? o_O this should never happen (i think).
        }

        InventoryHolder holder = event.getInventory().getHolder();
        if (!(holder instanceof Villager)) {
            return; // not a villager
        }

        Player player = (Player) event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.use.villagers")) {
            return;
        }

        if (Flags.isAllowed(name, player.getUniqueId(), ((Entity) holder).getLocation(), true, false)) {
            return;
        }

        new Chat(Lang.USE_CONTAINER_DENY).send(player);
        event.setCancelled(true);
    }
}
