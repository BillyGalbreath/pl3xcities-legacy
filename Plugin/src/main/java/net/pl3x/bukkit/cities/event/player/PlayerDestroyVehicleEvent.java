package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;

public class PlayerDestroyVehicleEvent extends PlayerEvent {
    private final Vehicle vehicle;

    public PlayerDestroyVehicleEvent(Player player, Vehicle vehicle) {
        super(player);
        this.vehicle = vehicle;
    }

    public Entity getVehicle() {
        return vehicle;
    }
}
