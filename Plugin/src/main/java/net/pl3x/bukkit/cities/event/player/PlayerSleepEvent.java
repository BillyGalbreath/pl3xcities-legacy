package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class PlayerSleepEvent extends PlayerEvent {
    private final Block bed;

    public PlayerSleepEvent(Player player, Block bed) {
        super(player);
        this.bed = bed;
    }

    public Block getBedBlock() {
        return bed;
    }
}
