package net.pl3x.bukkit.cities.command.plot;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.hook.SUI;
import net.pl3x.bukkit.cities.protection.Plot;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdPlotShow extends PlayerCommand {
    public CmdPlotShow(Pl3xCities plugin) {
        super(plugin, "show", Lang.CMD_DESC_PLOT_SHOW, "cities.command.plot.show", Lang.CMD_HELP_PLOT_SHOW);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        // get plot player is standing in
        Plot plot = pl3xPlayer.getPlot();
        if (plot == null) {
            throw new CommandException(Lang.FAILED_FIND_PLOT_LOCATION);
        }

        // show the plot visualizer
        SUI.showPlot(player);

        // no need for sound. visualizer will do its own sound effect.
    }
}
