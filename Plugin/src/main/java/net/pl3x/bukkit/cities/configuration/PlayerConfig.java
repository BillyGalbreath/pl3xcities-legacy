package net.pl3x.bukkit.cities.configuration;

import net.pl3x.bukkit.cities.Logger;
import net.pl3x.bukkit.cities.Pl3xCities;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.UUID;

public enum PlayerConfig {
    SOUNDS_ENABLED(true);

    private final Pl3xCities plugin;
    private final Object def;

    PlayerConfig(Object def) {
        this.plugin = Pl3xCities.getPlugin(Pl3xCities.class);
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public void set(UUID uuid, Object value) {
        saveConfig(uuid, value);
    }

    public boolean getBoolean(UUID uuid) {
        return getConfig(uuid).getBoolean(getKey(), (Boolean) def);
    }

    private YamlConfiguration getConfig(UUID uuid) {
        YamlConfiguration config = new YamlConfiguration();
        try {
            config.load(new File(plugin.getDataFolder(), "userdata" + File.separator + uuid.toString() + ".yml"));
        } catch (Exception e) {
            // do nothing! we don't save a config file for every player that joins!
        }
        return config;
    }

    private void saveConfig(UUID uuid, Object value) {
        YamlConfiguration config = new YamlConfiguration();
        try {
            File dir = new File(plugin.getDataFolder(), "userdata");
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    Logger.error("Unable to create player data directory: " + dir.getAbsolutePath());
                    return;
                }
            }
            File file = new File(dir, uuid.toString() + ".yml");
            if (!file.exists()) {
                if (!file.createNewFile()) {
                    Logger.error("Unable to create player data file: " + file.getAbsolutePath());
                    return;
                }
            }
            config.load(file);
            config.set(getKey(), value);
            config.save(file);
        } catch (Exception e) {
            Logger.error("Unable to save player data: " + uuid.toString() + ": " + getKey() + ": " + value);
        }
    }
}
