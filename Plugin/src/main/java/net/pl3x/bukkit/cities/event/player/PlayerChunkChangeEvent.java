package net.pl3x.bukkit.cities.event.player;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerChunkChangeEvent extends PlayerWalkEvent {
    private final Chunk to;
    private final Chunk from;

    public PlayerChunkChangeEvent(Player player, Location to, Location from, Chunk chunkTo, Chunk chunkFrom) {
        super(player, to, from);
        this.to = chunkTo;
        this.from = chunkFrom;
    }

    public Chunk getChunkTo() {
        return to;
    }

    public Chunk getChunkFrom() {
        return from;
    }
}
