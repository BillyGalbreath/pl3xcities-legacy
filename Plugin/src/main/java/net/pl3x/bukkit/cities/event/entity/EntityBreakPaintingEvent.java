package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Hanging;
import org.bukkit.event.hanging.HangingBreakEvent.RemoveCause;

@SuppressWarnings("unused")
public class EntityBreakPaintingEvent extends EntityEvent {
    private final Hanging hanging;
    private final RemoveCause cause;

    public EntityBreakPaintingEvent(Entity entity, Hanging hanging, RemoveCause cause) {
        super(entity);
        this.hanging = hanging;
        this.cause = cause;
    }

    public Hanging getHanging() {
        return hanging;
    }

    public Entity getRemover() {
        return getEntity();
    }

    public RemoveCause getRemoveCause() {
        return cause;
    }
}
