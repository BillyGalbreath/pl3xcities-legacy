package net.pl3x.bukkit.cities.protection;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;

@SuppressWarnings("WeakerAccess")
public class CityChunk {
    private final String cityName;
    private final World world;
    private final int x;
    private final int z;

    public CityChunk(String cityName, World world, int x, int z) {
        this.cityName = cityName;
        this.world = world;
        this.x = x;
        this.z = z;
    }

    public CityChunk(String cityName, Chunk chunk) {
        this(cityName, chunk.getWorld(), chunk.getX(), chunk.getZ());
    }

    public CityChunk(String cityName, Location location) {
        this(cityName, location.getChunk());
    }

    public String getCityName() {
        return cityName;
    }

    public World getWorld() {
        return world;
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    public Location getLocation() {
        return new Location(world, x << 4, 0, z << 4);
    }
}
