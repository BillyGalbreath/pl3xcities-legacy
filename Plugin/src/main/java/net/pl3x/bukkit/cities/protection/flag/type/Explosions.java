package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.explode.CreeperExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.EnderCrystalExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.FireballExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.GenericExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.HangingExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.TNTExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.WitherExplodeEvent;
import net.pl3x.bukkit.cities.event.explode.WitherSkullExplodeEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import java.util.List;

/*
 * Protection flag to stop explosions from doing block damage
 */
public class Explosions extends FlagListener {
    private final String name = "explosions";

    public Explosions(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.DENY);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onWitherExplode(WitherExplodeEvent event) {
        if (cancelEvent(event.getBlocks())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onWitherSkullExplode(WitherSkullExplodeEvent event) {
        if (cancelEvent(event.getBlocks())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onCreeperExplode(CreeperExplodeEvent event) {
        if (cancelEvent(event.getBlocks())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onEnderCrystalExplode(EnderCrystalExplodeEvent event) {
        if (cancelEvent(event.getBlocks())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onFireballExplode(FireballExplodeEvent event) {
        if (cancelEvent(event.getBlocks())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onTNTExplode(TNTExplodeEvent event) {
        if (cancelEvent(event.getBlocks())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onGenericExplode(GenericExplodeEvent event) {
        if (cancelEvent(event.getBlocks())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onHangingExplode(HangingExplodeEvent event) {
        if (Flags.isAllowed(name, null, event.getEntity().getLocation())) {
            event.setCancelled(true);
        }
    }

    private boolean cancelEvent(List<Block> blocks) {
        for (Block block : blocks) {
            if (!Flags.isAllowed(name, null, block.getLocation())) {
                return true;
            }
        }
        return false;
    }
}
