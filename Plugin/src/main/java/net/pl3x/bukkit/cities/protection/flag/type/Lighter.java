package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Logger;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;

/*
 * Protection flag to stop players from starting fires
 */
public class Lighter extends FlagListener {
    private final String name = "lighter";

    public Lighter(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    /*
     * Stops players from igniting blocks on fire
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onBlockIgnite(BlockIgniteEvent event) {
        IgniteCause cause = event.getCause();
        if (!cause.equals(IgniteCause.FLINT_AND_STEEL) && !cause.equals(IgniteCause.FIREBALL)) {
            return;
        }

        Player player = event.getPlayer();
        if (player != null) {
            if (PermManager.hasPerm(player, "cities.override.lighter")) {
                return;
            }
            if (Flags.isAllowed(name, player.getUniqueId(), event.getBlock().getLocation(), false, true)) {
                return;
            }
            new Chat(Lang.LIGHTER_DENY).send(player);
            Logger.debug("BlockIgniteEvent cancelled.");
            event.setCancelled(true);
            return;
        }
        if (Flags.isAllowed(name, null, event.getBlock().getLocation(), false, true)) {
            return;
        }
        event.setCancelled(true);
    }
}
