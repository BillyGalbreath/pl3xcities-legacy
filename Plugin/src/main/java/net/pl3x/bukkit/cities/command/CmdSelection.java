package net.pl3x.bukkit.cities.command;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.command.selection.CmdSelectionClear;
import net.pl3x.bukkit.cities.command.selection.CmdSelectionContract;
import net.pl3x.bukkit.cities.command.selection.CmdSelectionExpand;
import net.pl3x.bukkit.cities.command.selection.CmdSelectionShift;
import net.pl3x.bukkit.cities.configuration.Lang;

public class CmdSelection extends BaseCommand {
    public CmdSelection(Pl3xCities plugin) {
        super(plugin, "selection", Lang.CMD_DESC_SELECTION, "cities.command.selection", null);
        registerSubcommand(new CmdSelectionClear(plugin));
        registerSubcommand(new CmdSelectionExpand(plugin));
        registerSubcommand(new CmdSelectionContract(plugin));
        registerSubcommand(new CmdSelectionShift(plugin));
    }
}
