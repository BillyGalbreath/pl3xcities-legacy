package net.pl3x.bukkit.cities.tasks;

import net.pl3x.bukkit.cities.Pl3xCities;

public class LoadCities implements Runnable {
    private int totalCities;
    private int totalPlots;
    private int totalChunks = 0;
    private int finishedCities = 0;
    private int finishedPlots = 0;

    public LoadCities(int totalCities, int totalPlots) {
        this.totalCities = totalCities;
        this.totalPlots = totalPlots;
    }

    public void incrementCities() {
        finishedCities++;
    }

    public void incrementPlots() {
        finishedPlots++;
    }

    public void decreaseCitiesTotal() {
        totalCities--;
    }

    public void decreasePlotsTotal() {
        totalPlots--;
    }

    public void increaseChunksTotal() {
        totalChunks++;
    }

    public void finished() {
        output();
    }

    private void output() {
        System.out.println("[" + Pl3xCities.getPlugin(Pl3xCities.class).getName() +
                "] [INFO] Loaded " + finishedCities + "/" + totalCities +
                " cities and " + finishedPlots + "/" + totalPlots +
                " plots and " + totalChunks + " Chunks...");
    }

    @Override
    public void run() {
        while (finishedCities < totalCities) {
            output();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignore) {
            }
        }
    }
}
