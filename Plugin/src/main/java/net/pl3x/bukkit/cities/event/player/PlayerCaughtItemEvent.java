package net.pl3x.bukkit.cities.event.player;

import org.bukkit.entity.Entity;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

public class PlayerCaughtItemEvent extends PlayerReelEntityEvent {
    private Item caught;

    public PlayerCaughtItemEvent(Player player, Item caught, FishHook hook) {
        super(player, caught, hook);
        this.caught = caught;
    }

    public Item getItem() {
        return caught;
    }

    public Entity getCaught() {
        return getItem();
    }
}
