package net.pl3x.bukkit.cities.event.protection;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@SuppressWarnings("unused")
public class DragonEggTeleportEvent extends Event implements Cancellable {
    private static final HandlerList handlerList = new HandlerList();
    private final Block block;
    private final Block toBlock;
    private final BlockFace face;
    private boolean cancelled;

    public DragonEggTeleportEvent(Block block, Block toBlock, BlockFace face) {
        this.block = block;
        this.toBlock = toBlock;
        this.face = face;
    }

    public Block getBlock() {
        return block;
    }

    public Block getToBlock() {
        return toBlock;
    }

    public BlockFace getFace() {
        return face;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
