package net.pl3x.bukkit.cities.tasks;

import net.pl3x.bukkit.cities.configuration.CityConfig;
import net.pl3x.bukkit.cities.configuration.PlotConfig;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class UpdatePlayerUUID extends BukkitRunnable {
    private final Player player;

    public UpdatePlayerUUID(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        for (City city : CityManager.getManager().getCities()) {
            if (city.isOwner(player)) {
                city.addOwner(player);
                CityConfig.OWNERS.setPlayers(city.getName(), city.getOwners());
            }
            if (city.isMember(player)) {
                city.addMember(player);
                CityConfig.MEMBERS.setPlayers(city.getName(), city.getMembers());
            }
            for (Plot plot : city.getPlots()) {
                if (plot.isOwner(player)) {
                    plot.addOwner(player);
                    PlotConfig.OWNERS.setPlayers(plot.getId(), plot.getOwners());
                }
                if (plot.isMember(player)) {
                    plot.addMember(player);
                    PlotConfig.MEMBERS.setPlayers(plot.getId(), plot.getMembers());
                }
            }
        }
    }
}
