package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerPlaceItemFrameEvent extends PlayerEvent {
    private final Hanging itemframe;
    private final BlockFace blockface;
    private final Block against;

    public PlayerPlaceItemFrameEvent(Player player, Hanging itemframe, Block against, BlockFace blockface) {
        super(player);
        this.itemframe = itemframe;
        this.against = against;
        this.blockface = blockface;
    }

    public Hanging getItemframe() {
        return itemframe;
    }

    public Block getBlockAgainst() {
        return against;
    }

    public BlockFace getBlockface() {
        return blockface;
    }
}
