package net.pl3x.bukkit.cities.event.entity;

import net.pl3x.bukkit.cities.event.EntityEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class MobTargetPlayerEvent extends EntityEvent {
    private final Player player;

    public MobTargetPlayerEvent(Entity entity, Player player) {
        super(entity);
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}
