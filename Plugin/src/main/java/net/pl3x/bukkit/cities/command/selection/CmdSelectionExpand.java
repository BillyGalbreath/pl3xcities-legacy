package net.pl3x.bukkit.cities.command.selection;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.hook.SUI;
import net.pl3x.bukkit.cities.protection.PlotRegion;
import net.pl3x.bukkit.cities.protection.Selection;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdSelectionExpand extends PlayerCommand {
    public CmdSelectionExpand(Pl3xCities plugin) {
        super(plugin, "expand", Lang.CMD_DESC_SELECTION_EXPAND, "cities.command.selection.expand", Lang.CMD_HELP_SELECTION_EXPAND);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        List<String> result = new ArrayList<>();
        if (args.size() == 1 && "vert".startsWith(args.peek().toLowerCase())) {
            result.add("vert");
        }
        return result;
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // Show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        Selection selection = pl3xPlayer.getSelection();

        if (args.size() < 2) {
            if (args.size() == 1 && "vert".startsWith(args.peek().toLowerCase())) {
                Location primary = selection.getPrimary();
                Location secondary = selection.getSecondary();

                if (primary == null || secondary == null) {
                    throw new CommandException(Lang.SELECTION_NOT_COMPLETE);
                }

                primary.setY(0);
                selection.setPrimary(primary);

                secondary.setY(secondary.getWorld().getMaxHeight() - 1);
                selection.setSecondary(secondary);

                if (Pl3xCities.hasSUI()) {
                    SUI.showSelection(player);
                }

                new Chat(Lang.SELECTION_EXPANDED).send(player);
                return;
            }
            throw new CommandException(Lang.CMD_HELP_SELECTION_EXPAND);
        }

        int amount;
        try {
            amount = Integer.valueOf(args.pop().trim());
        } catch (NumberFormatException e) {
            throw new CommandException(Lang.NOT_A_NUMBER);
        }

        if (amount <= 0) {
            throw new CommandException(Lang.ZERO_NUMBER);
        }

        if (selection.getPrimary() == null || selection.getSecondary() == null) {
            throw new CommandException(Lang.SELECTION_NOT_COMPLETE);
        }

        PlotRegion region = new PlotRegion(selection);
        Location primary = region.getMinPoint();
        Location secondary = region.getMaxPoint();

        String direction = args.pop().trim().toLowerCase();
        if ("up".startsWith(direction)) {
            int max = secondary.getWorld().getMaxHeight() - 1;
            double newValue = secondary.getY() + amount;
            if (newValue > max) {
                newValue = max;
            }
            secondary.setY(newValue);
        } else if ("down".startsWith(direction)) {
            double newValue = primary.getY() - amount;
            if (newValue < 0) {
                newValue = 0;
            }
            primary.setY(newValue);
        } else if ("north".startsWith(direction)) {
            primary.setZ(primary.getZ() - amount);
        } else if ("south".startsWith(direction)) {
            secondary.setZ(secondary.getZ() + amount);
        } else if ("east".startsWith(direction)) {
            secondary.setX(secondary.getX() + amount);
        } else if ("west".startsWith(direction)) {
            primary.setX(primary.getX() - amount);
        } else {
            throw new CommandException(Lang.INVALID_DIRECTION.replace("{direction}", direction));
        }

        selection.setPrimary(primary).setSecondary(secondary);

        new Chat(Lang.SELECTION_EXPANDED).send(player);

        if (Pl3xCities.hasSUI()) {
            SUI.showSelection(player);
        }
    }
}
