package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.Location;
import org.bukkit.block.BlockState;
import org.bukkit.block.DoubleChest;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.InventoryHolder;

/*
 * Protection flag to stop hostile mobs from spawning
 */
public class UseChests extends FlagListener {
    private final String name = "use-chests";

    public UseChests(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.DENY);
    }

    /*
     * Stops players from opening containers (chest, furnace, hopper, dispenser, etc)
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onContainerOpen(InventoryOpenEvent event) {
        if (!(event.getPlayer() instanceof Player)) {
            return; // not a player? o_O this should never happen (i think).
        }

        Player player = (Player) event.getPlayer();

        // Check inventory holder
        InventoryHolder holder = event.getInventory().getHolder();
        if (holder instanceof Villager) {
            return; // handle villagers elsewhere (opens trading menu)
        }

        if (holder instanceof Horse) {
            if (UseAnimals.isOwner((Horse) holder, player)) {
                return;// player owns this horse
            }
        }

        if (PermManager.hasPerm(player, "cities.override.use.container")) {
            return; // player has permission override
        }

        // Get container location
        Location location = getContainerLocation(holder);
        if (location == null) {
            return; // unable to determine container's location. this should never happen
        }

        // Check flag (protection owner cannot open horse container if horse has owner)
        if (Flags.isAllowed(name, player.getUniqueId(), location, true, false)) {
            return; // allowed
        }

        // Deny
        new Chat(Lang.USE_CONTAINER_DENY).send(player);
        event.setCancelled(true);
    }

    private Location getContainerLocation(InventoryHolder holder) {
        if (holder instanceof Player) {
            return null; // player is opening their own inventory
        }
        if (holder instanceof BlockState) {
            return ((BlockState) holder).getLocation(); // opening a chest/furnace etc
        }
        if (holder instanceof Entity) {
            return ((Entity) holder).getLocation(); // opening a horse chest etc
        }
        if (holder instanceof DoubleChest) {
            return ((DoubleChest) holder).getLocation(); // apparently DoubleChest is not a BlockState. weird..
        }
        return null; // unable to determine/don't care
    }
}
