package net.pl3x.bukkit.cities.listener;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.Protection;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import net.pl3x.bukkit.cities.tasks.RefreshChunks;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.List;
import java.util.stream.Collectors;

/**
 * These listeners are default protection behavior and cannot be overridden by a flag
 */
public class ProtectionListener implements Listener {
    private final Pl3xCities plugin;

    public ProtectionListener(Pl3xCities plugin) {
        this.plugin = plugin;
    }

    /*
     * Stop liquids from entering/leaving cities/plots
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void protectFromLiquidSpills(BlockFromToEvent event) {
        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }

        if (!event.getBlock().isLiquid()) {
            return;
        }

        if (Protection.getProtection(event.getBlock().getLocation()) != Protection.getProtection(event.getToBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    /*
     * Stops pistons from pushing blocks into protected areas
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void protectFromPistonPush(BlockPistonExtendEvent event) {
        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }

        Block piston = event.getBlock();
        Location from = piston.getLocation();
        for (Block block : event.getBlocks()) {
            if (Protection.getProtection(from) != Protection.getProtection(block.getRelative(event.getDirection()).getLocation())) {
                event.setCancelled(true);
                piston.setType(Material.AIR);
                piston.getWorld().createExplosion(piston.getLocation(), 0F);
                return;
            }
        }
    }

    /*
     * Stops pistons from pulling blocks from protected areas
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void protectFromPistonPull(BlockPistonRetractEvent event) {
        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }

        Block piston = event.getBlock();
        Location from = piston.getLocation();
        for (Block block : event.getBlocks()) {
            if (Protection.getProtection(from) != Protection.getProtection(block.getLocation())) {
                event.setCancelled(true);
                piston.setType(Material.AIR);
                piston.getWorld().createExplosion(piston.getLocation(), 0F);
                new RefreshChunks(event.getBlocks()).runTaskLater(plugin, 1);
                return;
            }
        }
    }

    /*
     * Stops players from editing signs (via hacks or other plugins)
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onSignChange(SignChangeEvent event) {
        if (Config.isWorldDisabled(event.getBlock().getWorld())) {
            return; // cities not enabled in this world
        }

        Player player = event.getPlayer();
        if (PermManager.hasPerm(player, "cities.override.build")) {
            return;
        }
        if (Flags.isAllowed(null, player.getUniqueId(), event.getBlock().getLocation(), true, true)) {
            return;
        }
        new Chat(Lang.BUILD_DENY).send(player);
        event.setCancelled(true);
    }

    /*
     * Stops explosions from crossing borders
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void protectBorderExplosions(EntityExplodeEvent event) {
        if (Config.isWorldDisabled(event.getEntity().getWorld())) {
            return; // cities not enabled in this world
        }

        Location from = event.getEntity().getLocation();
        List<Block> toRemove = event.blockList().stream()
                .filter(block -> Protection.getProtection(from) != Protection.getProtection(block.getLocation()))
                .collect(Collectors.toList());
        event.blockList().removeAll(toRemove);
    }

    /*
     * Stops withers and dragons from spawning inside city limits
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void exploitWitherDragon(CreatureSpawnEvent event) {
        Entity entity = event.getEntity();
        if (Config.isWorldDisabled(entity.getWorld())) {
            return; // cities not enabled in this world
        }

        if (!Config.PREVENT_WITHER_DRAGON_EXPLOIT.getBoolean()) {
            return; // exploit prevention is disabled
        }

        EntityType type = entity.getType();
        if (type != EntityType.WITHER && type != EntityType.ENDER_DRAGON) {
            return; // not a wither or dragon
        }

        if (Protection.getProtection(entity.getLocation()) == null) {
            return; // not in a city
        }

        // stop wither/dragon from spawning inside city
        event.setCancelled(true);
    }
}
