package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.player.PlayerWalkEvent;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.protection.Protection;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerTeleportEvent;

/*
 * Protection flag to stop players from exiting protections
 */
public class Exit extends FlagListener {
    private final String name = "exit";

    public Exit(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerWalk(PlayerWalkEvent event) {
        if (cancelEvent(event.getPlayer(), event.getTo(), event.getFrom())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (cancelEvent(event.getPlayer(), event.getTo(), event.getFrom())) {
            event.setCancelled(true);
        }
    }

    private boolean cancelEvent(Player player, Location to, Location from) {
        if (PermManager.hasPerm(player, "cities.override.exit")) {
            return false;
        }
        if (Protection.getProtection(to) == Protection.getProtection(from)) {
            return false;
        }
        if (Flags.isAllowed(name, player.getUniqueId(), from, true, false)) {
            return false;
        }
        new Chat(Lang.EXIT_DENY).send(player);
        return true;
    }
}
