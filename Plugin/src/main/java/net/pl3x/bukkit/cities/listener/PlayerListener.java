package net.pl3x.bukkit.cities.listener;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.RegionWandLeftClickEvent;
import net.pl3x.bukkit.cities.event.RegionWandRightClickEvent;
import net.pl3x.bukkit.cities.event.player.PlayerChunkChangeEvent;
import net.pl3x.bukkit.cities.event.player.PlayerWalkEvent;
import net.pl3x.bukkit.cities.hook.SUI;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.tasks.UpdatePlayerUUID;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerListener implements Listener {
    private final Pl3xCities plugin;

    public PlayerListener(Pl3xCities plugin) {
        this.plugin = plugin;
    }

    /*
     * Player Join Server Event
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        // update player name/uuid map for city/plot data (for mojang name changes)
        new UpdatePlayerUUID(event.getPlayer()).runTaskAsynchronously(plugin);

        // delay a bit and then set player's initial city
        new BukkitRunnable() {
            @Override
            public void run() {
                Player player = event.getPlayer();
                Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);
                pl3xPlayer.setCity(CityManager.getManager().getCity(player.getLocation()));
                pl3xPlayer.setPlot(player.getLocation());
            }
        }.runTaskLater(plugin, 20);
    }

    /*
     * Player Quit Server Event (quits, disconnects, kicks, etc)
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Pl3xPlayer.remove(event.getPlayer());
    }

    /*
     * Player changed world
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();
        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        // clear wand selection
        pl3xPlayer.getSelection().clear();

        // update location
        pl3xPlayer.setCity(CityManager.getManager().getCity(player.getLocation()));
        pl3xPlayer.setPlot(player.getLocation());
    }

    /*
     * Player teleported
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        Player player = event.getPlayer();
        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        // clear wand selection
        pl3xPlayer.getSelection().clear();

        // update location
        pl3xPlayer.setCity(CityManager.getManager().getCity(player.getLocation()));
        pl3xPlayer.setPlot(player.getLocation());
    }

    /*
     * Changed Chunk
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerChangeChunk(PlayerChunkChangeEvent event) {
        Pl3xPlayer.getPlayer(event.getPlayer())
                .setCity(CityManager.getManager().getCity(event.getTo()));
    }

    /*
     * Player changed blocks
     */
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerWalk(PlayerWalkEvent event) {
        Pl3xPlayer.getPlayer(event.getPlayer())
                .setPlot(event.getTo());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onWandLeftClick(RegionWandLeftClickEvent event) {
        Player player = event.getPlayer();

        // Set the primary point
        Location loc = event.getClickedBlock().getLocation();
        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);
        pl3xPlayer.getSelection().setPrimary(loc);

        new Chat(Lang.SELECTION_PRIMARY_SET
                .replace("{x}", Integer.toString(loc.getBlockX()))
                .replace("{y}", Integer.toString(loc.getBlockY()))
                .replace("{z}", Integer.toString(loc.getBlockZ())))
                .send(player);

        if (Pl3xCities.hasSUI()) {
            SUI.showSelection(player);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onWandRightClick(RegionWandRightClickEvent event) {
        Player player = event.getPlayer();

        // Set the secondary point
        Location loc = event.getClickedBlock().getLocation();
        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);
        pl3xPlayer.getSelection().setSecondary(loc);

        new Chat(Lang.SELECTION_SECONDARY_SET
                .replace("{x}", Integer.toString(loc.getBlockX()))
                .replace("{y}", Integer.toString(loc.getBlockY()))
                .replace("{z}", Integer.toString(loc.getBlockZ())))
                .send(player);

        if (Pl3xCities.hasSUI()) {
            SUI.showSelection(player);
        }
    }
}
