package net.pl3x.bukkit.cities.protection.flag.type;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.event.entity.VillagerSpawnEvent;
import net.pl3x.bukkit.cities.protection.flag.FlagListener;
import net.pl3x.bukkit.cities.protection.flag.FlagState;
import net.pl3x.bukkit.cities.protection.flag.Flags;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/*
 * Protection flag to stop villagers from spawning
 */
public class SpawnVillagers extends FlagListener {
    private final String name = "spawn-villagers";

    public SpawnVillagers(Pl3xCities plugin) {
        super(plugin);

        Flags.registerFlag(name, FlagState.ALLOW);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onVillagerSpawn(VillagerSpawnEvent event) {
        if (Flags.isAllowed(name, null, event.getLocation(), false, false)) {
            return;
        }
        event.setCancelled(true);
    }
}
