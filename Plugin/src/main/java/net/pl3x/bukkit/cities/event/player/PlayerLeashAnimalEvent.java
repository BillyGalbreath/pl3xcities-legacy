package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerLeashAnimalEvent extends PlayerEvent {
    private final Entity leashed;

    public PlayerLeashAnimalEvent(Player player, Entity leashed) {
        super(player);
        this.leashed = leashed;
    }

    public Entity getEntity() {
        return leashed;
    }

    public Entity getLeashed() {
        return getEntity();
    }
}
