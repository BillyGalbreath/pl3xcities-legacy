package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerLeashMobToFenceEvent extends PlayerEvent {
    private final Entity mob;
    private final Location fence;

    public PlayerLeashMobToFenceEvent(Player player, Entity mob, Location fence) {
        super(player);
        this.mob = mob;
        this.fence = fence;
    }

    public Entity getMob() {
        return mob;
    }

    public Entity getEntity() {
        return getMob();
    }

    public Location getFenceLocation() {
        return fence;
    }
}
