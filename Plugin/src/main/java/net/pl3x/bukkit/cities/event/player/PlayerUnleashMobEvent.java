package net.pl3x.bukkit.cities.event.player;

import net.pl3x.bukkit.cities.event.PlayerEvent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PlayerUnleashMobEvent extends PlayerEvent {
    private final Entity mob;

    public PlayerUnleashMobEvent(Player player, Entity mob) {
        super(player);
        this.mob = mob;
    }

    public Entity getMob() {
        return mob;
    }

    public Entity getEntity() {
        return getMob();
    }

    public Player getRemover() {
        return getPlayer();
    }
}
