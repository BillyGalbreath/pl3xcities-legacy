package net.pl3x.bukkit.cities.command.plot;

import net.pl3x.bukkit.cities.Chat;
import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.Pl3xPlayer;
import net.pl3x.bukkit.cities.command.PlayerCommand;
import net.pl3x.bukkit.cities.configuration.Config;
import net.pl3x.bukkit.cities.configuration.Lang;
import net.pl3x.bukkit.cities.event.plot.PlotCreateEvent;
import net.pl3x.bukkit.cities.exception.CommandException;
import net.pl3x.bukkit.cities.manager.CityManager;
import net.pl3x.bukkit.cities.manager.PermManager;
import net.pl3x.bukkit.cities.manager.SoundManager;
import net.pl3x.bukkit.cities.protection.City;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.cities.protection.PlotRegion;
import net.pl3x.bukkit.cities.protection.Selection;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CmdPlotCreate extends PlayerCommand {
    public CmdPlotCreate(Pl3xCities plugin) {
        super(plugin, "create", Lang.CMD_DESC_PLOT_CREATE, "cities.command.plot.create", Lang.CMD_HELP_PLOT_CREATE);
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return new ArrayList<>();
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        // Show command help
        if ("?".equals(args.peek())) {
            showHelp(player);
            return;
        }

        CityManager cityManager = CityManager.getManager();

        // check if player is standing in city
        Location here = player.getLocation();
        City city = cityManager.getCity(here);
        if (city == null) {
            throw new CommandException(Lang.FAILED_FIND_CITY_LOCATION);
        }

        // check if player owns this city
        if (!city.isOwner(player) && !PermManager.hasPerm(player, "cities.override.createplot")) {
            throw new CommandException(Lang.NOT_CITY_OWNER);
        }

        // check player's selection
        Selection selection = Pl3xPlayer.getPlayer(player).getSelection();
        if (selection == null) {
            throw new CommandException(Lang.SELECTION_NOT_FOUND);
        }

        if (selection.getPrimary() == null) {
            throw new CommandException(Lang.SELECTION_NOT_COMPLETE);
        }
        if (selection.getSecondary() == null) {
            throw new CommandException(Lang.SELECTION_NOT_COMPLETE);
        }

        // get temporary region from selection
        PlotRegion region = new PlotRegion(selection);

        // check temp region is completely inside city
        if (!region.inCity(city)) {
            throw new CommandException(Lang.SELECTION_NOT_IN_CITY);
        }

        // check temp region is overlapping other regions/plots
        for (Plot plot : city.getPlots()) {
            if (plot.getRegion().overlaps(region)) {
                throw new CommandException(Lang.SELECTION_OVERLAPS_PLOT);
            }
        }

        // check if player is standing inside plot
        if (!region.contains(here)) {
            throw new CommandException(Lang.PLOT_MUST_BE_STANDING_IN);
        }

        // make the plot
        Plot plot = new Plot(city.getNextPlotId(), region);
        plot.setSpawn(here);
        plot.addOwner(player);

        // call the event and check for cancellation
        PlotCreateEvent event = new PlotCreateEvent(city, plot, player);
        Bukkit.getServer().getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            return;
        }

        // add plot to city
        city.addPlot(plot);
        cityManager.addPlot(city, plot);

        // notify player of success
        new Chat(Lang.PLOT_CREATE_SUCCESS).send(player);

        // update visualizer if enabled
        //Visualizer.update(player, here, here);

        // send player sound
        SoundManager.playSoundToPlayer(player, Config.SOUND_PLOT_CREATE.getString());
    }
}
