package net.pl3x.bukkit.cities.hook.sui;

import net.pl3x.bukkit.cities.Pl3xCities;
import net.pl3x.bukkit.cities.hook.SUI;
import net.pl3x.bukkit.cities.protection.Plot;
import net.pl3x.bukkit.pl3xsui.api.ParticleColor;
import net.pl3x.bukkit.pl3xsui.task.CuboidParticlesTask;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class PlotParticlesTask extends CuboidParticlesTask {
    private final BukkitTask timeoutTask;

    public PlotParticlesTask(Player player, Plot plot, ParticleColor fillColor, ParticleColor edgeColor) {
        super(player, new CuboidWrapper(plot));

        setFillColor(fillColor);
        setEdgeColor(edgeColor);

        timeoutTask = new BukkitRunnable() {
            public void run() {
                SUI.hidePlot(player);
            }
        }.runTaskLater(Pl3xCities.getPlugin(Pl3xCities.class), 30 * 20);
    }

    @Override
    public void cancel() {
        timeoutTask.cancel();
        super.cancel();
    }
}
