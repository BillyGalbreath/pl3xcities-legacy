package net.pl3x.bukkit.cities.nms.v1_8_R2;

import net.minecraft.server.v1_8_R2.ItemStack;
import net.minecraft.server.v1_8_R2.NBTTagCompound;
import net.pl3x.bukkit.cities.api.EntityTag;
import org.bukkit.craftbukkit.v1_8_R2.inventory.CraftItemStack;

public class EntityTagHandler implements EntityTag {
    public String getEntityTagId(org.bukkit.inventory.ItemStack item) {
        ItemStack stack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tagCompound = stack.getTag();
        if (tagCompound == null) {
            return null;
        }
        return tagCompound.getCompound("EntityTag").getString("id").toUpperCase();
    }
}
