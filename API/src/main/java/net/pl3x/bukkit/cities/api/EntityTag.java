package net.pl3x.bukkit.cities.api;

public interface EntityTag {
    /**
     * Get the EntityTag ID from NBT
     *
     * @param item ItemStack to check
     * @return String ID of EntityTag or null if none found
     */
    String getEntityTagId(org.bukkit.inventory.ItemStack item);
}
